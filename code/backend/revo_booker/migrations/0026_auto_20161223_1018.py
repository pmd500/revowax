# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-23 10:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0025_auto_20161222_1627'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staffregularavailability',
            name='staff',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='regular_availability', to='revo_booker.Staff'),
        ),
    ]

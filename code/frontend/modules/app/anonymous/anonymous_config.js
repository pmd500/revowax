// Configure App
revo_app.config(function($stateProvider) {
    // Anonymous Landing Page
    $stateProvider
        .state('app.anonymous.landing', {
            url: "/landing",
            data: {
                css: 'modules/app/directives/landing/landing.css'
            },
            views: {
                "anonymous_top_menu": {
                    template: "",
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/landing/landing_tmpl.html",
                    controller: "AnonymousLandingCtl",
                    controllerAs: "land_ctl"
                }
            },
        })

    // Anonymous Login Page
    $stateProvider
        .state('app.anonymous.login', {
            url: "/login",
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/login/login_tmpl.html",
                    controller: "LoginCtl",
                    controllerAs: "login_ctl"
                }
            },
        })


    // Anonymous Password Change 
    $stateProvider
        .state('app.anonymous.password_reset', {
            url: "/password-reset",
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/password_reset/password_reset_tmpl.html",
                    // controller: "LoginCtl",
                    // controllerAs: "login_ctl"
                }
            },
        })

    // Anonymous Password Change Confirmation
    $stateProvider
        .state('app.anonymous.password_reset_confirmation', {
            url: "/password-reset/{uid}/{token}",
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/password_reset_confirmation/password_reset_confirmation_tmpl.html",
                    // controller: "LoginCtl",
                    // controllerAs: "login_ctl"
                }
            },
        })


    // Anonymous Salon Profile Page
    $stateProvider
        .state('app.anonymous.salon_profile', {
            url: "/salon-profile",
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/salon/profile/salon_profile_tmpl.html",
                    controller: "AnonymousSalonProfileCtl",
                    controllerAs: "sal_pro_ctl"
                }
            },
        })
    
    //Anonymous Booking Select Services
    $stateProvider
        .state('app.anonymous.booking_select_services', {
            url: "/booking-select-services",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/booking/templates/booking_select_services_tmpl.html",
                    controller: "BookingCtl",
                    controllerAs: "booking_ctl"
                }
            },
        })


    //Anonymous Booking Select Slot
    $stateProvider
        .state('app.anonymous.booking_select_slot', {
            url: "/booking-select-slot",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/booking/templates/booking_select_slot_tmpl.html",
                    controller: "BookingCtl",
                    controllerAs: "booking_ctl"
                }
            },
        })

    // Anonymous Booking UserDetails
    $stateProvider
        .state('app.anonymous.booking_user_details', {
            url: "/booking-user-details",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/booking/templates/booking_user_details_tmpl.html",
                }
            },
        })

    // Anonymous Booking Overview
    $stateProvider
        .state('app.anonymous.booking_overview', {
            url: "/booking-overview",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/booking/templates/booking_overview_tmpl.html",
                }
            },
        })

    // Anonymous Booking Payment
    $stateProvider
        .state('app.anonymous.booking_payment', {
            url: "/booking-payment",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/booking/templates/booking_payment_tmpl.html",
                }
            },
        })

    // Anonymous Booking Confirmation
    $stateProvider
        .state('app.anonymous.booking_confirmation', {
            url: "/booking-confirmation",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/booking/templates/booking_confirmation_tmpl.html",
                }
            },
        })

    // Anonymous Booking Problem
    $stateProvider
        .state('app.anonymous.booking_problem', {
            url: "/booking-problem",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/shared/content/booking/templates/booking_problem_tmpl.html",
                }
            },
        })

});


revo_app.directive('bookingPayment', function(AppStateService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/payment/booking_payment_tmpl.html',
        scope: {
            // variables
            shop:"=",
            // functions
            next:"="
        },
        link : function (scope) {
            scope.continue = function(){
                $state.go(scope.next)
            }
        }
    };
});
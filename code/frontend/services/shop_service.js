revo_app.factory("ShopService", ["$q","UtilityService","$http", "AppStateService", function ($q, UtilityService, $http, AppStateService) {
    // Bind to var
    var service = this;

    // Add full path to services
    var add_full_path_to_services = function (services, path) {
        services.forEach(function (service) {
            service.hide = false;
            service.path = path + " -> " + service.name;
        })
    }

    // Traverse all subcategories building the full path, then adds full path to services
    var traverse_categories = function(categories, path){
        categories.forEach(function (category) {
            // set default
            category.hide = false;

            // if category has services
            if(category.services.length > 0) {
                add_full_path_to_services(category.services, (path + " -> " + category.name))
            }
            else if (category.subcategories.length > 0){
                var new_path = path + " " + category.name
                traverse_categories(category.subcategories,new_path)
            }
        })
    }

    // Formats service tree so that each service has a full path
    var format_service_tree = function (service_groups) {
        var path;
        service_groups.forEach(function (service_group) {
            path = service_group.name + " -> " + service_group.root_catergory.name + " -> "
            traverse_categories(service_group.root_catergory.subcategories, path)
        })
        return service_groups
    }

    // Get Shop Bookings
        // shop id
    service.get_shop_bookings = function (
        shop_id,
        //Callbacks
        success_external_callback,
        fail_external_callback
    )
    {
        console.log(AppStateService.user_state.token)
        // Start loading spinner
        UtilityService.spinner.start()

        // Call shop webservice
        $http({
            method: 'get',
            url: base_url + 'shops/'+ shop_id +'/bookings/',
            headers: {
                'Authorization': "Token " + AppStateService.user_state.token,
            },

        })
        .then(
            // SUCCESS
            function success_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()


                // Call success callback
                success_external_callback(response.data);
            },

            // FAIL
            function fail_callback(response) {
                console.log(response)
                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Call back
                fail_external_callback();

            }
        );
    }

    // Check if authenticated
    service.get_defered_shop_bookings = function (shop_id) {
        var deferred = $q.defer();

        service.get_shop_bookings(
            shop_id,
            // Success
            function (data) {
                deferred.resolve(data);
            },
            // Failed
            function () {
                deferred.reject();
            }
        )
        return deferred.promise;
    };
    
    
    // Authenticate - get user details from token
    service.get_shop_details = function (shop_id, success_external_callback, fail_external_callback) {
        // Start loading spinner
        UtilityService.spinner.start()

        // Call shop webservice
        $http({
            method: 'get',
            url: base_url + 'shops/' + shop_id + "/"
        })
        .then(
            // SUCCESS
            function success_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Add full path to all services in each category/service group
                format_service_tree(response.data.service_groups)

                // Call success callback
                success_external_callback(response.data);
            },

            // FAIL
            function fail_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Call back
                fail_external_callback();

            }
        );
    }

    // Check if authenticated
    service.get_defered_shop_details = function (shop_id) {
        var deferred = $q.defer();

        service.get_shop_details(
            shop_id,
            // Success
            function (data) {
                deferred.resolve(data);
            },
            // Failed
            function () {
                deferred.reject();
            }
        )
        return deferred.promise;
    };

    return service;
}]);

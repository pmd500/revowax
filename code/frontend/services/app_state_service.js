// Share application state is stored and used across the controllers
revo_app.factory('AppStateService', [ "$state","$rootScope", function ($state, $rootScope) {
    var service = {
        user_state : {
            // For all users
            logged_on: false,
            token:null,
            // username :null,
            // profile_id :null,
            // profile_type :null,
            // new_email :null,
            // new_last_name :null,
            // new_first_name :null,
            // mobile :null,
            // user_id:null,
            //
            // // Customers only
            // customer_id :null,
            //
            // // Staff only
            // business_id :null,
            // business :null,
            // staff_id :null,
            // staff_type_id :null,
            // staff_type :null

        },
        customer_state: {
            
        }
    };

    // Get User response_data
    service.clear_user_state = function(){
        service = {
            user_state: {
                // For all users
                logged_on: false,
                token:null,
                // profile_id:null,
                // profile_type:null,
                // new_email:null,
                // new_last_name:null,
                // new_first_name:null,
                // mobile:null,
                // user_id:null,
                // username:null,
                //
                // // Customers only
                // customer_id:null,
                //
                // // Staff only
                // business_id:null,
                // business:null,
                // staff_id:null,
                // staff_type_id:null,
                // staff_type:null
            }
        }
    }
    
    // Get User response_data
    service.get_user_state = function(){
        return service.user_state;
    }

    // Set User response_data
    service.set_log_on_status_4_user = function(token){
        service.user_state.logged_on = true;
        service.user_state.token = token;
    }
    
    // Set user response_data from authentication service
    service.set_user_state_frm_response_data = function(response_data, is_logged_on, token){
        service.user_state = response_data;
        service.user_state.logged_on = is_logged_on;
        service.user_state.token = token;

        // service.user_state.profile_type = response_data.profile_type;
        // service.user_state.new_email = response_data.new_email;
        // service.user_state.new_last_name = response_data.new_last_name;
        // service.user_state.new_first_name = response_data.new_first_name;
        // service.user_state.mobile = response_data.mobile;
        // service.user_state.user_id = response_data.user_id;
        // service.user_state.username = response_data.username;
        //
        // // Customer only fields
        // service.user_state.customer_id = (response_data.profile_type == "C") ? response_data.customer_id :null;
        //
        // // Staff only fields
        // service.user_state.business_id = (response_data.profile_type == "S") ? response_data.business_id :null;
        // service.user_state.business = (response_data.profile_type == "S") ? response_data.business :null;
        // service.user_state.staff_id = (response_data.profile_type == "S") ? response_data.staff_id :null;
        // service.user_state.staff_type_id = (response_data.profile_type == "S") ?response_data.staff_type_id :null;
        // service.user_state.staff_type = (response_data.profile_type == "S") ? response_data.staff_type :null;

    }
    
    //If re route to correct dashboard depending on profile type
    service.navigate_to_profile_home = function () {
        if(service.user_state.profile_type != null){
            switch (service.user_state.profile_type) {
                // If customer
                case  "C":
                    $state.go("app.customer.landing")
                    break;

                // If staff
                case  "S":
                    $state.go("app.customer.landing")
                    break

                // If manager
                case  "M":
                    $state.go("app.manager.landing")
                    break

                // Else got back to anonymous landing
                default :
                    $state.go("app.anonymous.landing")

            }
        }
        else
            $state.go("app.anonymous.landing")
    }


    //If re route to correct dashboard depending on profile type
    service.nav_to_anonymous_landing_if_not_logged_in = function () {
        if(!service.user_state.logged_on) 
            // Else got back to anonymous landing
            $state.go("app.anonymous.landing")
    }
    
    return service;
}]);
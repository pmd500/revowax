from django.shortcuts import get_object_or_404
import json
from rest_framework import status
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView

from revo_booker.utility import validate_fields
from revo_booker.serializers.deep import *

# Generates customer, staff or manager profile
def generate_profile_json (profile):
    json = {}
    json["id"] = profile.id
    json["profile_type"] = profile.profile_type
    json["email"] = profile.email
    json["first_name"] = profile.first_name
    json["last_name"] = profile.last_name
    json["mobile"] = profile.mobile
    json["registered"] = profile.registered
    json["receive_promotions"] = profile.receive_promotions

    # If user is registereded
    if (profile.registered):
        json["user_id"] = profile.user.id

    # Staff
    if (profile.profile_type == "S"):
        json["business_id"] = profile.staff.business.id
        json["business"] = profile.staff.business.name
        json["staff_id"] = profile.staff.id
        json["staff_type_id"] = profile.staff.type.id
        json["staff_type"] = profile.staff.type.role

    return json

class AuthenticateProfileView(APIView):
    permission_classes = (IsAuthenticated,)

    # Authenticate Profile
    def get(self, request, format=None):

        # Get Profile
        profile = request.user.profile

        return Response(generate_profile_json(profile))

class CreateProfileView(APIView):
    permission_classes = (AllowAny,)

    # Add relationship between customer and business
    def add_customer_business_relation(self, business, profile):
        if(business != None):
            print "ADDING RELATIONSHIP"
            print business.id
            print profile.id
            exists = CustomerBusiness.objects.filter(business=business, customer=profile).exists()
            print exists
            if(not exists):
                cb = CustomerBusiness(business=business, customer=profile)
                cb.save()
                print cb.id

    # Create Profile and related models
    def post(self, request, format=None):
        has_business = False
        business = None
        if 'business' in request.data.keys():
            business_id = request.data.pop('business', None)
            business = Business.objects.get(id=business_id)
            print "business_id"
            print business_id
            has_business = True

        serializer = SimpleProfileSerializer(data=request.data)

        if(not serializer.is_valid()):
            return Response({'message': serializer.errors}, status=status.HTTP_406_NOT_ACCEPTABLE)

        # Extract password
        password = ""
        if 'password' in request.data:
            password = request.data["password"]
            del request.data['password']

        # Create new profile
        profile = Profile(**request.data)

        # Check if profie already exists
        profile_exists = False
        if not ((profile.email == "") | (profile.email == " ")):
            profile_exists = Profile.objects.filter(email=profile.email).exists()
            if(profile_exists):
                print "Email exists: ", profile.email
        if not ((profile.mobile == "") | (profile.mobile == " ")):
            profile_exists = profile_exists | Profile.objects.filter(mobile=profile.mobile).exists()
            if (profile_exists):
                print "Mobile exists:: ", profile.mobile

        if(profile_exists):
            # Get profile
            found_profile = Profile.objects.filter(models.Q(email=profile.email) | models.Q(mobile=profile.mobile))[0]

            # Update Details
            profile.id = found_profile.id

            # If non registered user
            if not profile.registered:
                # If user wants to registereded
                if(profile.registered):
                    user = User(username=profile.email, email=profile.email, first_name=profile.first_name, last_name=profile.last_name)
                    user.set_password(password)
                    user.save()
                    profile.user = user
                    profile.save()

                    # Add relationship between customer and business
                    self.add_customer_business_relation(business, profile)

                    # Serialized Data
                    serializer = DeepProfileSerializer(profile)
                    return Response(serializer.data)

                # Non registered user on already on system
                else:
                    # Update any details
                    profile.save()
                    serializer = SimpleProfileSerializer(profile)

                    # Add relationship between customer and business
                    self.add_customer_business_relation(business, profile)

                    return Response(serializer.data)

            # Don't created new profile
            else:
                return Response({'message': 'Email/Mobile address is already in use'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            # If new user wants to registered
            if (request.data["registered"]):
                user = User(username=profile.email)
                user.set_password(request.data["password"])
                user.first_name = request.data["first_name"]
                user.last_name = request.data["last_name"]
                user.email = request.data["email"]
                user.username = request.data["email"]
                user.save()
                profile.user = user
                profile.save()

                # Add relationship between customer and business
                self.add_customer_business_relation(business, profile)

                serializer = DeepProfileSerializer(profile)

                return Response(serializer.data)
            else:
                profile.save()

                # Add relationship between customer and business
                self.add_customer_business_relation(business, profile)

                serializer = SimpleProfileSerializer(profile)
                return Response(serializer.data)


# Create update destroy Revo User
class ProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Profile.objects.all()
    serializer_class = DeepProfileSerializer


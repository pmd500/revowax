from django.contrib.auth.models import User
from django_ical.views import ICalFeed
from django.shortcuts import get_object_or_404

from revo_booker.models import CustomerBooking, Shop, Business, Resource, Staff

class RevoFeed(ICalFeed):
    product_id = '-//revowax.com//kingscross//EN'
    timezone = 'UTC'
    file_name = "revowax-kingscross.ics"
    business = None
    shop = None
    resource = None
    staff = None

    def items(self):
        return CustomerBooking.objects.all().order_by('-start_time')

    def item_title(self, item):
        return item.customer.first_name + " " + item.customer.last_name

    def item_description(self, item):
        return item.business.name + " " + item.shop.name

    def item_start_datetime(self, item):
        return item.start_time

    def item_end_datetime(self, item):
        return item.end_time

    def item_link(self, item):
        return "http://revowax.com"

class BusinessBookingsFeed(RevoFeed):
    def get_object(self, request, business_id):
        self.business = Business.objects.get(pk=business_id)
        self.file_name= unicode(self.business.name + "-bookings.ics").lower()
        self.product_id = unicode('-//revowax.com//'+self.business.name+'-bookings//EN').lower()
        return self

    def items(self):
        return CustomerBooking.objects.filter(business=self.business).order_by('-start_time')


class ShopBookingsFeed(RevoFeed):
    def get_object(self, request, shop_id):
        self.shop = Shop.objects.get(pk=shop_id)
        self.business = self.shop.business
        self.file_name= unicode(self.business.name + "-" +self.shop.name+"-bookings.ics").lower()
        self.product_id = unicode('-//revowax.com//'+self.business.name + "-" +self.shop.name+'-bookings//EN').lower()
        return self

    def items(self):
        return CustomerBooking.objects.filter(shop=self.shop).order_by('-start_time')

class ResourceBookingsFeed(RevoFeed):
    def get_object(self, request, resource_id):
        self.resource = Resource.objects.get(pk=resource_id)
        self.shop = self.resource.shop
        self.business = self.resource.shop.business
        self.file_name= unicode(self.business.name + "-" +self.shop.name+'-'+self.resource.name+"-bookings.ics").lower()
        self.product_id = unicode('-//revowax.com//'+self.business.name + "-" +self.shop.name+'-'+self.resource.name+'-bookings//EN').lower()
        return self

    def items(self):
        return CustomerBooking.objects.filter(resource=self.resource).order_by('-start_time')

class StaffBookingsFeed(RevoFeed):
    def get_object(self, request, staff_id):
        self.staff = Staff.objects.get(pk=staff_id)
        self.business = self.staff.business
        self.file_name = unicode(self.business.name + "-" + self.staff.first_name + "-" + self.staff.last_name + "-bookings.ics").lower()
        self.product_id = unicode('-//revowax.com//' + self.business.name + "-" + self.staff.first_name + "-" + self.staff.last_name +  '-bookings//EN').lower()
        return self

    def items(self):
        return CustomerBooking.objects.filter(staff=self.staff).order_by('-start_time')
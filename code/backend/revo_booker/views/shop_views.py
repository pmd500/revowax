from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from revo_booker.serializers.deep import *
from revo_booker.serializers.simple import *

# Staff for shop
class ShopStaffList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get staff for shop
    def get(self, request, shop_id, format=None):
        shop = Shop.objects.get(shop = shop_id)
        serializer = StaffSerializer(shop.staff.all(), many=True)
        return Response(serializer.data)


# Customers for shop
class ShopCustomerList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get customers for shop
    def get(self, request, shop_id, format=None):
        shop = Shop.objects.get(shop=shop_id)
        serializer = SimpleProfileSerializer(shop.customers.all(), many=True)
        return Response(serializer.data)


# Resource types
class ResourceTypeList(generics.ListCreateAPIView):
    queryset = ResourceType.objects.all()
    serializer_class = ResourceTypeSerializer


# Shop End points
class ShopDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Shop.objects.all()
    serializer_class = DeepShopSerializer


class ShopCreate(generics.CreateAPIView):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer


class ShopOpeningTimesCreate(generics.CreateAPIView):
    queryset = ShopOpeningTimes.objects.all()
    serializer_class = ShopOpeningTimesSerializer


class ShopOpeningTimesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ShopOpeningTimes.objects.all()
    serializer_class = ShopOpeningTimesSerializer


# Resources for shop
class ShopResourcesList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get resources for shop
    def get(self, request, shop_id, format=None):
        resources = Resource.objects.get(shop=shop_id)
        serializer = ResourceSerializer(resources, many=True)
        return Response(serializer.data)

class StaffBookingsList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get bookings for business
    def get(self, request, staff_id, format=None):
        bookings = CustomerBooking.objects.filter(staff=staff_id)
        serializer = CustomerBookingSerializer(bookings, many=True)
        return Response(serializer.data)

# Bookings for shop
class ShopBookingsList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get bookings for shop
    def get(self, request, shop_id, format=None):
        bookings = CustomerBooking.objects.filter(shop=shop_id)
        serializer = CustomerBookingSerializer(bookings, many=True)
        return Response(serializer.data)




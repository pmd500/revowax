var DATE_STRING_FORMAT = "yyyy-mm-dd"
var DAYS_AHEAD = 7; // number of days to look ahead for available appointments


// Profile Types
const PROFILE_TYPES = {
    CUSTOMER: 'C',
    STAFF: 'S',
    MANAGER: 'M',
}

// Booking Statuses
const BOOKING_STATUS = {
    CREATED : 1,
    CONFIRMED : 2,
    CANCELLED : 3,
    COMPLETE : 4,
    NOSHOW : 5
}

// Payment Statuses
const PAYMENT_STATUS = {
    REFUNDED : 1,
    PAID : 2,
    INSTORE : 3,
    ONLINE : 4,
    PENDING : 5
}

var BUSINESS_ID = 1
var SHOP_ID = 3
var GROUP_ID = 1

// Static site (no online booking or dynamic pricing)
var IS_STATIC_SITE = false;
var ACTIVATE_TREATWELL = true;
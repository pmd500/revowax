// revo_app.factory('SalonService', ['$http', function($http) {
//     // Use cached data
//     var salon = {
//         details: {}
//     };
//
//     // Get Salon information
//     salon.GetSalonData = function (salonName, cbSuccess, cbFail){
//         // If static pricing is enabled
//         if(IS_STATIC_SITE) {
//             // mock webservice call
//             $http({
//                 method: 'get',
//                 url: "services/static_responses/clientDetails.json",
//                 // SUCCESS
//             }).then(function successCallback(response) {
//                     cbSuccess(response.data);
//                     salon.details = response.data;
//                 }
//             );
//         }
//         else {
//             $http({
//                 method: 'put',
//                 url: urlBase + ':9876/revoweb/booker/clientDetails',
//                 headers: {
//                     "REVO-PUBLIC-TOKEN": "token"
//                 },
//                 data: {
//                     clientName : salonName
//                 }
//                 // SUCCESS
//             }).then(function successCallback(response) {
//                     cbSuccess(response.data);
//                     salon.details = response.data;
//                 },
//                 // FAIL
//                 function errorCallback() {
//                     cbFail();
//                 });
//         }
//     };
//
//     // Format the list of categories and services with IDs and selected flags
//     salon.GetSelectableServices = function (data) {
//         var local_subcategories;
//         var local_services;
//         var local_categories;
//         var subcategory_selected;
//         var subcategory_css_class;
//         var subcategory_id = 0;
//         var service_id = 0;
//
//         var allTreatments = data.TreatmentCategory.List;
//
//         local_categories = [];
//         // Loop through treatment types
//         for (var i =0; i< allTreatments.length ; i++){
//             treatmentType = allTreatments[i].TreatmentCategory;
//             categories = treatmentType.List;
//
//
//             // Loop through main categories
//             for (var j =0; j< categories.length; j++){
//                 category = categories[j].TreatmentCategory;
//                 subcategories = category.List;
//
//                 local_subcategories = [];
//                 for (var k =0; k< subcategories.length; k++) {
//                     subcategory = subcategories[k].TreatmentCategory;
//                     treatments = subcategory.List;
//                     //console.log("SubCat: " + subcategory.Name)
//
//                     local_services = [];
//                     // Loop through services
//                     for(var n =0; n< treatments.length; n++) {
//                         treatment = treatments[n];
//                         local_services.push(
//                             // Bindings
//                             {
//                                 service_name: treatment.TreatmentName,
//                                 service_price: treatment.TreatmentPrice,
//                                 service_duration: treatment.TreatmentDuration,
//                                 service_db_id: treatment.TreatmentId,
//                                 service_path: treatment.TreatmentPath + "-" + treatment.TreatmentName,
//                                 service_price_currency: treatment.TreatmentPriceCurrency,
//                                 service_selected:false,
//                                 service_num: n,
//                                 service_id: n,
//                                 service_css_class: "w-clearfix menu-list-item"
//                             }
//                         );
//                         // Unique Service ID
//                         service_id = service_id + 1;
//                     }
//
//                     // If first service, select
//                     if((j==0)&&(k==0)) {
//                         subcategory_selected = true;
//                         subcategory_css_class = "w-tab-link w--current w-inline-block drink-tab-button";
//                     }
//                     else {
//                         subcategory_selected = false;
//                         subcategory_css_class = "w-tab-link  w-inline-block drink-tab-button";
//                     }
//                     // Push SubCategory
//                     local_subcategories.push(
//                         {
//                             subcategory_name: subcategory.Name,
//                             subcategory_selected: subcategory_selected,
//                             subcategory_id: subcategory_id,
//                             subcategory_num: k,
//                             subcategory_css_class: subcategory_css_class,
//                             services: local_services
//                         }
//                     )
//
//                     // Unique Subcategory ID
//                     subcategory_id = subcategory_id + 1;
//                 }
//                 local_categories.push(
//                     {
//                         category_name: category.Name,
//                         category_num: j,
//                         category_id: j,
//                         subcategories: local_subcategories
//                     }
//                 )
//             }
//         }
//         return local_categories;
//     }
//
//     return salon;
// }]);
import json
from datetime import datetime, timedelta

from django.http import Http404
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.db.models import Q

from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework import status

from revo_booker.utility import validate_fields, string_to_datetime
from revo_booker.serializers.simple import CustomerBookingSerializer
from revo_booker.serializers.deep import *
from revo_booker.booker import RevoBooker, TreatwellBooker


class BookingAvailability(APIView):
    permission_classes = (AllowAny,)
    def post(self, request,format=None):
        # Check request data
        try:
            business = get_object_or_404(Business, id=request.data["business_id"])
            shop = get_object_or_404(Shop, id=request.data["shop_id"])
            service_group = get_object_or_404(ServiceGroup, id=request.data["service_group_id"])
            duration = request.data["duration"]
            start_datetime = string_to_datetime(request.data["start_date"],
                                                       settings.STANDARD_DATE_STRING_FORMAT)
            end_datetime = string_to_datetime(request.data["end_date"],
                                                     settings.STANDARD_DATE_STRING_FORMAT)
        except Exception:
            content = {'error_message': 'Please specify business_id, shop_id, service_group_id, duration, start_date, end_date'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        # Init RevoBooker
        booker = RevoBooker(business,shop, service_group)

        # Get Available appointments
        available_booking_slots = booker.get_available_appointments(start_datetime, end_datetime, duration)

        return Response(available_booking_slots, content_type='application/json')

# Checks fields of new booking request
def validate_booking_request(request):
    # Validate request fields
    (has_error, error_message) = \
        validate_fields(request,
                        "booked_by_profile_id"
                        "business_id",
                        "customer_profile_id"
                        "shop_id",
                        "staff_id",
                        "resource_id",
                        "service_group_id",
                        "start_datetime",
                        "end_datetime",
                        "duration",
                        "booking_status_id",
                        "payment_status_id",
                        "total_price",
                        "service_list",
                        )
    return has_error, error_message


class NewBooking(APIView):
    permission_classes = (AllowAny,)

    # TODO validate request, check times and dates
    def post(self, request, format=None):
        print json.dumps(request.data, indent=3)
        # Set staff to none if empty
        if request.data["staff_id"] != "":
            staff = Staff.objects.get(id=request.data["staff_id"])
        else:
            staff = None

        # Vaidate Request
        has_error, error_message = validate_booking_request(request)

        if(has_error):
            return Response({'message': error_message}, status=status.HTTP_406_NOT_ACCEPTABLE)

        # Get Details
        business = Business.objects.get(id=request.data["business_id"])
        shop = Shop.objects.get(id=request.data["shop_id"])
        service_group = ServiceGroup.objects.get(id=request.data["service_group_id"])

        # Init RevoBooker
        booker = RevoBooker(business, shop, service_group)

        # try:
        booking = booker.book_appointment(
            booked_by_profile = Profile.objects.get(id=request.data["booked_by_profile_id"]),
            customer_profile = Profile.objects.get(id=request.data["customer_profile_id"]),
            resource = Resource.objects.get(id=request.data["resource_id"]),
            start_datetime = string_to_datetime(
                request.data["start_datetime"],
                settings.STANDARD_DATETIME_STRING_FORMAT
            ),
            end_datetime=string_to_datetime(
                request.data["end_datetime"],
                settings.STANDARD_DATETIME_STRING_FORMAT
            ),
            duration = request.data["duration"],
            booking_status=request.data["booking_status_id"],
            payment_status = request.data["payment_status_id"],
            total_price = request.data["total_price"],
            staff = staff,
            service_list = request.data["service_list"]
        )

        # Appointment slot no longer available
        if(booking == None):
            content = {'message': 'Appointment slot is no longer available, please try a different slot'}
            return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

        # Appointment booked
        else:
            serializer = CustomerBookingSerializer(booking)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

class TreatWellBookingRequest(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        booker = TreatwellBooker()

        # Validate request
        has_error, error_message = booker.validate_treatwell_booking_request(request)

        # Simple validation
        if has_error:
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

        # Creat booking from request
        booking = booker.create_booking_frm_request(request)

        if booking == None:
            return Response({"message": "Cannot link venue name with shop."}, status=status.HTTP_404_NOT_FOUND)
        else:
            serializer = CustomerBookingSerializer(booking)
            return Response({"message": "Treatwell booking added to system", "booking":serializer.data}, status=status.HTTP_201_CREATED)



class UpdateBooking(APIView):
    permission_classes = (AllowAny,)
    """
       Retrieve, update or delete a snippet instance.
       """
    def get_object(self, pk):
        try:
            return CustomerBooking.objects.get(pk=pk)
        except CustomerBooking.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        booking = self.get_object(pk)
        serializer = DeepCustomerBookingSerializer(booking, data=request.data)
        if serializer.is_valid():
            serializer.save()

            # Delete all services for booking
            ServiceCustomerBooking.objects.filter(customer_booking=booking).delete()

            print json.dumps(request.data, indent=3)

            # Re Add Bookings
            RevoBooker.add_services_2_booking(booking, request.data["services"])

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# Booking Details
class BookingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CustomerBooking.objects.all()
    serializer_class = DeepCustomerBookingSerializer

# Deep Service Group Detail
class DeepServiceGroupDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ServiceGroup.objects.all()
    serializer_class = DeepServiceGroupSerializer


# Deep Service Group Create
class DeepServiceGroupCreate(generics.CreateAPIView):
    queryset = ServiceGroup.objects.all()
    serializer_class = DeepServiceGroupSerializer


# Service Group Detail
class ServiceGroupDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ServiceGroup.objects.all()
    serializer_class = ServiceGroupSerializer


# Service Group Create
class ServiceGroupCreate(generics.CreateAPIView):
    queryset = ServiceGroup.objects.all()
    serializer_class = ServiceGroupSerializer


# Service Category Detail
class ServiceCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ServiceCategory.objects.all()
    serializer_class = ServiceCategorySerializer


# Service Category Create
class ServiceCategoryCreate(generics.CreateAPIView):
    queryset = ServiceCategory.objects.all()
    serializer_class = ServiceCategorySerializer


# Service Detail
class ServiceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer


# Service Create
class ServiceCreate(generics.CreateAPIView):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer

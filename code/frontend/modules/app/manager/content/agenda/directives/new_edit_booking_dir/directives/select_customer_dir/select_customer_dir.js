revo_app.directive('agendaCustomerSelect', function(BusinessService, Webworker, UserService) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_customer_dir/select_customer_tmpl.html',
        scope: {
            existing_customers: "=existingCustomers",
            selected_customer: "=selectedCustomer",
            editable: "=",
            booking_details:"=bookingDetails"
        },
        link: function (scope) {
            // Model
            scope.new_customer = {}
            scope.edit_customer = {}
            scope.selected_customer = {};
            scope.error_message = ""

            // Test Customer Day
            scope.search_terms = "";
            scope.split_search_terms = []
            scope.filtered_customers = scope.existing_customers.sort(function(a, b){
                return s.naturalCmp(a.first_name, b.last_name);
            });

            var init_form = function (){
                // If showing edit form
                if(scope.editable) {
                    scope.show_search_user_form = false;
                    scope.show_new_client_form = false;
                    scope.show_user_selected_form = true;
                    scope.show_edit_client_form = false;
                    scope.selected_customer = scope.booking_details.customer
                }
                // New booking form
                else {
                    scope.show_search_user_form = true;
                    scope.show_new_client_form = false;
                    scope.show_user_selected_form = false;
                    scope.show_edit_client_form = false;
                }
            }

            // Multithreaded Search
            function mt_filter_options (input_string){
                function makeString(object) {
                    if (object == null) return '';
                    return '' + object;
                };
                function include(str, needle) {
                    if (needle === '') return true;
                    return makeString(str).indexOf(needle) !== -1;
                };
                var does_string_contain_all_substrings = function(array, string_value){
                    var has_all_strings = true;

                    if(string_value !== null){
                        array.forEach( function(substring){
                            // Is substring present in array
                            has_all_strings = has_all_strings && include(string_value.toLowerCase(), substring.toLowerCase())
                        })
                    }
                    else{
                        has_all_strings = false;
                    }
                    return has_all_strings;
                }

                var input = JSON.parse(input_string)
                var search_terms = input["search_terms"]
                var data = input["data"]
                search_terms  = search_terms.split(" ");

                var results = []
                var filtered_data = data.forEach(function (customer) {
                    var matches_mobile = does_string_contain_all_substrings(search_terms, customer.mobile)
                    var matches_email = does_string_contain_all_substrings(search_terms, customer.email)
                    var matches_first_name = does_string_contain_all_substrings(search_terms, customer.first_name)
                    var matches_last_name = does_string_contain_all_substrings(search_terms, customer.last_name)

                    if((matches_mobile ||  matches_email || matches_first_name || matches_last_name))
                        results.push(customer)

                    // results.push({"first_name": d.first_name,"matches_first_name":matches_first_name, "last_name": d.last_name ,"matches_last_name":matches_last_name, "email": d.email,"matches_email":matches_email, "mobile": d.mobile,  "matches_mobile":matches_mobile })
                    return matches_mobile ||  matches_email || matches_first_name || matches_last_name;
                })

                return JSON.stringify(results);
            }

            // Filtered Data
            var is_searching = false;
            scope.filter_options = function(search_terms, data){
                  if(!is_searching){
                    var myWorker = Webworker.create(mt_filter_options);
                    is_searching = true;
                    myWorker.run(JSON.stringify({"search_terms":search_terms, "data":data})).then(function(output_string) {
                        var filtered_customers = JSON.parse(output_string)
                        scope.filtered_customers = filtered_customers
                        is_searching = false;
                    });
                }
            }

            // Search string for an array of substrings
            var does_string_contain_all_substrings = function(array, string_value){
                var has_all_strings = true;

                if(string_value !== null){
                    _.each(array, function(substring){
                        // Is substring present in array
                        has_all_strings = has_all_strings && s.include(string_value.toLowerCase(), substring.toLowerCase())
                    })
                }
                return has_all_strings;
            }
            
            // Open Create New Client Form
            scope.open_create_new_form = function () {
                scope.show_new_client_form = true
                scope.split_search_terms = scope.search_terms.split(" ")
                if( scope.split_search_terms.length > 0){
                    scope.new_customer.first_name = scope.split_search_terms[0]
                }
                if( scope.split_search_terms.length > 1){
                    scope.new_customer.last_name = scope.split_search_terms[1]
                }
            }
            
            // Open Create New Client Form
            scope.open_edit_client_form = function () {
                // Update Model

                scope.edit_customer = angular.copy(scope.selected_customer);
                l("Selected Customer: ", scope.edit_customer)


                // Show form
                scope.show_edit_client_form = true;
            }
            
            // Validate form
            var validate_customer = function(customer){
                var error_message = ""
                var is_valid=true;
             
                if(customer.first_name == ""){
                    error_message = "Please fill in the first name field";
                    is_valid=false;
                }
                else if (customer.last_name == ""){
                    error_message = "Please fill in the last name field";
                    is_valid=false;
                }
                return {is_valid: is_valid, message:error_message}
            }
            
            // Save new customer
            scope.save_new_customer = function () {
                scope.error_message = ""
                
                var validation_obj = validate_customer(scope.new_customer)
                l(validation_obj)
                if(validation_obj.is_valid){
                    UserService.register(
                        scope.new_customer.first_name,
                        scope.new_customer.last_name,
                        scope.new_customer.email,
                        scope.new_customer.mobile,
                        "",
                        scope.new_customer.receive_promotions,
                        false,
                        // On success 
                        function (data) {
                            scope.post_save_new_customer(data)
                        },
                        // On error
                        function () {
                            
                        }
                    )
                }
                else {
                    scope.error_message = validation_obj.message
                }
            }

            // close form
            scope.post_save_new_customer = function(data){
                scope.selected_customer = data;
                scope.show_search_user_form = false;
                scope.show_new_client_form = false;
                scope.show_user_selected_form = true;

                // Add Customer to Existing Customers
                scope.existing_customers.push(scope.selected_customer)
                
                // Sort Filter Customers
                scope.filtered_customers = scope.existing_customers.sort(function(a, b){
                    return s.naturalCmp(a.first_name, b.last_name);
                });
            }
            
            // Save existing customer
            scope.update_existing_customer = function () {
                scope.error_message = ""

                var validation_obj = validate_customer(scope.new_customer)
                l(validation_obj)
                if(validation_obj.is_valid){
                    UserService.update_profile(
                        scope.edit_customer.id,
                        scope.edit_customer.first_name,
                        scope.edit_customer.last_name,
                        scope.edit_customer.email,
                        scope.edit_customer.mobile,
                        scope.edit_customer.receive_promotions,
                        // On success
                        function () {
                            scope.post_save_edit_customer()
                        },
                        // On error
                        function () {

                        }
                    )
                }
                else {
                    scope.error_message = validation_obj.message
                }
            }

            // Update Client and hide form
            scope.post_save_edit_customer = function () {
                // Update Model
                scope.selected_customer = angular.copy(scope.edit_customer);

                // hide form
                scope.show_edit_client_form = false;
                scope.show_search_user_form = false;
                scope.show_new_client_form = false;
                scope.show_user_selected_form = true;
            }

            scope.show_pop_over = function() {
                scope.pop_over_is_visible = true;
            };

            scope.hide_pop_over = function () {
                scope.pop_over_is_visible = false;
            };

            scope.select_customer = function (customer) {
                scope.selected_customer = customer;

                // Show/Hide relevant forms
                scope.show_search_user_form = false;
                scope.show_new_client_form = false;
                scope.show_user_selected_form = true;

                // Hide pop up
                scope.pop_over_is_visible = false;
            };

            // Add if service if only 1
            scope.add_customer_on_enter = function () {
                if(scope.filtered_customers .length == 1){
                    scope.select_customer(scope.filtered_customers[0]);

                    // Reset search terms and filter
                    scope.pop_over_is_visible = false;
                    scope.search_terms = ""
                    scope.filter_options(scope.search_terms, scope.existing_customers)
                }
                // Assume new user
                else if(scope.filtered_customers.length == 0) {
                    scope.pop_over_is_visible = false;
                    scope.open_create_new_form()
                }
            }

            // Init
            init_form()
        }
    }
});
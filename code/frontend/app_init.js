// Init Angular App
var revo_app = angular.module(
    'revo_app',
    [
        'ui.router',
        'ui.keypress',
        'angularSpinner',
        'uiGmapgoogle-maps',
        'angularShamSpinner', 
        'ngSanitize',
        'ngAnimate',
        'ui.select', 
        'ui.bootstrap', 
        'ui.calendar',
        'ui.validate',
        'uiRouterStyles', 
        'ngCookies',
        'ngWebworker',
        'zumba.angular-waypoints'
    ]);



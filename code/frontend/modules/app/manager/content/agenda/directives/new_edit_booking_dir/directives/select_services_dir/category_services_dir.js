revo_app.directive('categoryServices', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_services_dir/category_services_tmpl.html',
        scope: {
            services: "=",
            level:"=",
            selected_services: "=selectedServices"
        },
        link: function (scope) {
            // Locates player in the selected player array or returns null
            var locate_service = function (service) {
                var array_index = null
                scope.selected_services.forEach(
                    function (selected_service, index) {
                        // console.log("find service ", selected_service.name)
                        // console.log("selected_service: ", selected_service.id, " service: ", service)
                        if(selected_service.id==service.id)
                            array_index = index

                    }
                )
                return array_index
            }

            // Add Service
            scope.add_service = function (service){
                var array_index = locate_service(service)

                // If player is not already selected
                if ((array_index == null))
                    scope.selected_services.push(service)
            }
            
            var indent_string = "--";
            scope.repeated_indent_string = indent_string.repeat(scope.level)
        }
    }
});
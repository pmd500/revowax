revo_app.directive('landing3StepTreatment', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_3_step_treatment_tmpl.html',
    };
});

revo_app.directive('landingContactUs', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_contact_us_tmpl.html',
    };
});

revo_app.directive('landingFixedTopMenu', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_fixed_top_menu_tmpl.html',
    };
});

revo_app.directive('landingFloatingBookNow', function($state) {
    return {
        scope:{
          target:"@",  
        },
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_floating_book_now_tmpl.html',
        link: function (scope) {
            scope.nav_to_booking = function () {
                $state.go(scope.target, {}, { reload: true })
            }
            
        }
    };
});

revo_app.directive('landingFooter', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_footer_tmpl.html',
    };
});

revo_app.directive('landingOurStory', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_our_story_tmpl.html',
    };
});

revo_app.directive('landingSocialMedia', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_social_media_tmpl.html',
    };
});

revo_app.directive('landingTopMenu', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_top_menu_tmpl.html',
    };
});

revo_app.directive('landingVideoBanner', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_video_banner_tmpl.html',
    };
});

revo_app.directive('landingWhyRevoBullets', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_why_revowax_bullets_tmpl.html',
    };
});

revo_app.directive('landingWhyRevoInfo', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/landing/templates/landing_why_revowax_info_tmpl.html',
    };
});

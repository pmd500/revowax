import pytz
from datetime import date,datetime, time, timedelta
from django.utils import timezone

MINUTES_IN_HOUR = 60

# Convert the opening times into an array that can be indexed by the day of the week
def convert_opening_times(ot):
    opening_times_array = [
        # Monday
        {"is_open": ot.monday_is_open, "start": ot.monday_start, "end": ot.monday_end},
        # Tuesday
        {"is_open": ot.tuesday_is_open, "start": ot.tuesday_start, "end": ot.tuesday_end},
        # Wednesday
        {"is_open": ot.wednesday_is_open, "start": ot.wednesday_start, "end": ot.wednesday_end},
        # Thursday
        {"is_open": ot.thursday_is_open, "start": ot.thursday_start, "end": ot.thursday_end},
        # Friday
        {"is_open": ot.friday_is_open, "start": ot.friday_start, "end": ot.friday_end},
        # Saturday
        {"is_open": ot.saturday_is_open, "start": ot.saturday_start, "end": ot.saturday_end},
        # Sunday
        {"is_open": ot.sunday_is_open, "start": ot.sunday_start, "end": ot.sunday_end},
    ]
    return opening_times_array

def string_to_datetime(string, datetime_format):
    # current_tz = timezone.get_current_timezone()
    converted_datetime = datetime.strptime(string,datetime_format).replace(tzinfo=pytz.UTC)
    # converted_datetime_timezone_aware = timezone.make_aware(converted_datetime, timezone.get_current_timezone())
    # converted_datetime_timezone_aware = current_tz.localize(converted_datetime)
    return datetime.strptime(string,datetime_format)

# Convert the availability times into an array that can be indexed by the day of the week
def convert_availabilty_times(at):
    availabilty_times = [
        # Monday
        {"is_available": at.monday_is_available, "start": at.monday_start, "end": at.monday_end},
        # Tuesday
        {"is_available": at.tuesday_is_available, "start": at.tuesday_start, "end": at.tuesday_end},
        # Wednesday
        {"is_available": at.wednesday_is_available, "start": at.wednesday_start, "end": at.wednesday_end},
        # Thursday
        {"is_available": at.thursday_is_available, "start": at.thursday_start, "end": at.thursday_end},
        # Friday
        {"is_available": at.friday_is_available, "start": at.friday_start, "end": at.friday_end},
        # Saturday
        {"is_available": at.saturday_is_available, "start": at.saturday_start, "end": at.saturday_end},
        # Sunday
        {"is_available": at.sunday_is_available, "start": at.sunday_start, "end": at.sunday_end},
    ]
    return availabilty_times

def calculate_time_in_mins(time):
    return (time.hour * MINUTES_IN_HOUR + time.minute)

# Generate days
def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

# Validate request fields
def validate_fields(request, *field_names):
    has_error = False
    error_message = ""
    for field_name in field_names:
        if not field_name in request.data:
            error_message = field_name + " is required; "
    return has_error, error_message
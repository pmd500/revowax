/**
 * Created by admin on 19/01/2016.
 */
revo_app.service("UserDetailsModel", ["$rootScope", function($rootScope) {
    var data = {
        logged_in: true,
        new_first_name: "Phil",
        new_last_name: "Day",
        role: "customer",
        new_email:"fatmansfridge@msn.com",
        mobile:"07506788205",
        username: "phil",
        token: "token",
        user_type:"S"
    };

    // Broadcast change of User Data
    var BroadCastChange = function () {
        $rootScope.$broadcast('UserDetailsModel:updated', data);
    }

    // Universal Getter
    this.GetUserDetails = function () {
        return data;
    }

    // Setters
    this.SetFirstName = function(first_name){
        data.new_first_name = new_first_name;
        BroadCastChange();
    }
    this.SetLastName = function(last_name){
        data.new_last_name = new_last_name;
        BroadCastChange();
    }
    this.SetRole = function(role){
        data.role = role;
        BroadCastChange();
    }
    this.SetRole = function(role){
        data.role = role;
        BroadCastChange();
    }
    this.SetEmail = function(email){
        data.new_email = new_email;
        BroadCastChange();
    }
    this.SetMobile = function(mobile){
        data.mobile = mobile;
        BroadCastChange();
    }
    this.SetUserName = function(username){
        data.username = username;
        BroadCastChange();
    }
    this.SetToken = function(token){
        data.token = token;
        BroadCastChange();
    }
}]);
from django.conf.urls import url,include
from django.contrib import admin

from rest_auth.views import UserDetailsView, PasswordResetConfirmView, PasswordResetView, LoginView, LogoutView, PasswordChangeView
from revo_overrides.rest_auth.password_reset_view_with_html_email import PasswordResetViewWithHtmlEmail

from revo_booker.views import profile_views, business_views, staff_views, shop_views, resource_views, customer_views, booker_views
from revo_booker.feeds import *

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView

class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter

urlpatterns = [
    url(r'^', admin.site.urls),

    # Authentication Endpoints

    # Revo Profile
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^profile/$', profile_views.CreateProfileView.as_view()),
    url(r'^profile/(?P<pk>[0-9]+)/$', profile_views.ProfileDetail.as_view()),
    url(r'^profile/authenticate/$', profile_views.AuthenticateProfileView.as_view()),
    url(r'^profile/password/change/$', PasswordChangeView.as_view(), name='rest_password_change'),
    url(r'^profile/password/reset/$', PasswordResetViewWithHtmlEmail.as_view(), name='rest_password_reset'),
    url(r'^profile/password/reset/confirm/$', PasswordResetConfirmView.as_view(), name='rest_password_reset_confirm'),
    url(r'^profile/login/$', LoginView.as_view(), name='rest_login'),
    url(r'^profile/logout/$', LogoutView.as_view(), name='rest_logout'),
    url(r'^profile/user/$', UserDetailsView.as_view(), name='rest_user_details'),

    # Revo Customer
    url(r'^customer/(?P<customer_id>[0-9]+)/bookings/$', customer_views.CustomerBookingsList.as_view()),

    # Business
    url(r'^businesses/$', business_views.BusinessCreate.as_view()),
    url(r'^businesses/(?P<pk>[0-9]+)/$', business_views.BusinessDetail.as_view()),
    url(r'^businesses/(?P<business_id>[0-9]+)/bookings/$', business_views.BusinessBookingsList.as_view()),
    url(r'^businesses/(?P<business_id>[0-9]+)/staff/$', business_views.BusinessStaffList.as_view()),
    url(r'^businesses/(?P<business_id>[0-9]+)/customers/$', business_views.BusinessCustomerListCreate.as_view()),

    # Shops
    url(r'^shops/$', shop_views.ShopCreate.as_view()),
    url(r'^shops/(?P<pk>[0-9]+)/$', shop_views.ShopDetail.as_view()),
    url(r'^shops/(?P<shop_id>[0-9]+)/bookings/$', shop_views.ShopBookingsList.as_view()),
    url(r'^shops/(?P<shop_id>[0-9]+)/staff/$', shop_views.ShopStaffList.as_view()),
    url(r'^shops/(?P<shop_id>[0-9]+)/resources/$', shop_views.ShopResourcesList.as_view()),
    url(r'^shops/(?P<shop_id>[0-9]+)/customers/$', shop_views.ShopCustomerList.as_view()),
    url(r'^shops/opening_times/$', shop_views.ShopOpeningTimesCreate.as_view()),
    url(r'^shops/opening_times/(?P<pk>[0-9]+)/$', shop_views.ShopOpeningTimesCreate.as_view()),
    # url(r'^shops/link-staff/$', .as_view()),
    # url(r'^shops/link-staff/(?P<pk>[0-9]+)/$', ShopOpeningTimesCreate.as_view()),
    # url(r'^shops/link-service-groups/$', .as_view()),
    # url(r'^shops/link-service-groups/(?P<pk>[0-9]+)/$', .as_view()),

    # Services
    url(r'^service-tree/$', booker_views.DeepServiceGroupCreate.as_view()),
    url(r'^service-tree/(?P<pk>[0-9]+)/$', booker_views.DeepServiceGroupDetail.as_view()),
    url(r'^service-groups/$', booker_views.DeepServiceGroupCreate.as_view()),
    url(r'^service-groups/(?P<pk>[0-9]+)/$', booker_views.ServiceGroupDetail.as_view()),
    url(r'^service-categories/$', booker_views.ServiceCategoryCreate.as_view()),
    url(r'^service-categories/(?P<pk>[0-9]+)/$', booker_views.ServiceCategoryDetail.as_view()),
    url(r'^services/$', booker_views.ServiceCreate.as_view()),
    url(r'^services/(?P<pk>[0-9]+)/$', booker_views.ServiceDetail.as_view()),

    # Resources
    url(r'^resources/$', resource_views.ResourceCreate.as_view()),
    url(r'^resources/(?P<pk>[0-9]+)/$', resource_views.ResourceDetail.as_view()),
    url(r'^resources/(?P<resource_id>[0-9]+)/bookings/$', resource_views.ResourceBookingsList.as_view()),
    url(r'^resources/regular-availability/$', resource_views.ResourceRegularAvailabilityCreate.as_view()),
    url(r'^resources/regular-availability/(?P<pk>[0-9]+)/$', resource_views.ResourceRegularAvailabilityDetail.as_view()),
    url(r'^resources/types/$', resource_views.ResourceTypeList.as_view()),

    # Staff
    url(r'^staff/(?P<staff_id>[0-9]+)/bookings/$', staff_views.StaffBookingsList.as_view()),
    url(r'^staff/regular-availability/$', staff_views.StaffRegularAvailabilityCreate.as_view()),
    url(r'^staff/regular-availability/(?P<pk>[0-9]+)/$', staff_views.StaffRegularAvailabilityDetail.as_view()),
    url(r'^staff/custom-availability/$', staff_views.StaffCustomAvailabilityCreate.as_view()),
    url(r'^staff/custom-availability/(?P<pk>[0-9]+)/$', staff_views.StaffCustomAvailabilityDetail.as_view()),
    url(r'^staff/skills/$', staff_views.SkillList.as_view()),

    # Booker Services
    url(r'^treatwell-booking-request/$', booker_views.TreatWellBookingRequest.as_view()),
    url(r'^booking-availability/$', booker_views.BookingAvailability.as_view()),
    url(r'^new-booking/$', booker_views.NewBooking.as_view()),
    url(r'^update-booking/(?P<pk>[0-9]+)/$', booker_views.UpdateBooking.as_view()),
    url(r'^bookings/(?P<pk>[0-9]+)/$',booker_views.BookingDetail.as_view()),

    # Ical feed for Treatwell
    url(r'^feeds/business-bookings/(?P<business_id>[0-9]+)/$', BusinessBookingsFeed()),
    url(r'^feeds/shop-bookings/(?P<shop_id>[0-9]+)/', ShopBookingsFeed()),
    url(r'^feeds/staff-bookings/(?P<staff_id>[0-9]+)/', StaffBookingsFeed()),
    url(r'^feeds/resource-bookings/(?P<resource_id>[0-9]+)/', ResourceBookingsFeed()),
]


urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]


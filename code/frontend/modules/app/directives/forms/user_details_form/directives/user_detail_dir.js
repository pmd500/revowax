revo_app.directive('userDetail', function() {
        return {
            restrict: 'E',
            scope: {
                ulabel: "@",
                udata: "=",
                editable: "=",
                inputType:"@"
            },
            templateUrl: "modules/app/directives/forms/user_details_form/directives/user_detail_tmpl.html"
        }
    });

// Fade in and fade out directive
revo_app.directive('hideMe', function ($animate){
    return function (scope, element, attrs) {
        scope.$watch(attrs.hideMe, function (newValue) {
            if(newValue) {
                $animate.addClass(element, "fade");
            }
            else {
                $animate.removeClass(element, "fade");
            }
        })
    }
})

// Fade in and out animation
revo_app.animation(".fade", function(){
    return {
        addClass: function(element, className){
            var tween = new TWEEN.Tween(element).to({opacity:0}, 4000);
            tween.start();
        },
        removeClass: function(element, className) {
            var tween = new TWEEN.Tween(element).to({opacity:0}, 4000);
            tween.start();
        }
    }
})

// Configure App
revo_app.config(function($stateProvider) {

    // Customer Landing Page
    $stateProvider
        .state('app.customer.landing', {
            url: "/landing",
            data: {
                css: 'modules/app/directives/landing/landing.css'
            },
            views: {
                "customer_top_menu": {
                    template: ""
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/landing/landing_tmpl.html",
                    controller: "CustomerLandingCtl",
                    controllerAs: "land_ctl"
                }
            },
        })

    // Salon Profile Page
    $stateProvider
        .state('app.customer.salon_profile', {
            url: "/salon-profile",
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/salon/profile/salon_profile_tmpl.html",
                    controller: "CustomerSalonProfileCtl",
                    controllerAs: "sal_pro_ctl"
                }
            },
        })
    
    // Customer User Details
    $stateProvider
        .state('app.customer.user_details', {
            url: "/user-details",
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/user_details/user_details_tmpl.html",
                }
            },
        })
    
    // Customer Bookings
    $stateProvider
        .state('app.customer.bookings', {
            url: "/bookings",
            data: {
                css: 'modules/app/customer/content/bookings/bookings.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/bookings/bookings_tmpl.html",
                    controller: "CustomerBookingsCtl",
                    controllerAs: "bookings_ctl"
                }
            },
        })
    
    //Customer Booking Select Services
    $stateProvider
        .state('app.customer.booking_select_services', {
            url: "/booking-select-services",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/booking/templates/booking_select_services_tmpl.html",
                    controller: "BookingCtl",
                    controllerAs: "booking_ctl"
                }
            },
        })


    //Customer Booking Select Slot
    $stateProvider
        .state('app.customer.booking_select_slot', {
            url: "/booking-select-slot",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/booking/templates/booking_select_slot_tmpl.html",
                    controller: "BookingCtl",
                    controllerAs: "booking_ctl"
                }
            },
        })

    // Customer Booking UserDetails
    $stateProvider
        .state('app.customer.booking_user_details', {
            url: "/booking-user-details",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/booking/templates/booking_user_details_tmpl.html",
                    // controller: "BookingStage4Ctl",
                    // controllerAs: "booking_stage4_ctl"
                }
            },
        })

    // Customer Booking Overview
    $stateProvider
        .state('app.customer.booking_overview', {
            url: "/booking-overview",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/booking/templates/booking_overview_tmpl.html",
                    // controller: "BookingStage4Ctl",
                    // controllerAs: "booking_stage4_ctl"
                }
            },
        })

    // Customer Booking Payment
    $stateProvider
        .state('app.customer.booking_payment', {
            url: "/booking-payment",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/booking/templates/booking_payment_tmpl.html",
                }
            },
        })
    
    // Customer Booking Confirmation
    $stateProvider
        .state('app.customer.booking_confirmation', {
            url: "/booking-confirmation",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/customer/content/booking/templates/booking_confirmation_tmpl.html",
                }
            },
        })

    // Customer Booking Problem
    $stateProvider
        .state('app.customer.booking_problem', {
            url: "/booking-problem",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "customer_top_menu": {
                    templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
                    controller: "CustomerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "customer_content": {
                    templateUrl: "modules/app/shared/content/booking/templates/booking_problem_tmpl.html",
                }
            },
        })
    //     })
});


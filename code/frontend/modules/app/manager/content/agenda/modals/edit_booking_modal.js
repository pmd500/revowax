// For creating new appointments
revo_app.controller('EditAppointmentModalCtl', function ($scope, $uibModalInstance, ShopService, BookingService, ExistingCustomers,RevowaxShopDetails, Start, End, Id, BookingDetails) {
    // Bind Data
    $scope.existing_customers = ExistingCustomers;
    $scope.shop_details = RevowaxShopDetails;
    $scope.start = Start;
    $scope.end = End;
    $scope.id = Id;
    $scope.modal_instance = $uibModalInstance;
    $scope.booking_details = BookingDetails;

});
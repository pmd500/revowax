# from __future__ import print_function
import boto3, json, urllib
from email import message_from_string
import json, requests

ses = boto3.client('ses')
s3 = boto3.client('s3')

email_from = 'new-bookings@revobooker.com'
email_to = 'philipmatthewday@gmail.com'
email_subject = 'AWS Lambda Test'
email_body = 'Hello lambda'


def lambda_handler(event, context):
    # Extracts a string between two strings from within a text string
    def extract_string_between(message, string1, string2):
        try:
            value = message.split(string1)[1].split(string2)[0].strip()
        except:
            value = "*** FAILED ***"
        return value

    def parse_services(msg):
        services_text = extract_string_between(msg, "Appointment", "Venue")
        durations = []
        services = []
        categories = []
        category = ""
        for line in services_text.splitlines():
            if ("mins" in line):
                category = line.split("(")[0].strip()
                durations.append(extract_string_between(line, "(", "mins"))
                categories.append(category)
            else:
                services.append({"name": line, "category": category})

        duration = reduce(lambda a, b: int(a) + int(b), durations)

        services_string = ""
        for category in categories:
            # check if category has one service
            # has_one_service = reduce(lambda s1,s2: (category in s1["category"]) | (category in s2["category"]), services)
            has_one_service = len(filter(lambda s: (category == s["category"]), services)) > 0

            # if cateogory is actually a service
            if not has_one_service:
                services.append({"name": category, "category": category})

        services_string = reduce(lambda s1, s2: s1 + ", " + s2, map(lambda s: s["name"], services))
        return services_string, duration

    key = event["Records"][0]['s3']['object']['key']
    print key
    response = s3.get_object(Bucket="treatwell-emails", Key=key)

    # Get Messafe
    msg = message_from_string(response['Body'].read())
    subject = msg["subject"]
    msg = str(msg)

    # Is Booking Email from Treatwell? Then parse email
    if (("Treatwell" in msg) and ("Booking Details" in msg)):
        # Parse Data
        appointment_details = extract_string_between(msg, "Appointment", "Accept")
        date = extract_string_between(appointment_details, "Date/time", "at")
        datetime_string = extract_string_between(appointment_details, "Date/time", "Price")
        time = extract_string_between(datetime_string, "at", "Price")
        service_price = extract_string_between(appointment_details, "=C2=A3", "with")
        payment_status = appointment_details.split("Status")[1].strip()
        booking_details = extract_string_between(msg, "*Booking Details*", "Quantity")
        treatwell_ref_number = extract_string_between(subject, "Our Ref. ", ")")
        venue = extract_string_between(booking_details, "Venue:", "Product Name: ")
        price = extract_string_between(booking_details, "=C2=A3", "Guest Email: ")
        email = extract_string_between(booking_details, "Guest Email: ", "Guest Tel.:")
        mobile = extract_string_between(booking_details, "Guest Tel.:", "Quantity")
        services, duration = parse_services(msg)
        booked_at = extract_string_between(msg, "Booked:", "Source")
        source = extract_string_between(msg, "Source:", "Order ref")
        accept_link = extract_string_between(msg, "Accept", "/>").replace('\r\n', '') + "/>"
        reschedule_link = extract_string_between(msg, "Reschedule", "/>").replace('\r\n', '') + "/>"
        booking_link = extract_string_between(msg, "Open appointment in Treatwell Connect", "/>").replace('\r\n',
                                                                                                          '') + "/>"
        guest_name = extract_string_between(msg, "Guest name", "Open appointment in Treatwell Connect")

        payload = {
            "booking_ref": treatwell_ref_number,
            "venue": venue.strip(),
            "date": date,
            "time": time,
            "price": price.strip(),
            "email": email.strip(),
            "mobile": mobile.strip(),
            "services": services,
            "duration": duration,
            "payment_status": payment_status,
            "booked_at": booked_at,
            "source": source,
            "accept_link": accept_link,
            "reschedule_link": reschedule_link,
            "booking_link": booking_link,
            "guest_name": guest_name
        }

        # Create booking on RevoBooker
        url = 'http://dev.api.revowax.com/treatwell-booking-request/'
        headers = {'content-type': 'application/json'}
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        print response
    else:
        print "Non treatwell email detected"


revo_app.directive('agendaDurationSelect', function(UtilityService) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_duration_dir/select_duration_tmpl.html',
        scope: {
            start:"=",
            end : "=",
            editable: "=",
            selected_services : "=selectedServices"
        },
        link: function (scope) {
            var generate_durations = function () {
                var min_interval = 5;
                var max_duration = 240; // 8 hours
                var iterations = max_duration / min_interval;

                var intervals = []
                for (var i = 1; i <= iterations; i++){
                    var minutes = i*min_interval;
                    var hours = Math.floor(minutes / 60);
                    var left_minutes = minutes % 60;
                    var duration_string = (hours > 0) ? (hours + " hr " + left_minutes + " min"): (left_minutes + " min");
                    intervals.push({minutes:minutes, duration_string:duration_string})
                }
                return intervals
            }

            var find_duration = function (minutes) {
                var found_duration = null;
                scope.durations.forEach(function (duration) {
                    if(duration.minutes == minutes){
                        found_duration = duration
                    }
                })
                // if not found, return null
                return found_duration
            }

            // Set duration on drop down menu
            var  set_duration = function() {
                // l("start_date", start_date)
                // l("end_date", end_date)
                var calculated_duration = UtilityService.date_methods.calculate_time_difference(start_date, end_date)
                var found_duration = find_duration(calculated_duration)
                if(found_duration != null)
                    scope.duration.selected = found_duration;
                    scope.selected_duration = found_duration;
            }

            // Watch for the selected services changed
            scope.$watchCollection('selected_services', function(newValue, oldValue) {
                if (newValue)
                    console.log("I see a data change in child");
            });

            // // FIXEME TIMEZONE ISSUES
            var start_date = new Date(scope.start.toDate())
            var end_date = new Date(scope.end.toDate())
            if (scope.editable){
                start_date.setHours((start_date.getHours()), start_date.getMinutes(), 0);
                end_date.setHours((end_date.getHours()), end_date.getMinutes(), 0);
            }
            else {
                start_date.setHours((start_date.getHours()-1), start_date.getMinutes(), 0);
                end_date.setHours((end_date.getHours()-1), end_date.getMinutes(), 0);
            }

            // Generation drop down options
            scope.duration = {}
            scope.durations = generate_durations()

            // Select Duration
            set_duration()
        }
    }
});
revo_app.controller("AnonymousCtl", ["$scope", "$state", "UserService", function($scope, $state, UserService){
    var ctl = this
    
    // Add environment variables
    ctl.activate_treatwell_bookings = ACTIVATE_TREATWELL;
    
    return ctl;
}]);
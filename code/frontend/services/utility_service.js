/**
 Name:
 utility_service.js

 Description:
 Contains methods that are used by multiple controllers and services. The following are contained in this file
    - Start and stop methods for the loading spinner
    - Useful date methods
**/
p = console.log

revo_app.factory("UtilityService", ["usSpinnerService", "AppStateService","$q","$http",  function (usSpinnerService, AppStateService, $q, $http) {
    // Bind to var
    var service = this;

    // Spinner Start Stop Methods
    service.spinner = {
        start: function () {
            usSpinnerService.spin('spinner-1');
        },
        stop: function () {
            usSpinnerService.stop('spinner-1');
        }
    };

    // Collection of useful date related methods
    service.date_methods = {
        // Must be in YYYY-MM-DD format
        extract_year_frm_date_string: function(string_date){
            return parseInt(string_date.substr(0, 4))
        },
        // Must be in YYYY-MM-DD format
        extract_month_frm_date_string: function(string_date){
            return parseInt(string_date.substr(5, 7))
        },
        // calculate number of minutes between two dates (result in minutes)
        calculate_time_difference: function (start_datetime, end_datetime) {
            var start_time = start_datetime;
            var end_time = end_datetime;
            // This will give difference in milliseconds
            var difference = end_time.getTime() - start_time.getTime();
            return Math.round(difference / 60000);
        },
        // Changes just the date of a datetime, keeps time the same.
        change_date_of_datetime: function(old_datetime, new_datetime){
            return new Date(
                new_datetime.getFullYear(),
                new_datetime.getMonth(),
                new_datetime.getDate(),
                old_datetime.getHours(),
                old_datetime.getMinutes(),
                old_datetime.getSeconds(),
                old_datetime.getMilliseconds()
            );
        },
        // Changes just the time of a datetime, keeps date the same.
        change_time_of_datetime: function(old_datetime, new_datetime){
            return new Date(
                old_datetime.getFullYear(),
                old_datetime.getMonth(),
                old_datetime.getDate(),
                new_datetime.getHours(),
                new_datetime.getMinutes(),
                new_datetime.getSeconds(),
                new_datetime.getMilliseconds()
            );
        },
        calc_end_date_time: function(start_datetime, durations_minutes){
            return new Date(start_datetime.getTime() + durations_minutes*60000);
        }
    };

    service.api_utils = {
        // Get Request
        get: function (relative_url, success_callback, error_callback) {
            // Start loading spinner
            usSpinnerService.spin('spinner-1')

            // Call shop webservice
            $http({
                method: 'get',
                url: relative_url,
                headers: {
                    'Authorization': "Token " + AppStateService.user_state.token,
                },
            })
            .then(
                // SUCCESS
                function success_callback(response) {
                    // Default Stop Spinner
                    usSpinnerService.stop('spinner-1');

                    console.log(response.data)
                    // Call success callback
                    success_callback(response.data);
                },

                // FAIL
                function fail_callback(response) {
                    // Default Stop Spinner
                    usSpinnerService.stop('spinner-1');

                    // Call back
                    error_callback(response);
                }
            );
        },

        // Deferred get
        get_deferred : function (relative_url) {
            var deferred = $q.defer();

            // Start loading spinner
            usSpinnerService.spin('spinner-1')

            // Call shop webservice
            $http({
                method: 'get',
                url: relative_url,
                headers: {
                    'Authorization': "Token " + AppStateService.user_state.token,
                },
            })
            .then(
                // SUCCESS
                function success_callback(response) {
                    usSpinnerService.stop('spinner-1');
                    deferred.resolve(response.data);
                },

                // FAIL
                function fail_callback(response) {
                    usSpinnerService.stop('spinner-1');
                    deferred.reject(response);
                }
            );

            return deferred.promise;
        },

        // Post Request
        post: function (relative_url, data, success_callback, error_callback) {
            // Start loading spinner
            usSpinnerService.spin('spinner-1')

            // Call shop webservice
            $http({
                method: 'get',
                url: base_url + relative_url,
                headers: {
                    'Authorization': "Token " + AppStateService.user_state.token,
                },
                data: data
            })
            .then(
                // SUCCESS
                function success_callback(response) {
                    // Default Stop Spinner
                    usSpinnerService.stop('spinner-1');

                    // Call success callback
                    success_callback(response.data);
                },
                // FAIL
                function fail_callback(response) {
                    // Default Stop Spinner
                    usSpinnerService.stop('spinner-1');

                    // Call back
                    error_callback(response);
                }
            );
        }
    }

    return service
}]);

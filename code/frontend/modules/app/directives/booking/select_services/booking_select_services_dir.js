revo_app.directive('bookingSelectServices', function(AppStateService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/select_services/booking_select_services_tmpl.html',
        scope: {
            // variables
            shop:"=",
            // next state
            next:"@"
        },
        link : function (scope) {
            // Show/hide "please
            scope.show_message = false;

            // Contains selected services
            scope.selected_services = [];

            // Calculates Total Price
            scope.total_price = 0;
            scope.total_duration = 0;

            // Go to next stage of booking and update model
            scope.continue = function (){
                AppStateService.customer_state.selected_services = scope.selected_services
                AppStateService.customer_state.total_price = scope.total_price
                AppStateService.customer_state.total_duration = scope.total_duration

                // check if atleast one service has been selected
                if(scope.selected_services.length > 0)
                    // call external function
                    $state.go(scope.next)
                else
                    scope.show_message = true;
            }
        }
    };
});
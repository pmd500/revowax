from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from revo_booker.serializers.deep import *


# Staff for business
class BusinessStaffList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get staff for business
    def get(self, request, business_id, format=None):
        staff = Staff.objects.filter(business=business_id)
        serializer = StaffSerializer(staff, many=True)
        return Response(serializer.data)

# Customers for business
class BusinessCustomerListCreate(APIView):
    permission_classes = (IsAuthenticated,)

    # Get customers for business
    def get(self, request, business_id, format=None):
        business = Business.objects.get(id = business_id)
        customers = business.customers.all()
        serializer = SimpleProfileSerializer(customers, many=True)

        return Response(serializer.data)

    # A new customer to business
    def post(self, request, business_id, format=None):
        business = Business.objects.get(business=business_id)
        serializer = CustomerSerializer(request.data)

        if serializer.is_valid():
            # Save Customer
            serializer.save()

            # Add customer to business
            business.customers.add(serializer.validated_data)
            business.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Business Detail
class BusinessDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Business.objects.all()
    serializer_class = DeepBusinessSerializer

# Create Business
class BusinessCreate(generics.CreateAPIView):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer

# Bookings for business
class BusinessBookingsList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get bookings for business
    def get(self, request, business_id, format=None):
        bookings = CustomerBooking.objects.filter(business=business_id)
        serializer = CustomerBookingSerializer(bookings, many=True)
        return Response(serializer.data)


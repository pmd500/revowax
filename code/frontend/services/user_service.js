revo_app.factory('UserService', ['$http', "$cookies", "$rootScope", "usSpinnerService", "$q", "AppStateService", "UtilityService", function ($http, $cookies, $rootScope, usSpinnerService, $q, AppStateService, UtilityService) {
    var service = this;

    // Sets token in cookie
    var set_cookie_credentials = function (token) {
        $cookies.put('revo_token', token);
    };

    // Clears token in cookie
    var clear_cookie_credentials = function () {
        $cookies.remove('revo_token');
    }

    // login Get Token
    service.login = function (username, password, success_external_callback, fail_external_callback) {
        UtilityService.spinner.start()
        console.log("calling login service: ", username, password)
        $http({
            method: 'post',
            url: base_url + 'profile/login/',
            data: {
                "username": username,
                "password": password
            }
        })
        .then(
            // SUCCESS
            function success_callback(response) {
                UtilityService.spinner.stop()

                AppStateService.set_log_on_status_4_user(response.data.key)

                // Save token
                set_cookie_credentials(response.data.key)

                service.authenticate_with_token(response.data.key, success_external_callback, fail_external_callback)
            },
            // FAIL
            function fail_callback(response) {
                UtilityService.spinner.stop()

                fail_external_callback(response);
            }
        );
    };

    // Register New User
    service.register = function (first_name, last_name, email, mobile, password, receive_promotions, register, success_external_callback, fail_external_callback) {
        UtilityService.spinner.start()
        $http({
            method: 'post',
            url: base_url + 'profile/',
            data: {
                "first_name": first_name,
                "last_name": last_name,
                "profile_type": "C",
                "email": (email == null) ? " " : email,
                "mobile": (mobile == null) ? " " : mobile,
                "password": password,
                "receive_promotions": (receive_promotions != null) ? receive_promotions : false,
                "registered": register,
                "business": BUSINESS_ID
            }
        })
        .then(
            // SUCCESS
            function success_callback(response) {
                // If registered, log user on
                if(register) {
                    service.login(email, password, success_external_callback, fail_external_callback)
                }
                else {
                    // Register non registered details
                    // AppStateService.set_user_state_frm_response_data(response.data, false, null)
                    UtilityService.spinner.stop()
                    success_external_callback(response.data)
                }
            },
            // FAIL
            function fail_callback(response) {
                UtilityService.spinner.stop()
                l(response)
                fail_external_callback(response);
            }
        );
    };

    // Update Profile
    service.update_profile = function (id, first_name, last_name, email, mobile, receive_promotions, success_external_callback, fail_external_callback) {
        UtilityService.spinner.start()
        console.log("PROFILE ID: ", id)
        $http({
            method: 'patch',
            url: base_url + 'profile/'+id +"/",
            data: {
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "mobile": mobile,
                "receive_promotions": receive_promotions
            }
        })
            .then(
                // SUCCESS
                function success_callback(response) {
                    UtilityService.spinner.stop()
                    success_external_callback()
                },
                // FAIL
                function fail_callback(response) {
                    UtilityService.spinner.stop()
                    fail_external_callback(response);
                }
            );
    };

    // // Delete Profile
    // service.delete_profile = function (new_first_name, new_last_name, new_email, mobile, success_external_callback, fail_external_callback) {
    //     UtilityService.spinner.start()
    //     $http({
    //         method: 'delete',
    //         url: base_url + 'profile/'+AppStateService.user_state.profile_id +"/",
    //         data: {
    //             "new_first_name": new_first_name,
    //             "new_last_name": new_last_name,
    //             "new_email": new_email,
    //             "mobile": mobile,
    //         }
    //     })
    //         .then(
    //             // SUCCESS
    //             function success_callback(response) {
    //                 service.log_out()
    //                 UtilityService.spinner.stop()
    //                 success_external_callback()
    //             },
    //             // FAIL
    //             function fail_callback(response) {
    //                 UtilityService.spinner.stop()
    //                 fail_external_callback(response);
    //             }
    //         );
    // };
    //
    // Authenticate - get user details from token
    service.authenticate_with_token = function (token, success_external_callback, fail_external_callback) {
        // Start Loading Spinner
        UtilityService.spinner.start()

        // Call login Webservice
        $http({
            method: 'get',
            url: base_url + 'profile/authenticate/',
            headers: {
                'Authorization': "Token " + token,
            },
        })
            .then(
                // SUCCESS
                function success_callback(response) {
                    // Default Stop Spinner
                    UtilityService.spinner.stop()

                    // Update User State
                    AppStateService.set_user_state_frm_response_data(response.data, true, token)

                    // Call success call back
                    success_external_callback(response.data);
                },

                // FAIL
                function fail_callback(response) {
                    console.log("Auth Fail")
                    console.log(response)
                    // Default Stop Spinner
                    UtilityService.spinner.stop()

                    // Call Back
                    fail_external_callback();
                }
            );
    };

    // Authenticate - get user details from token
    service.authenticate_from_cookies = function (success_external_callback, fail_external_callback) {
        // If not already logged in
        if (!service.logged_on) {
            // Get token from cookie
            var cookie_token = $cookies.get('revo_token');

            if (cookie_token !== undefined) {

                // Start loading spinner
                UtilityService.spinner.start()
                $http({
                    method: 'get',
                    url: base_url + 'profile/authenticate/',
                    headers: {
                        'Authorization': "Token " + cookie_token,
                    },
                })
                    .then(
                        // SUCCESS
                        function success_callback(response) {
                            // Default Stop Spinner
                            UtilityService.spinner.stop()

                            console.log("Authenticate from cookie success")

                            // Update User State
                            AppStateService.set_user_state_frm_response_data(response.data, true, cookie_token)

                            // Call success callback
                            success_external_callback();

                        },

                        // FAIL
                        function fail_callback(response) {
                            // Default Stop Spinner
                            UtilityService.spinner.stop()

                            // Call back
                            fail_external_callback();

                        }
                    );
            }
            else {
                // No valid token in cookies
                fail_external_callback()
            }
        }
        else {
            // User already logged in
            success_external_callback();
        }

    };

    // Check if authenticated
    service.check_authenticated = function () {
        var deferred = $q.defer();

        if (AppStateService.get_user_state().logged_on) {
            deferred.resolve(AppStateService.get_user_state());
        }

        else {
            service.authenticate_from_cookies(
                // Success
                function () {
                    AppStateService.nav_to_anonymous_landing_if_not_logged_in()
                    deferred.resolve(AppStateService.get_user_state());
                },
                // Failed
                function () {
                    AppStateService.navigate_to_profile_home()
                    deferred.reject();
                }
            )
        }
        return deferred.promise;
    };

    // Deletes cookies and clears user details
    service.log_out = function () {
        clear_cookie_credentials();
        AppStateService.clear_user_state();
    };


    // // Deletes cookies and clears user details
    // service.log_out = function (call_back) {
    //     // Start loading spinner
    //     UtilityService.spinner.start()
    //     // Call Webservice
    //     $http({
    //         method: 'post',
    //         headers: {
    //             'Authorization': "Token " + AppStateService.user_state.token,
    //         },
    //         url: base_url + 'authentication/logout/'
    //     })
    //         .then(
    //             // SUCCESS
    //             function successCallback() {
    //                 // Stop Loading Spinner
    //                 UtilityService.spinner.stop()
    //
    //                 // Clear cookie
    //                 clear_cookie_credentials();
    //
    //                 // Clear User Details
    //                 clear_user_details();
    //
    //                 // CallBack
    //                 call_back();
    //             },
    //             // FAIL
    //             function errorCallback(response) {
    //                 // Stop Loading Spinner
    //                 UtilityService.spinner.stop()
    //
    //                 // Clear cookie
    //                 clear_cookie_credentials();
    //
    //                 // Clear User Details
    //                 clear_user_details();
    //
    //                 // CallBack
    //                 call_back();
    //             }
    //         )
    // }

    // send reset password new_email
    service.reset_password = function (email, success_cb, fail_cb) {
        // Start loading spinner
        UtilityService.spinner.start()

        // Call Webservice
        $http({
            method: 'post',
            url: base_url + 'profile/password/reset/',
            data: {
                "new_email": new_email
            }
        })
            .then(
                // SUCCESS
                function successCallback(response) {
                    // Stop Loading Spinner
                    UtilityService.spinner.stop()

                    console.log("response")
                    console.log(response)

                    // CallBack
                    success_cb(response);
                },
                // FAIL
                function errorCallback(response) {
                    // Stop Loading Spinner
                    UtilityService.spinner.stop()

                    // CallBack
                    fail_cb(response);
                }
            )
    };

    // resets password
    service.reset_password_confirmation = function (new_password1,new_password2, uid, token, success_cb, fail_cb) {
        // Start loading spinner
        UtilityService.spinner.start()

        // Call Webservice
        $http({
            method: 'post',
            url: base_url + 'profile/password/reset/confirm/',
            data: {
                "new_password1": new_password1,
                "new_password2": new_password2,
                "uid": uid,
                "token": token
            }
        })
            .then(
                // SUCCESS
                function successCallback(response) {
                    // Stop Loading Spinner
                    UtilityService.spinner.stop()

                    // CallBack
                    success_cb(response);
                },
                // FAIL
                function errorCallback(response) {
                    // Stop Loading Spinner
                    UtilityService.spinner.stop()

                    console.log(response)
                    // CallBack
                    fail_cb(response);
                }
            )
    };

    // change password
    service.change_password = function (old_password, new_password1,new_password2, success_cb, fail_cb) {
        // Start loading spinner
        UtilityService.spinner.start()

        // Call Webservice
        $http({
            method: 'post',
            url: base_url + 'profile/password/change/',
            headers: {
                'Authorization': "Token " + AppStateService.user_state.token,
            },
            data: {
                "old_password": old_password,
                "new_password1": new_password1,
                "new_password2": new_password2
            }
        })
            .then(
                // SUCCESS
                function successCallback(response) {
                    // Stop Loading Spinner
                    UtilityService.spinner.stop()

                    // CallBack
                    success_cb(response);
                },
                // FAIL
                function errorCallback(response) {
                    // Stop Loading Spinner
                    UtilityService.spinner.stop()

                    // CallBack
                    fail_cb(response);
                }
            )
    };
    
    return service;

}]);
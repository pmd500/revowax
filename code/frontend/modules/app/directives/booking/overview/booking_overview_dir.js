revo_app.directive('bookingOverview', function(AppStateService,BookingService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/overview/booking_overview_tmpl.html',
        scope: {
            // variables
            shop:"=",
            // redirect to state
            redirect:"@",
            // success state
            success:"@",
            // failure state
            error:"@"
        },
        link : function (scope) {
            // Check there there is data otherwise redirect
            if(AppStateService.customer_state.selected_slot == null)
                $state.go(scope.redirect)

            // Bind appointment details
            scope.selected_slot = AppStateService.customer_state.selected_slot
            scope.selected_services = AppStateService.customer_state.selected_services
            scope.total_price = AppStateService.customer_state.total_price
            scope.total_duration = AppStateService.customer_state.total_duration
            scope.start_date =  new Date(scope.selected_slot.start)

            // Continue
            scope.book = function (){
                // Book
                BookingService.book(
                    // Booked By Profile ID
                    AppStateService.user_state.id,
                    // Booked By Customer ID
                    AppStateService.user_state.id,
                    // Business_id
                    BUSINESS_ID,
                    // shop_id
                    SHOP_ID,
                    // resource_id
                    scope.selected_slot.resource_id,
                    // service_group_id
                    GROUP_ID,
                    // staff_id
                    "",
                    // start_datetime
                    scope.selected_slot.start,
                    // end_datetime
                    scope.selected_slot.end,
                    // duration
                    scope.total_duration,
                    // booking_status_id
                    BOOKING_STATUS.CONFIRMED,
                    // payment_status_id
                    PAYMENT_STATUS.PENDING,
                    // total_price
                    scope.total_price,
                    // service_list
                    scope.selected_services,
                    // Success
                    function(data){
                        $state.go(scope.success)

                    },
                    // Error
                    function(){
                        $state.go(scope.error)
                    }
                )
            }
        }
    };
});
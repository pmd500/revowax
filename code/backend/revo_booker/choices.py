from djchoices import DjangoChoices, ChoiceItem

class ProfileTypes(DjangoChoices):
   customer = ChoiceItem('C', 'Customer')
   staff = ChoiceItem('S', 'Staff')
   manager = ChoiceItem('M', 'Manager')

class Gender(DjangoChoices):
   male = ChoiceItem('M', 'Male')
   female = ChoiceItem('F', 'Female')

class BookingSources(DjangoChoices):
   treatwell_widget = ChoiceItem('TW', 'Treatwell Widget')
   treatwell_website = ChoiceItem('TO', 'Treatwell Online')
   revowax_website = ChoiceItem('RO', 'RevoWax Online')
   revowax_walkin = ChoiceItem('WI', 'Walk in')

class DaysOfWeek(DjangoChoices):
    monday = ChoiceItem(0, 'Monday')
    tuesday = ChoiceItem(1, 'Tuesday')
    wednesday = ChoiceItem(2, 'Wednesday')
    thursday = ChoiceItem(3, 'Thursday')
    friday = ChoiceItem(4, 'Friday')
    saturday = ChoiceItem(5, 'Saturday')
    sunday = ChoiceItem(6, 'Sunday')


class AvailabilityTypes(DjangoChoices):
   extrashift = ChoiceItem(0, 'Extra Shift')
   holiday = ChoiceItem(1, 'Holiday')
   illness = ChoiceItem(2, 'Illness')
   training = ChoiceItem(3, 'Training')
   other = ChoiceItem(4, 'Other')


class BookingStatus(DjangoChoices):
    created = ChoiceItem(1, 'created')
    requested = ChoiceItem(1, 'Requested')
    confirmed = ChoiceItem(2, 'Confirmed')
    cancelled = ChoiceItem(3, 'Cancelled')
    complete = ChoiceItem(4, 'Complete')
    noshow = ChoiceItem(5, 'No Show')


class PaymentStatus(DjangoChoices):
    refunded = ChoiceItem(1, 'Refunded')
    paid = ChoiceItem(2, 'Paid')
    instore = ChoiceItem(3, 'Instore')
    online = ChoiceItem(4, 'Online')
    pending = ChoiceItem(5, 'Pending')
    unknown = ChoiceItem(5, 'Unknown')
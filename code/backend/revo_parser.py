# Import the email modules we'll needd
from email.parser import Parser
from email import message_from_file

file_path="revo_parser/original_msg5.txt"
headers = Parser().parse(open(file_path, 'r'))
msg = message_from_file(open(file_path, 'r'))

subject = headers['subject']
# msg = str(msg).replace('\r\n', ' ')
msg = str(msg)

# Extracts a string between two strings from within a text string
def extract_string_between(message, string1, string2):
    try:
        value = message.split(string1)[1].split(string2)[0].strip()
    except:
        value = "*** FAILED ***"
    return value

def parse_services(msg):
    services_text = extract_string_between(msg, "Appointment", "Venue")
    durations = []
    services = []
    categories = []
    category = ""
    for line in services_text.splitlines():
        if ("mins" in line):
            category = line.split("(")[0].strip()
            durations.append(extract_string_between(line, "(", "mins"))
            categories.append(category)
        else:
            services.append({"name": line, "category": category})

    duration = reduce(lambda a, b: int(a) + int(b), durations)

    services_string = ""
    for category in categories:
        # check if category has one service
        # has_one_service = reduce(lambda s1,s2: (category in s1["category"]) | (category in s2["category"]), services)
        has_one_service = len(filter(lambda s: (category == s["category"]), services)) > 0

        # if cateogory is actually a service
        if not has_one_service:
            services.append({"name": category, "category": category})

    services_string = reduce(lambda s1, s2: s1 + ", " + s2, map(lambda s:s["name"], services))
    return services_string, duration

# Parse Data
appointment_details = extract_string_between(msg, "Appointment","Accept" )
date = extract_string_between(appointment_details, "Date/time","at")
datetime_string = extract_string_between(appointment_details, "Date/time","Price")
time = extract_string_between(datetime_string, "at","Price" )
service_price = extract_string_between(appointment_details, "=C2=A3","with" )
payment_status = appointment_details.split("Status")[1].strip()
booking_details = extract_string_between(msg, "*Booking Details*", "Quantity")
treatwell_ref_number = extract_string_between(subject, "Our Ref. ", ")")
venue = extract_string_between(booking_details, "Venue:", "Product Name: ")
price = extract_string_between(booking_details, "=C2=A3", "Guest Email: ")
email = extract_string_between(booking_details,"Guest Email: ", "Guest Tel.:")
mobile = extract_string_between(booking_details,"Guest Tel.:", "Quantity")
services, duration = parse_services(msg)
booked_at = extract_string_between(msg,"Booked:", "Source")
source = extract_string_between(msg,"Source:", "Order ref")
accept_link = extract_string_between(msg,"Accept", "/>").replace('\r\n', '') + "/>"
reschedule_link = extract_string_between(msg,"Reschedule", "/>").replace('\r\n', '') + "/>"
booking_link = extract_string_between(msg,"Open appointment in Treatwell Connect", "/>").replace('\r\n', '') + "/>"
guest_name = extract_string_between(msg, "Guest name", "Open appointment in Treatwell Connect")



result_json = {
    "Treatwell Booking: (", treatwell_ref_number , ")",
    "venue:", venue.strip(),
    "date:", date,
    "time:", time,
    "price:", price.strip(),
    "email:", email.strip(),
    "mobile:", mobile.strip(),
    "services:", services,
    "duration:", duration,
    "payment_status:" , payment_status,
    "booked_at:" , booked_at,
    "source:" , source,
    "accept_link:" , accept_link,
    "reschedule_link:" , reschedule_link,
    "booking_link:", booking_link,
    "guest_name", guest_name
}


print "Treatwell Booking: (", treatwell_ref_number , ")"
print "venue:", venue.strip()
print "date:", date
print "time:", time
print "price:", price.strip()
print "email:", email.strip()
print "mobile:", mobile.strip()
print "services:", services
print "duration:", duration
print "payment_status:" , payment_status
print "booked_at:" , booked_at
print "source:" , source
print "accept_link:" , accept_link
print "reschedule_link:" , reschedule_link
print "booking_link:", booking_link
print "guest_name", guest_name
# *Booking Details*
# Order reference:    79753434
# Venue:    Revowax at The Tanning Shop
# Product Name:    Brazilian Waxing
# Price paid:    =C2=A331.50
# Guest Email:    briedisem@hotmail.com
# Guest Tel.:    +44 7806 615089
# Quantity:    1



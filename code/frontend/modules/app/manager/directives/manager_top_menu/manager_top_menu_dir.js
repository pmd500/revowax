revo_app.directive('managerTopMenu', function($state, UserService) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/directives/manager_top_menu/manager_top_menu_tmpl.html',
        link: function(scope, element, attrs, tabsCtrl) {
            scope.is_collapsed = true;
            
            scope.toggle_nav = function () {
                scope.is_collapsed = !scope.is_collapsed
            }

            // Set up functions
            scope.log_out = function () {
                UserService.log_out();
                $state.go("app.anonymous.login")
            }
        }
    };
});

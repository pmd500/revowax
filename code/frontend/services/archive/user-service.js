// revo_app.factory('UserService', ['$http', '$rootScope', '$cookies', function ($http, $rootScope, $cookies) {
//     // Init User Data
//     var user = { 
//         details: {
//             isLoggedIn: false,
//             hash: null,
//             username: null,
//             userType: null,
//             userFirstName: null,
//             userSurName: null,
//             userMobile: null,
//             userLandline: null,
//             userAddress1: null,
//             userAddress2: null,
//             userPostcode: null,
//             tokenValidityDays: null
//         }
//     };
//
//     // Sets every field to null
//     var ClearUserDetails = function () {
//         user.details.isLoggedIn = false;
//         user.details.hash = null;
//         user.details.username = null;
//         user.details.userType = null;
//         user.details.userFirstName = null;
//         user.details.userSurName = null;
//         user.details.userMobile = null;
//         user.details.userLandline = null;
//         user.details.userAddress1 = null;
//         user.details.userAddress2 = null;
//         user.details.userPostcode = null;
//         user.details.tokenValidityDays = null;
//     };
//
//     // Sets User Details and adds token to cookie
//     var SetCookieCredentials = function (username, token) {
//         $cookies.put('revowax_user', username);
//         $cookies.put('revowax_token', token);
//     };
//
//     // Sets User Details and adds token to cookie
//     var ClearCookieCredentials = function () {
//         $cookies.remove('revowax_user');
//         $cookies.remove('revowax_token');
//     }
//
//     // User Registration
//     user.Register =  function (username,
//                                userType,
//                                userFirstName,
//                                userSurName,
//                                userMobile,
//                                userLandline,
//                                userAddress1,
//                                userAddress2,
//                                userPostcode,
//                                userPassword,
//                                cb_success,
//                                cb_fail)
//     {
//         // Add user
//         $http({
//             method: 'post',
//             url: urlBase + ':9875/revoweb/addUser',
//             headers: {
//                 'REVO-PUBLIC-TOKEN': "token",
//             },
//             data: {
//                 username : username,
//                 userType : userType,
//                 userFirstName : userFirstName,
//                 userSurName : userSurName,
//                 userMobile : userMobile,
//                 userLandline : userLandline,
//                 userAddress1 : userAddress1,
//                 userAddress2 : userAddress2,
//                 userPostcode : userPostcode,
//                 userPassword : userPassword
//             }
//
//         // SUCCESS
//         })
//         .then(function successCallback(response) {
//             console.log("reg success")
//             cb_success()
//
//         // FAIL
//         }, function errorCallback(response) {
//             console.log("reg fail")
//             cb_fail();
//         });
//     }
//
//     // User Authentication
//     user.Authenticate = function (cb_success, cb_fail) {
//
//         console.log("Checking cookie")
//         var cookie_user = $cookies.get('revowax_user');
//         var cookie_token = $cookies.get('revowax_token');
//
//         // Test cookie
//         if (cookie_user !== undefined) {
//             console.log("Authenticating Token stored in cookie")
//             // Try Authenticating with Token
//             $http({
//                 method: 'post',
//                 url: urlBase + ':9875/revoweb/userByTokenIdQuery',
//                 headers: {
//                     'REVO-PUBLIC-TOKEN': cookie_token,
//                     'X-REVO-AUTH-USER': cookie_user
//                 },
//                 data: {
//                     "tokenHashId": cookie_token
//                 }
//
//                 // SUCCESS
//             }).then(function successCallback(response) {
//
//                 console.log("Success")
//                 // Update Full User details
//                 user.details.isLoggedIn = true;
//                 user.details.hash = cookie_token;
//                 user.details.username = cookie_user;
//                 user.details.userType = response.data.userType;
//                 user.details.userFirstName = response.data.userFirstName;
//                 user.details.userSurName = response.data.userSurName;
//                 user.details.userMobile =  response.data.userMobile;
//                 user.details.userLandline =  response.data.userLandline;
//                 user.details.userAddress1 =  response.data.userAddress1;
//                 user.details.userAddress2 =  response.data.userAddress2;
//                 user.details.userPostcode =  response.data.userPostcode;
//                 user.details.tokenValidityDays =  response.data.tokenValidityDays;
//
//                 // Success Callback
//                 cb_success();
//
//             // FAIL
//             }, function errorCallback(response) {
//                 console.log("Fail")
//
//                 ClearCookieCredentials();
//                 ClearUserDetails();
//
//                 // Fail Callback
//                 cb_fail();
//             });
//         }
//     };
//
//     // Login service for revowax, returns access token and basic user details
//     user.Login = function (username, password, cb_success, cb_failure) {
//         // Login
//         $http({
//             method: 'put',
//             url: urlBase + ':9875/revoweb/authenticate',
//             headers: {
//                 'REVO-TOKEN': "dsjksdjsd"
//             },
//             data: {
//                 "username": username,
//                 "password": password
//             }
//         })
//
//             // Login Successful
//             .then(function successCallback(response) {
//                 // Update Basic User details
//                 user.details.isLoggedIn = true;
//                 user.details.hash = response.data.Hash;
//                 user.details.username = username;
//                 user.details.userType = response.data.UserType;
//                 user.details.userFirstName = response.data.UserFirstname;
//                 user.details.userSurName = response.data.UserSurname;
//
//                 // Save token & user to cookie
//                 SetCookieCredentials(username, response.data.Hash);
//
//                 // Call back
//                 cb_success();
//             },
//
//             // Login failed
//             function errorCallback(response) {
//                 ClearUserDetails();
//                 cb_failure();
//             });
//     };
//
//     // Get All Users
//     user.GetUsers = function (cbSuccess, cbFail){
//         // Get up to date user data
//         $http({
//             method: 'get',
//             url: urlBase + ':9875/revoweb/getUsers',
//             headers: {
//                 'X-REVO-AUTH-TOKEN':  user.details.hash,
//                 'X-REVO-AUTH-USER': user.details.username
//             },
//         })
//             // SUCCESS
//             .then(function successCallback(response) {
//                 cbSuccess(response.data);
//             },
//             // FAIL
//             function errorCallback() {
//                 cbFail();
//             });
//     };
//
//     // Get All Users
//     user.DeactivateUser = function (cbSuccess, cbFail){
//         // Get up to date user data
//         $http({
//             method: 'put',
//             url: urlBase + ':9875/revoweb/updateUserActiveStatus',
//             headers: {
//                 'X-REVO-AUTH-TOKEN':  user.details.hash,
//                 'X-REVO-AUTH-USER': user.details.username
//             },
//             data: {
//                 "username" : user.details.username,
//                 "activeStatus" : false
//             }
//         })
//             // SUCCESS
//             .then(function successCallback(response) {
//                 cbSuccess();
//             },
//             // FAIL
//             function errorCallback() {
//                 cbFail();
//             });
//     };
//
//     // returns user full details
//     user.GetUserDetails = function () {
//         return user.details;
//     }
//
//     // Deletes cookies and clears user details
//     user.LogOut = function () {
//         ClearCookieCredentials();
//         ClearUserDetails();
//     };
//
//     return user;
// }]);
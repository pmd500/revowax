revo_app.directive('passwordResetForm', function(UserService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/forms/password_reset_form/password_reset_form_tmpl.html',
        link: function (scope, element, attrs, tabsCtrl) {
            // Controller variables
            scope.new_email = ""
            scope.message = "";
            scope.message_style = {"color": "red"}

            // Set up functions
            scope.password_reset = function (email){
                // Call webservice
                UserService.reset_password(
                    new_email,
                    // On Success
                    function(){
                        scope.message_style = {"color": "green"}
                        scope.message = "Reset new_email successfully sent to " + new_email;
                    },
                    // On Failure
                    function(){
                        scope.message_style = {"color": "red"}
                        scope.message = 'This new_email has not been registered';
                    }
                )
            }
        }
    }
});
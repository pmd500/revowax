from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.forms import PasswordResetForm

from rest_auth.serializers import PasswordResetSerializer
from rest_auth.views import PasswordResetView

# Sends HTML Email as well as plain text email on password reset
class PasswordResetFormWithHtmlEmail(PasswordResetForm):

    # Overriding save method so it sends html emails as well
    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name='registration/password_reset_html_email.html',
             extra_email_context=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        email = self.cleaned_data["email"]
        for user in self.get_users(email):
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            context = {
                'email': user.email,
                'domain': domain,
                'frontend_domain': settings.SECRETS['FRONTEND_DOMAIN'],
                'backend_domain': settings.SECRETS['BACKEND_DOMAIN'],
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            if extra_email_context is not None:
                context.update(extra_email_context)

            self.send_mail(
                subject_template_name, email_template_name, context, from_email,
                user.email, html_email_template_name=html_email_template_name,
            )

class PasswordResetSerializerWithHtmlEmail(PasswordResetSerializer):
    # Over class variable with different form
    password_reset_form_class = PasswordResetFormWithHtmlEmail

class PasswordResetViewWithHtmlEmail(PasswordResetView):
    # Over class variable with different serializer
    serializer_class = PasswordResetSerializerWithHtmlEmail

import os, django
from django.utils import timezone
from datetime import date, datetime, time, timedelta
import json, requests

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "revo_api.settings.local")
django.setup()

from revo_booker.utility import validate_fields, string_to_datetime
# from revo_booker.models import *

from revo_booker.views.booker_views import *
# from revo_booker.emailer import *
from revo_booker.models import *
# from revo_booker.serializers.simple import *
# from django.conf import settings
# from django.shortcuts import get_object_or_404

# url = 'http://dev.api.revowax.com/treatwell-booking-request/'
# headers = {'content-type': 'application/json'}
# payload = {
#     "booking_link": "<https://connect.treatwell.co.uk/link/order/aREVIEW.s13268.o4668288.x441E62=63/>",
#     "payment_status": "Prepaid unconfirmed",
#     "price": "43.00",
#     "reschedule_link": "<https://connect.treatwell.co.uk/link/order/aREVIEW.s13268.o4668288.x441E62=63/>",
#     "date": "11 April 2017",
#     "booked_at": "9 April 2017, 3:43 PM.",
#     "services": "Underarms Wax, Lower Leg Wax, Eyebrows Wax",
#     "duration": 50,
#     "booking_ref": "79460779",
#     "mobile": "+44 7521 046994 <+44%207521%20046994>",
#     "accept_link": "<https://connect.treatwell.co.uk/link/order/aCONFIRM.s13268.o4668288.xA8BDD=679/>",
#     "venue": "Revowax at The Tanning Shop",
#     "source": "Treatwell.",
#     "time": "5:00 pm",
#     "email": "laura.dyduch27@gmail.com",
#     "guest_name": "Florie Doublet"
# }
# response = requests.post(url, data=json.dumps(payload), headers=headers)

print timezone.get_current_timezone()
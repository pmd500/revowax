from django.contrib.auth.models import User
from django.db import models
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from revo_booker.models import *
from revo_booker.serializers.simple import *
from drf_writable_nested import WritableNestedModelSerializer


class DeepProfileSerializer(serializers.ModelSerializer):
    user = SimpleUserSerializer(required=False)
    class Meta:
        model =  Profile
        fields = ('__all__')

class DeepResourceSerializer(serializers.ModelSerializer):
    type = ResourceTypeSerializer()
    availability = ResourceRegularAvailabilitySerializer()

    class Meta:
        model =  Resource
        fields = '__all__'

class DeepStaffSerializer(serializers.ModelSerializer):
    user = SimpleUserSerializer()
    custom_availability = StaffCustomAvailabilitySerializer()
    staff_type = StaffTypeSerializer()
    skills = SkillSerializer(many=True)
    class Meta:
        model =  Staff
        fields = '__all__'

class DeepServiceCategorySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    services = ServiceSerializer(many=True)
    subcategories = RecursiveField(required=False, allow_null=False, many=True)

    class Meta:
        model = ServiceCategory
        fields = ('id','name','has_services','services','subcategories')

class DeepServiceGroupSerializer(serializers.ModelSerializer):
    resource_needed = ResourceTypeSerializer(many=False)
    services = ServiceSerializer(many=True)
    root_catergory = DeepServiceCategorySerializer(many=False)

    class Meta:
        model = ServiceGroup
        fields = '__all__'

class DeepShopSerializer(serializers.ModelSerializer):
    service_groups = DeepServiceGroupSerializer(many=True)
    opening_times = ShopOpeningTimesSerializer(many=False)
    resources = DeepResourceSerializer(many=True)
    # staff = DeepStaffSerializer(many=True)
    class Meta:
        model = Shop
        fields = '__all__'

class DeepCustomerBookingSerializer(WritableNestedModelSerializer):
    services = ServiceSerializer(many=True, read_only=True)

    class Meta:
        model =  CustomerBooking
        fields = '__all__'

class DeepBusinessSerializer(serializers.ModelSerializer):
    shops = DeepShopSerializer(many=True)
    class Meta:
        model = Business
        fields = '__all__'
revo_app.directive("userDetailsForm", function(AppStateService,UserService, $timeout) {
    return {
        restrict: 'E',
        templateUrl: "modules/app/directives/forms/user_details_form/user_details_form_tmpl.html",
        link:function(scope, element, attrs){
            // State
            scope.editable = false;
            scope.show_alert_flag = false;

            // Binable Variables Info
            scope.user_state = AppStateService.user_state;
            scope.old_password = "";
            scope.new_password1 = "";
            scope.new_password2 = "";
            scope.show_message = false
            scope.message = ""
            scope.message_style = {"color":"black"}

            // Change Password
            scope.change_password = function (){
                console.log("clicked")
                var passwords_match = (scope.new_password1 == scope.new_password2)
                var new_password_not_blank = !((scope.new_password1 == "") || (scope.new_password1 == null))
                var old_password_not_blank = !((scope.old_password == "") || (scope.old_password == null))

                // Call Change Password Web service
                if(passwords_match && new_password_not_blank && old_password_not_blank) {
                    UserService.change_password(
                        scope.old_password,
                        scope.new_password1,
                        scope.new_password2,
                        // On Success
                        function(response){
                            scope.show_message = true
                            scope.message = "Password successfully updated"
                            scope.message_style = {"color":"green"}

                            // Reset inputs
                            scope.old_password = "";
                            scope.new_password1 = "";
                            scope.new_password2 = "";

                        },
                        // On Fail
                        function(response){
                            scope.show_message = true
                            scope.message_style = {"color":"red"}

                            // Decipher error code
                            if(response.status == 400) {
                                console.log(response)
                                // Password problems
                                if (response.data.new_password1 != null)
                                    scope.message = "Password is " + response.data.new_password1[0]
                                else if (response.data.new_password2 != null)
                                    scope.message = "Password is " + response.data.new_password2[0]
                                else if (response.data.old_password != null)
                                    scope.message = "Old password is invalid"
                                else
                                    scope.message = "Server Error [500]"
                            }

                            else
                                scope.message = "Server error 500"
                        }
                    )
                }
                else {
                    if(new_password_not_blank) {
                        scope.show_message = true
                        scope.message = "Your passwords don't match!"
                        scope.message_style = {"color": "red"}
                    }
                }
            }

            // Deactivate User
            scope.delete = function () {
                UserService.DeactivateUser(
                    // Success
                    function(){
                        // Clear Cookies
                        UserService.LogOut();
                        // Change State
                        $state.go("app.anonymous.landing")
                    },
                    // Fail
                    function(){
                        console.log("User Deactivation Error")
                    }
                )
            }

            // Save User
            scope.save = function () {
                UserService.update_profile(
                    scope.user_state.id,
                    scope.user_state.first_name,
                    scope.user_state.last_name,
                    scope.user_state.email,
                    scope.user_state.mobile,
                    scope.user_state.receive_promotions,
                    // success callback
                    function () {
                        flashAlertBox();
                    },
                    //fail callback
                    function () {

                    }
                )
            }

            // Flash Alert Box
            var flashAlertBox = function (){
                // Show flag
                scope.show_alert_flag = true;

                $timeout(function(){
                    scope.show_alert_flag = false;
                }, 3000)
            }
        }
    }
});

// Configure App
revo_app.config(function($stateProvider) {

    // Manager Landing Page
    $stateProvider
        .state('app.manager.landing', {
            url: "/landing",
            data: {
                css: 'modules/app/directives/landing/landing.css'
            },
            views: {
                "manager_top_menu": {
                    template: ""
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/landing/landing_tmpl.html",
                    controller: "ManagerLandingCtl",
                    controllerAs: "land_ctl"
                }
            },
        })

    // Salon Profile Page
    $stateProvider
        .state('app.manager.agenda', {
            url: "/agenda",
            data: {
                css: 'modules/app/manager/content/agenda/agenda.css'
            },
            resolve: {
                ShopBookings: function(ShopService, UserDetail){
                    return ShopService.get_defered_shop_bookings(SHOP_ID);
                },
                ExistingCustomers: function (BusinessService, UserDetail) {
                    return BusinessService.api.get_deferred_business_customers(BUSINESS_ID)
                    // return [
                    //     {
                    //         first_name: "Phil",
                    //         last_name: "Day",
                    //         mobile: "07506788205",
                    //         email: "phil.day@gmail.com"
                    //     },
                    //     {
                    //         first_name: "Daniele",
                    //         last_name: "Perseguine",
                    //         mobile: "076667538205",
                    //         email: "dani.perseguine@gmail.com"
                    //     },
                    //     {
                    //         first_name: "Maggie",
                    //         last_name: "Day",
                    //         mobile: "076367533405",
                    //         email: "musicalmaggie@gmail.com"
                    //     },
                    //     {
                    //         first_name: "Melissa",
                    //         last_name: "Osborn",
                    //         mobile: "076323533405",
                    //         email: "dancingmel@gmail.com"
                    //     }
                    // ]
                },
                RevowaxShopDetails: function(ShopService){
                    return ShopService.get_defered_shop_details(SHOP_ID);
                }
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/agenda/agenda_tmpl.html",
                    controller: "ManagerAgendaCtl",
                    controllerAs: "agenda_ctl"
                },
               
            },
        })



    // Salon Profile Page
    $stateProvider
        .state('app.manager.salon_profile', {
            url: "/salon-profile",
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/salon/profile/salon_profile_tmpl.html",
                    controller: "ManagerSalonProfileCtl",
                    controllerAs: "sal_pro_ctl"
                }
            },
        })
    
    // Manager User Details
    $stateProvider
        .state('app.manager.user_details', {
            url: "/user-details",
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "LoginCtl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/user_details/user_details_tmpl.html",
                }
            },
        })
    
    // Manager Bookings
    $stateProvider
        .state('app.manager.bookings', {
            url: "/bookings",
            data: {
                css: 'modules/app/manager/content/bookings/bookings.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/bookings/bookings_tmpl.html",
                    controller: "ManagerBookingsCtl",
                    controllerAs: "bookings_ctl"
                }
            },
        })
    
    //Manager Booking Select Services
    $stateProvider
        .state('app.manager.booking_select_services', {
            url: "/booking-select-services",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/booking/templates/booking_select_services_tmpl.html",
                    controller: "BookingCtl",
                    controllerAs: "booking_ctl"
                }
            },
        })


    //Manager Booking Select Slot
    $stateProvider
        .state('app.manager.booking_select_slot', {
            url: "/booking-select-slot",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/booking/templates/booking_select_slot_tmpl.html",
                    controller: "BookingCtl",
                    controllerAs: "booking_ctl"
                }
            },
        })

    // Manager Booking UserDetails
    $stateProvider
        .state('app.manager.booking_user_details', {
            url: "/booking-user-details",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/booking/templates/booking_user_details_tmpl.html",
                    // controller: "BookingStage4Ctl",
                    // controllerAs: "booking_stage4_ctl"
                }
            },
        })

    // Manager Booking Overview
    $stateProvider
        .state('app.manager.booking_overview', {
            url: "/booking-overview",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/booking/templates/booking_overview_tmpl.html",
                    // controller: "BookingStage4Ctl",
                    // controllerAs: "booking_stage4_ctl"
                }
            },
        })

    // Manager Booking Payment
    $stateProvider
        .state('app.manager.booking_payment', {
            url: "/booking-payment",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/booking/templates/booking_payment_tmpl.html",
                }
            },
        })
    
    // Manager Booking Confirmation
    $stateProvider
        .state('app.manager.booking_confirmation', {
            url: "/booking-confirmation",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/manager/content/booking/templates/booking_confirmation_tmpl.html",
                }
            },
        })

    // Manager Booking Problem
    $stateProvider
        .state('app.manager.booking_problem', {
            url: "/booking-problem",
            data: {
                css: 'modules/app/directives/booking/booking.css'
            },
            views: {
                "manager_top_menu": {
                    templateUrl: "modules/app/manager/top_menu/top_menu_tmpl.html",
                    controller: "ManagerTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "manager_content": {
                    templateUrl: "modules/app/shared/content/booking/templates/booking_problem_tmpl.html",
                }
            },
        })
    //     })
});


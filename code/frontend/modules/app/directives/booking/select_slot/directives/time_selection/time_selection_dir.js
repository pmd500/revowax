revo_app.directive('timeSelection', function(UtilityService) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/select_slot/directives/time_selection/time_selection_tmpl.html',
        scope: {
            seldatetime : "=",
            onupdate : "="
        },
        link : function (scope) {
            scope.mytime = new Date();

            scope.hstep = 1;
            scope.mstep = 1;

            scope.options = {
                hstep: [1, 2, 3],
                mstep: [1, 5, 10, 15, 25, 30]
            };

            scope.ismeridian = true;
            scope.toggleMode = function () {
                scope.ismeridian = !scope.ismeridian;
            };

            scope.update = function () {
                var d = new Date();
                d.setHours(12);
                d.setMinutes(0);
                scope.mytime = d;
                scope.onupdate(UtilityService.date_methods.change_time_of_datetime(scope.seldatetime, scope.mytime))
            };

            scope.changed = function () {
                console.log("update bookings")
                // Call external on update function
                scope.onupdate(UtilityService.date_methods.change_time_of_datetime(scope.seldatetime, scope.mytime))
            };

            scope.clear = function () {
                scope.mytime = null;
            };
        }
    };
});
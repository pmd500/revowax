revo_app.controller("AnonymousLandingCtl", ["$scope", "$state", "UserService", "RevowaxShopDetails", "ShopService", function($scope, $state, UserService, RevowaxShopDetails, ShopService){
    var ctl = this;
    
    // bind revo wax details
    ctl.revowax_shop_details = RevowaxShopDetails;

    return ctl;
}]);
revo_app.service('ConfirmService', function($modal) {
    var service = {};
    service.open = function (text, onOk) {
        var modalInstance = $modal.open({
            templateUrl: 'modules/app/directives/modals/confirm_modal_tmpl.html',
            controller: 'ModalConfirmCtrl',
            resolve: {
                text: function () {
                    return text;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            onOk();
        }, function () {
        });
    };

    return service;
})

revo_app.controller('ModalConfirmCtrl', function ($scope, $modalInstance, text) {

    $scope.text = text;

    $scope.ok = function () {
        $modalInstance.close(true);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.directive('confirm', function(ConfirmService) {
    return {
        restrict: 'A',
        scope: {
            eventHandler: '&ngClick'
        },
        link: function(scope, element, attrs){
            element.unbind("click");
            element.bind("click", function(e) {
                ConfirmService.open(attrs.confirm, scope.eventHandler);
            });
        }
    }
});
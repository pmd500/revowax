import os, django
# from django.utils import timezone
from django.conf import settings
from django.db.models import Q

from datetime import date,datetime, time, timedelta

from revo_booker.utility import *
from revo_booker.models import *
from revo_booker.emailer import send_confirmation_of_booking

MINUTES_IN_HOUR = 60
SECONDS_IN_MINUTE = 60
START_TIME_INTERVAL = 20

class RevoBooker:
    send_confirmation_email = False

    # Init RevoBooker
    def __init__(self, business, shop, service_group):
        self.business = business
        self.shop = shop
        self.service_group = service_group

    # Get customer bookings for resource
    def filter_customer_bookings_by_day_and_resource(self, shop_bookings,resource, start_date):
        # Map and filter
        resource_bookings = filter(lambda booking:((booking.resource == resource) & (booking.start_time.day == start_date.day)), shop_bookings)

        return resource_bookings
    
    # Is Available
    def check_time_slot_with_all_resource_bookings(self,resource_bookings, start, end):
        def check_times(booking):
            start_conflict = (start >= booking.start_time) and (start < booking.end_time)
            end_conflict = (end >= booking.start_time) and (end < booking.end_time)
            return start_conflict or end_conflict
    
        # check bookings slot with already booked appointments
        appointment_conflicts = filter(lambda booking: check_times(booking), resource_bookings)
    
        # Return True if no appointment conflicts
        return not any(appointment_conflicts)
    
    # Gets available appointment slots for resource based on customer bookings and the resource/shop availability
    def get_available_appointment_slots(self, shop_opening_times, shop_bookings, resource, resource_availability, start_date, end_date, duration):
        days = []
    
        appointments = []
    
        for single_date in date_range(start_date, end_date):
    
            # Get resource bookings for day
            resource_bookings = self.filter_customer_bookings_by_day_and_resource(shop_bookings, resource, single_date)
    
            # Use shop opening times rather than resource availability
            if(resource.availability.match_store_opening_times):
                is_available = shop_opening_times[single_date.weekday()]["is_open"]
                opening_time = shop_opening_times[single_date.weekday()]["start"]
                closing_time = shop_opening_times[single_date.weekday()]["end"]
            # Use fine grain resource availability
            else:
                is_available = resource_availability[single_date.weekday()]["is_available"]
                opening_time = resource_availability[single_date.weekday()]["start"]
                closing_time = resource_availability[single_date.weekday()]["end"]
    
            if (is_available):
    
                # Calculate number of minutes in the day
                total_minutes = calculate_time_in_mins(closing_time) - calculate_time_in_mins(opening_time) - duration
    
                # Calculate max times
                max_num_of_start_times = total_minutes / self.shop.min_slot_duration
    
                # loop through possible slots
                for i in range(max_num_of_start_times):
                    # Calculate start and end time of appointment
                    start = datetime.combine(single_date, opening_time) + timedelta(minutes=(i*START_TIME_INTERVAL))
                    end = datetime.combine(single_date, opening_time) + timedelta(minutes=(i*START_TIME_INTERVAL + duration))
    
                    # Make times aware
                    # start_timezone_aware = timezone.make_aware(start, timezone.get_default_timezone())
                    # end_timezone_aware = timezone.make_aware(end, timezone.get_default_timezone())
    
                    # Check appointment slot is available
                    slot_available = self.check_time_slot_with_all_resource_bookings(resource_bookings, start, end)
    
                    # Add Appointment
                    if(slot_available):
                        print "slot", resource.name, start.strftime(settings.STANDARD_DATETIME_STRING_FORMAT)
                        appointments.append({"resource_id": resource.id,
                                             "resource_name": resource.name,
                                             "start": start.strftime(settings.STANDARD_DATETIME_STRING_FORMAT),
                                             "end": end.strftime(settings.STANDARD_DATETIME_STRING_FORMAT)
                                             })
    
        return appointments
    
    # Returns available booking slots between start and end dates
    def get_available_appointments(self, start_date, end_date, duration):
    
        # Get Shop Opening Times
        shop_opening_times = convert_opening_times(self.shop.opening_times)
    
        # Get Shop Resources
        shop_resources = self.shop.resources.filter(type=self.service_group.resource_needed)
    
        # ID of needed shop resources
        shop_resource_ids = shop_resources.values_list('id', flat=True)
    
        # Get shop bookings for dates and resources
        shop_bookings = CustomerBooking.objects.filter(business=self.business,shop=self.shop,resource__in = shop_resource_ids, start_time__gte=start_date, end_time__lte=end_date)
    
        # Loop through each resource, generating available appointments
        available_appointments_4_resources = []
        for resource in shop_resources:
            resource_availability = convert_availabilty_times(resource.availability)
    
            # generate available appointment slots for resource
            available_appointments_4_resource = self.get_available_appointment_slots(
                shop_opening_times,
                shop_bookings,
                resource,
                resource_availability,
                start_date,
                end_date,
                duration
            )
    
            # Join to full list
            available_appointments_4_resources += available_appointments_4_resource
    
        return available_appointments_4_resources
    
    # Checks is time range if free for a resource at a shop
    def check_appointment_availability(self,resource, start_datetime, end_datetime):
    
        # Get Shop Resources
        shop_resources = self.shop.resources.filter(type=self.service_group.resource_needed)
    
        # Get shop bookings for dates and resources
        resource_bookings = CustomerBooking.objects.filter(Q(business=self.business,
                                                           shop=self.shop,
                                                           resource = resource) &
                                                           (Q(start_time__range = [start_datetime, end_datetime]) |
                                                           Q(end_time__range = [start_datetime, end_datetime])))
    
        slot_available = self.check_time_slot_with_all_resource_bookings(resource_bookings, start_datetime, end_datetime)
    
        return slot_available
    
    
    # Associates the booking with the chosen services
    @staticmethod
    def add_services_2_booking(booking, service_list):
        # Create array of service customer booking objects
        services = []
        for service in service_list:
            services.append(
                ServiceCustomerBooking(
                    service=Service.objects.get(id=service["id"]),
                    customer_booking=booking,
                    price=service["price"],
                    duration=service["minutes"],
                )
            )
        # Bulk save services
        ServiceCustomerBooking.objects.bulk_create(services)
    
    # Book Appointment
    def book_appointment(self,booked_by_profile, customer_profile, resource, start_datetime, end_datetime, duration, total_price, booking_status,payment_status, staff, service_list):
    
        # Check if appointment is still available
        if(booked_by_profile == customer_profile):
            still_available = self.check_appointment_availability(resource, start_datetime, end_datetime)
        # Staff override
        else:
            still_available = True

        print "START MOD: ", start_datetime

        if(still_available):
            # Create new booking
            booking = CustomerBooking(
                booked_by=booked_by_profile,
                booked_at=timezone.now(),
                customer=customer_profile,
                business=self.business,
                service_group = self.service_group,
                shop=self.shop,
                start_time=start_datetime,
                end_time=end_datetime,
                duration=duration,
                total_price=total_price,
                booking_status=booking_status,
                payment_status=payment_status,
                resource=resource,
                staff=staff,
            )
    
            # Save booking
            booking.save()
    
            # Add Chosen Services
            self.add_services_2_booking(booking, service_list)
    
            # Associate Customer with shop, business
            CustomerBusiness.objects.get_or_create(customer=customer_profile, business=self.business)
            CustomerShop.objects.get_or_create(customer=customer_profile, shop=self.shop)
    
            # Send email confirmation
            if self.send_confirmation_email:
                send_confirmation_of_booking(
                    to_email = customer_profile.email,
                    first_name = customer_profile.first_name,
                    date = start_datetime.strftime("%H:%M %d-%m-%y"),
                    price = str(total_price),
                    duration = str(duration)
                )
    
            # Appointment Added
            return booking
        else:
            # Appointment no longer available
            return None

class TreatwellBooker:
    # Lookups or create profile based on email or mobile
    def look_up_or_create_profile(self, guest_name, mobile, email):

        # Look up profile based on email or mobile
        ps = Profile.objects.filter(Q(email=email) or Q(mobile))

        # If length == 1, get profile
        if len(ps) == 1:
            print "PROFILE FOUND"
            profile = ps[0]
        else:
            print "PROFILE NEEDS CREATING"
            split_names = guest_name.split(" ")
            # Only first name
            if len(split_names) == 1:
                first_name = guest_name
                last_name = None
            elif len(split_names) == 2:
                first_name = split_names[0]
                last_name = split_names[1]

            # Create profile
            profile = Profile(first_name=first_name, last_name=last_name, mobile=mobile, email=email)
        return profile

    # Checks fields of new treatwell booking request
    def validate_treatwell_booking_request(self, request):
        # Validate request fields
        (has_error, error_message) = \
            validate_fields(request,
                            "booking_link",
                            "payment_status",
                            "price",
                            "reschedule_link",
                            "date",
                            "booked_at",
                            "services",
                            "duration",
                            "booking_ref",
                            "mobile",
                            "accept_link",
                            "venue",
                            "source",
                            "time",
                            "email",
                            "guest_name"
                            )

        return has_error, error_message

    # Create a booking from a post request
    def create_booking_frm_request(self, request):
        # Booking data
        data = request.data

        # Get shop name by treatwell name
        shops = Shop.objects.filter(treatwell_name=data['venue'])

        # If only one shop found
        if len(shops) == 1:
            shop = shops[0]
            business = shop.business

            # FIXME - hack to set service group and resource
            service_group = business.service_groups.all()[0]
            resource = shop.resources.filter(type=service_group.resource_needed)[0]

            # Create Booking
            booking = CustomerBooking()
            booking.booking_source = BookingSources.treatwell_website
            booking.booked_by = None
            booking.booked_at = string_to_datetime(data["booked_at"], '%d %B %Y, %I:%M %p.')
            booking.notes = None
            booking.customer = self.look_up_or_create_profile(data["guest_name"], data["mobile"], data["email"])
            booking.business = business
            booking.shop = shop
            booking.service_group = service_group
            booking.start_time = string_to_datetime((data["date"] + " " + data["time"]), '%d %B %Y %I:%M %p')
            booking.end_time = booking.start_time + timedelta(minutes=int(data["duration"]))
            booking.duration = data["duration"]
            booking.total_price = data["price"]
            booking.booking_status = BookingStatus.requested
            booking.payment_status = PaymentStatus.unknown
            booking.resource = resource
            booking.staff = None
            booking.save()

            # Create Treatwell Booking
            new_tw = TreatwellBooking()
            new_tw.booking = booking
            new_tw.booking_ref = data["booking_ref"]
            new_tw.booking_link = data["booking_ref"]
            new_tw.confirmation_link = data["accept_link"]
            new_tw.reschedule_link = data["reschedule_link"]
            new_tw.payment_status = data["payment_status"]
            new_tw.services = data["services"]
            new_tw.save()
            
            return booking

        else:
            return None
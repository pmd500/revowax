revo_app.directive('dateSelection', function(UtilityService) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/select_slot/directives/date_selection/date_selection_tmpl.html',
        scope: {
            seldatetime : "=",
            onupdate : "="
        },
        link : function (scope) {
            scope.today = function() {
                scope.dt = new Date();
            };
            scope.today();

            scope.clear = function () {
                scope.dt = null;
            };

            // Disable weekend selection
            scope.disabled = function(date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            };

            scope.toggleMin = function() {
                scope.minDate = scope.minDate ? null : new Date();
            };
            scope.toggleMin();
            scope.maxDate = new Date(2020, 5, 22);

            scope.open = function($event) {
                scope.status.opened = true;
            };

            scope.setDate = function(year, month, day) {
                scope.dt = new Date(year, month, day);
            };

            scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            scope.format = scope.formats[0];

            scope.status = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            scope.events = [];

            scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);

                    for (var i=0;i<scope.events.length;i++){
                        var currentDay = new Date(scope.events[i].date).setHours(0,0,0,0);

                        if (dayToCheck === currentDay) {
                            return scope.events[i].status;
                        }
                    }
                }

                return '';
            };

            scope.onDateClick = function () {
                scope.onupdate(UtilityService.date_methods.change_date_of_datetime(scope.seldatetime,  scope.dt))
            }
        }
    };
});

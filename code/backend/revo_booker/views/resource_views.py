from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from revo_booker.serializers.deep import *

# Resource End points
class ResourceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer


class ResourceCreate(generics.CreateAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer


class ResourceRegularAvailabilityCreate(generics.CreateAPIView):
    queryset = ResourceRegularAvailability.objects.all()
    serializer_class = ResourceRegularAvailabilitySerializer

#
class ResourceRegularAvailabilityDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ResourceRegularAvailability.objects.all()
    serializer_class = ResourceRegularAvailabilitySerializer

# Bookings for resource
class ResourceBookingsList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get bookings for business
    def get(self, request, resource_id, format=None):
        bookings = CustomerBooking.objects.filter(resource=resource_id)
        serializer = CustomerBookingSerializer(bookings, many=True)
        return Response(serializer.data)

# Resource types
class ResourceTypeList(generics.ListCreateAPIView):
    queryset = ResourceType.objects.all()
    serializer_class = ResourceTypeSerializer

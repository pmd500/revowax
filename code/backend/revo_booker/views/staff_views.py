from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from revo_booker.serializers.deep import *

class StaffShopLinkCreate(generics.CreateAPIView):
    queryset = ShopStaff.objects.all()
    serializer_class = ShopStaffSerializer


class StaffShopLinkDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ShopStaff.objects.all()
    serializer_class = ShopStaffSerializer


# Staff End points
class StaffDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer


class StaffCreate(generics.CreateAPIView):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer


class StaffRegularAvailabilityCreate(generics.CreateAPIView):
    queryset = StaffRegularAvailability.objects.all()
    serializer_class = StaffRegularAvailabilitySerializer


class StaffRegularAvailabilityDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StaffRegularAvailability.objects.all()
    serializer_class = StaffRegularAvailabilitySerializer


class StaffCustomAvailabilityCreate(generics.CreateAPIView):
    queryset = StaffCustomAvailability.objects.all()
    serializer_class = StaffCustomAvailabilitySerializer


class StaffCustomAvailabilityDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StaffCustomAvailability.objects.all()
    serializer_class = StaffCustomAvailabilitySerializer


# Skills
class SkillList(generics.ListCreateAPIView):
    queryset = Skill.objects.all()
    serializer_class = SkillSerializer

# Staff Roles
class StaffRoleList(generics.ListCreateAPIView):
    queryset = ResourceType.objects.all()
    serializer_class = ResourceTypeSerializer


# Bookings for Staff
class StaffBookingsList(APIView):
    permission_classes = (IsAuthenticated,)
    # Get bookings for business
    def get(self, request, staff_id, format=None):
        bookings = CustomerBooking.objects.filter(staff=staff_id)
        serializer = CustomerBookingSerializer(bookings, many=True)
        return Response(serializer.data)
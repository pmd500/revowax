revo_app.controller("CustomerLandingCtl", ["$scope", "$state", "RevowaxShopDetails", function($scope, $state, RevowaxShopDetails){
    var ctl = this;

    // bind variables
    ctl.shop = RevowaxShopDetails;
    
    return ctl;
}]);
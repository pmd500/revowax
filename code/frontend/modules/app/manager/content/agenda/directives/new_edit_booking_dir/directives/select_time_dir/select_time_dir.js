revo_app.directive('agendaTimeSelect', function(UtilityService, $filter) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_time_dir/select_time_tmpl.html',
        scope: {
            start : "=",
            editable: "=",
        },
        link: function (scope) {
            var num_of_appointments = 10;
            
            // Generates list of appointments
            var generate_appointment_times = function () {

                // Generate time ranges
                var booking_times = []
                var start_date = new Date(scope.start.toDate())
                var temp_date = new Date(start_date)
                var start_hour = 07; // 7 AM
                var end_hour = 22; // 10 pm

                temp_date.setHours(start_hour);
                temp_date.setMinutes(0);

                // Calculate available appointments
                var appointment_duration = 10;
                num_of_appointments = ((end_hour - start_hour) * 60) / appointment_duration

                for (var i = 0; i<num_of_appointments; i++) {
                    var booking_slot = $filter('date')(temp_date, "HH:mm ")
                    booking_times.push(booking_slot)

                    // Set default time
                    if((temp_date >= start_date) && (scope.booking_time.selected == undefined)){
                        scope.booking_time.selected = booking_slot
                    }
                    temp_date = UtilityService.date_methods.calc_end_date_time(temp_date, appointment_duration)
                }
                return booking_times
            }

            // Select new time, updates the scope variable start with the selected time
            scope.select_time = function (){
                var dt = new Date(scope.start.toDate())
                var time_string = scope.booking_time.selected
                var hours = time_string.split(":")[0]
                var minutes = time_string.split(":")[1]
                var updated_datetime = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), hours, minutes)
                scope.start = moment(updated_datetime)
            }

            var find_time = function (time) {
                var found_idx = 0
                scope.booking_times.forEach(function (booking_time, idx) {
                    if(booking_time == time) {
                        found_idx = idx
                        return found_idx
                    }
                })
                return found_idx
            }

            // Scroll to right position
            scope.scroll_to_time = function () {
                var window_height = $("#time-choice-list").height()
                var scroll_height = $("#time-choice-list > li").height()
                var scroll_percentage = (find_time(scope.booking_time.selected)/num_of_appointments)
                var scroll_offset = parseInt(window_height/2 - 15)
                var scroll_to = parseInt(scroll_percentage * scroll_height) - scroll_offset

                // scroll to correct height
                $("#time-choice-list").scrollTop(scroll_to)
            }
            
            // Bindable Variables
            scope.booking_time = {};
            scope.booking_times = generate_appointment_times();

        }

        
    }
});

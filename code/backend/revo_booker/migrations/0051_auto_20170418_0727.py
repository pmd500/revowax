# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-18 07:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0050_auto_20170410_1644'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerbooking',
            name='booking_source',
            field=models.CharField(choices=[(b'TW', b'Treatwell Widget'), (b'TO', b'Treatwell Online'), (b'RO', b'RevoWax Online'), (b'WI', b'Walk in')], default=b'RO', max_length=2),
        ),
        migrations.AddField(
            model_name='customerbooking',
            name='service_group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bookings', to='revo_booker.ServiceGroup'),
        ),
        migrations.AddField(
            model_name='shop',
            name='treatwell_name',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]

revo_app.directive('bookingProblem', function($state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/problem/booking_problem_tmpl.html',
        scope: {
            // variables
            shop:"=",
            // functions
            next:"="
        },
        link : function (scope) {
            scope.continue = function(){
                $state.go(scope.next)
            }
        }
    };
});
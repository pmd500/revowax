from django.contrib import admin
from models import *

# Admin Settings
admin.site.site_header = "Revo Booker"


# Inlines
class ServiceInline(admin.TabularInline):
    model = Service
    fields = ("id", "name","price","minutes")

class ShopInline(admin.TabularInline):
    model = Shop

class StaffSkillsInline(admin.TabularInline):
    model = Staff.skills.through

class ShopServiceGroupsInline(admin.TabularInline):
    model = Shop.service_groups.through

class ShopOpeningTimesInline(admin.StackedInline):
    model = ShopOpeningTimes

class StaffInline(admin.TabularInline):
    model = Staff

class ServiceGroupInline(admin.TabularInline):
    model = ServiceGroup

class ServicesCustomBookingInline(admin.TabularInline):
    model = CustomerBooking.services.through

class ShopStaffInline(admin.TabularInline):
    model = Shop.staff.through

class StaffCustomAvailabilityInline(admin.TabularInline):
    model = StaffCustomAvailability

class StaffRegularAvailabilityInline(admin.StackedInline):
    model = StaffRegularAvailability

class ResourceInline(admin.TabularInline):
    model = Resource

class ResourceRegularAvailabilityInline(admin.StackedInline):
    model = ResourceRegularAvailability

# Admin
class BusinessAdmin(admin.ModelAdmin):
    inlines = (ServiceGroupInline,)

class ShopAdmin(admin.ModelAdmin):
    inlines = (ShopOpeningTimesInline, ShopStaffInline, ResourceInline)
    exclude = ("cached_service_tree", "cached_at")

class ServiceCategoryAdmin(admin.ModelAdmin):
    inlines = (ServiceInline,)

class StaffAdmin(admin.ModelAdmin):
    inlines = (StaffRegularAvailabilityInline,)

class CustomBookingAdmin(admin.ModelAdmin):
    inlines = (ServicesCustomBookingInline,)

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', "email", "mobile")
    search_fields = ['first_name', 'last_name', "email", "mobile"]

class ResourceAdmin(admin.ModelAdmin):
    inlines = (ResourceRegularAvailabilityInline,)

# Register your models here.
admin.site.register(Profile, ProfileAdmin)
admin.site.register(StaffType)
admin.site.register(Skill)
admin.site.register(Staff, StaffAdmin)
# admin.site.registered(StaffCustomAvailability)
# admin.site.registered(StaffRegularAvailability)
admin.site.register(BusinessType)
admin.site.register(Business, BusinessAdmin)
# admin.site.registered(CustomerBusiness)
admin.site.register(ServiceCategory, ServiceCategoryAdmin)
admin.site.register(Service)
admin.site.register(Shop, ShopAdmin)
# admin.site.registered(ShopOpeningTimes)
admin.site.register(ShopStaff)
# admin.site.registered(CustomerShop)
admin.site.register(ResourceType)
admin.site.register(Resource, ResourceAdmin)
# admin.site.registered(ResourceRegularAvailability)
# admin.site.registered(StaffResourceAllocation)
# admin.site.registered(StaffResourceAllocationLog)
admin.site.register(ServiceGroup)
admin.site.register(Payment)
admin.site.register(CustomerBooking, CustomBookingAdmin)
admin.site.register(ServiceCustomerBooking)


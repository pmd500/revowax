revo_app.directive('agendaServicesSelect', function(AppStateService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_services_dir/select_services_tmpl.html',
        scope: {
            shop_details: "=shopDetails",
            selected_services: "=servicesServices",
            service_group: "=selectedGroup",
            editable: "=",
            booking_details:"=bookingDetails"
        },
        link: function (scope) {
            // Binded variables
            // scope.service_group = {};
            scope.service_groups = scope.shop_details.service_groups;

            // Internal Variables
            var num_of_services_remaining = 0;
            var remaining_service = null;

            // Set up form
            var init_form = function(){
                if(scope.editable){
                    // Find selected service group
                    var selected_service_group = _.find(scope.service_groups, function (service_group) {
                        return scope.booking_details.service_group == service_group.id;
                    });

                    // Select service group
                    scope.service_group.selected = selected_service_group

                    // Add selected services
                    scope.selected_services = scope.booking_details.services
                }
            }

            // Search string for an array of substrings
            var does_string_contain_all_substrings = function(array, string){
                var has_all_strings = true;
                _.each(array, function(substring){
                    // Is substring present in array
                    has_all_strings = has_all_strings && s.include(string.toLowerCase(), substring.toLowerCase())
                })
                return has_all_strings;
            }

            // Hides each service based on search terms and hides 
            var hide_services_and_hide_category = function (search_terms, services) {
                var hide_parent_category = true;
                services.forEach(function (service) {
                    var include_service = does_string_contain_all_substrings(search_terms, service.path)
                    service.hide = !include_service
                    if(include_service) {
                        num_of_services_remaining += 1;

                        // If first service that matches search terms
                        if(num_of_services_remaining == 1)
                            remaining_service = service;
                        hide_parent_category = false;
                    }
                })
                return hide_parent_category
            }

            // Traverse each category and filter the services base on search terms
            var traverse_subcategories = function (search_terms, parent_category) {
                var hide_parent;

                // No hide categories if there are no services showing
                if(search_terms.length > 0 ) {
                    hide_parent = true
                    parent_category.subcategories.forEach(function (category) {
                        if(category.subcategories.length > 0){
                            category.hide = traverse_categories(search_terms, category)

                            if(!category.hide){
                                hide_parent = false;
                            }
                        }
                        else if (category.services.length > 0){
                            category.hide = hide_services_and_hide_category(search_terms, category.services)

                            if(!category.hide){
                                hide_parent = false;
                            }
                        }
                    })
                }
                // No Search Terms
                else {
                    hide_parent = false;
                }
                return hide_parent
            }

            // Filtered Data
            scope.filter_options = function(search_terms, categories){
                var split_search_terms = search_terms.split(" ")
                num_of_services_remaining = 0
                categories.forEach(function(category){
                    category.hide = traverse_subcategories(split_search_terms, category)
                })
            }

            scope.show_pop_over = function() {
                scope.pop_over_is_visible = true;
            };

            scope.hide_pop_over = function () {
                scope.pop_over_is_visible = false;
            };

            // Locates player in the selected player array or returns null
            var locate_service = function (service) {
                var array_index = null
                scope.selected_services.forEach(
                    function (selected_service, index) {
                        if(selected_service.id==service.id)
                            array_index = index

                    }
                )
                return array_index
            }

            // Add Service
            scope.add_service = function (service){
                console.log("ADDD")
                var array_index = locate_service(service)

                // If player is not already selected
                if ((array_index == null))
                    scope.selected_services.push(service)

                // Call external method
                // scope.on_alteration()
            }


            // Remove Service
            scope.remove_service = function (service){
                // Call external method
                var array_index = locate_service(service)
                // Remove from array
                if(array_index != null)
                    scope.selected_services.splice(array_index, 1);

                // Call external method
                // scope.on_alteration()
            }
            
            // Add if service if only 1
            scope.add_service_on_enter = function () {
                if(num_of_services_remaining == 1){
                    scope.pop_over_is_visible = false;
                    scope.add_service(remaining_service)
                    scope.services_search_terms = ""
                    scope.filter_options(scope.services_search_terms, scope.service_group.selected.root_catergory.subcategories)
                }
            }

            // Init form
            init_form()

        }
    }
});
# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 12:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0042_auto_20170301_1247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staff',
            name='skills',
            field=models.ManyToManyField(blank=True, to='revo_booker.Skill'),
        ),
    ]

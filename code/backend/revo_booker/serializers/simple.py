from django.db import models
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from revo_booker.models import *

class SimpleProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Profile
        fields = ('__all__')

class SimpleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model =  User
        fields = ('id','username')


class SimpleCreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model =  User
        fields = ('id','username','password')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model =  User
        fields = '__all__'

class StaffTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model =  StaffType
        fields = '__all__'

class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Skill
        fields = '__all__'

class StaffSerializer(serializers.ModelSerializer):
    user = UserSerializer
    class Meta:
        model =  Staff
        fields = '__all__'

class StaffCustomAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model =  StaffCustomAvailability
        fields = '__all__'

class StaffRegularAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model =  StaffRegularAvailability
        fields = '__all__'

# class CustomerSerializer(serializers.ModelSerializer):
#     class Meta:
#         model =  Profile
#         fields = '__all__'

class BusinessTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model =  BusinessType
        fields = '__all__'

class BusinessSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Business
        fields = '__all__'

# class CustomerBusinessSerializer(serializers.ModelSerializer):
#     class Meta:
#         model =  Profile
#         fields = '__all__'

class ServiceCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model =  ServiceCategory
        fields = '__all__'

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Service
        fields = '__all__'

class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Shop
        fields = '__all__'

class ShopOpeningTimesSerializer(serializers.ModelSerializer):
    class Meta:
        model =  ShopOpeningTimes
        fields = '__all__'

class ShopStaffSerializer(serializers.ModelSerializer):
    class Meta:
        model =  ShopStaff
        fields = '__all__'

class CustomerShopSerializer(serializers.ModelSerializer):
    class Meta:
        model =  CustomerShop
        fields = '__all__'


class ResourceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model =  ResourceType
        fields = '__all__'

class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Resource
        fields = '__all__'

class ResourceRegularAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model =  ResourceRegularAvailability
        fields = '__all__'


class StaffResourceAllocationSerializer(serializers.ModelSerializer):
    class Meta:
        model =  StaffResourceAllocation
        fields = '__all__'

class StaffResourceAllocationLogSerializer(serializers.ModelSerializer):
    class Meta:
        model =  StaffResourceAllocationLog
        fields = '__all__'

class ServiceGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model =  ServiceGroup
        fields = '__all__'

class BookingStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model =  BookingStatus
        fields = '__all__'

class PaymentStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model =  PaymentStatus
        fields = '__all__'

class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Payment
        fields = '__all__'

class CustomerBookingSerializer(serializers.ModelSerializer):
    # customer = serializers.StringRelatedField()
    # services = serializers.StringRelatedField(many=True)
    class Meta:
        model =  CustomerBooking
        fields = '__all__'




class ServiceCustomerBookingSerializer(serializers.ModelSerializer):
    class Meta:
        model =  ServiceCustomerBooking
        fields = '__all__'

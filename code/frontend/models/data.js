// Set up enums
var catergory = {"Forhim": 0, "Forher": 1};
var subcatergory = {"Intimate": 0, "Body": 1, "Face": 2, "Combinations": 3};
var payment_types = {"Cash": 0, "Debit/Card": 1, "Paypal": 2, "Worldpay": 3};

////Salon Data
//var salon_data = {
//    name: "Kingscross",
//    address1: "8 York Road",
//    address2: "Kingscross",
//    TownCity: "London",
//    postcode: "SE280GF",
//    longitude: "000",
//    latitude: "000",
//    currency: "�",
//    accepted_payment_types: [0, 1, 2, 3],
//    staff: [{name: "Daniele", new_email: "daniele.perseguine@revowax.com"},
//        {name: "Phil", new_email: "phil.day@revowax.com"},
//        {name: "Derek", new_email: "derek.bryan@revowax.com"},
//        {name: "Katie", new_email: "katie@revowax.com"},
//        {name: "Tuca", new_email: "tuca@revowax.com"},
//    ],
//    opening_times: {
//        monday: "11:00am",
//        tuesdday: "11:00am",
//        wednesday: "11:00am",
//        thursday: "11:00am",
//        friday: "11:00am",
//        saturday: "11:00am",
//        sunday: "Closed",
//    },
//    closing_times: {
//        monday: "8:00pm",
//        tuesdday: "8:00pm",
//        wednesday: "8:00pm",
//        thursday: "8:00pm",
//        friday: "8:00pm",
//        saturday: "8:00pm",
//        sunday: "Closed",
//    },
//    // Categories of services
//    categories: [
//        {
//            name: "For Her",
//            subcategories: [
//                {
//                    name: "Intimate",
//                    services: [
//                        {name: "Hollywood", duration: "20", price: "40"},
//                        {name: "Brazilian", duration: "20", price: "35"},
//                        {name: "Bikini Line", duration: "20", price: "18"},
//                        {name: "Bikini Extended", duration: "20", price: "24"},
//                        {name: "Bottom Inside", duration: "10", price: "10"},
//                        {name: "Bottom Outside", duration: "20", price: "15"},
//                        {name: "Full Bottom", duration: "20", price: "25"},
//                    ]
//                },
//                {
//                    name: "Body",
//                    services: [
//                        {name: "Half Arms", duration: "20", price: "20"},
//                        {name: "Arms", duration: "30", price: "27"},
//                        {name: "Under Arm", duration: "10", price: "10"},
//                        {name: "Full Belly", duration: "10", price: "15"},
//                        {name: "Lower Belly", duration: "10", price: "3"},
//                        {name: "Lower Leg", duration: "20", price: "20"},
//                        {name: "Upper Leg", duration: "30", price: "22"},
//                        {name: "Full Leg", duration: "40", price: "40"},
//                    ]
//                },
//                {
//                    name: "Face",
//                    services: [
//                        {name: "Upper Lip", duration: "10", price: "10"},
//                        {name: "Eyebrows", duration: "20", price: "13"},
//                        {name: "Side Burns", duration: "10", price: "10"},
//                        {name: "Chin", duration: "10", price: "10"},
//                        {name: "Chin Upper Lip", duration: "20", price: "15"},
//                        {name: "Face (Each Part)", duration: "10", price: "5"},
//                    ]
//                },
//                {
//                    name: "Combinations",
//                    services: [
//                        {name: "Lower Leg + Hollywood", duration: "40", price: "51"},
//                        {name: "Lower Leg + Brazilian", duration: "40", price: "46"},
//                        {name: "Lower Leg + Bikini Line", duration: "40", price: "31"},
//                        {name: "Lower Leg + Bikini Extended", duration: "40", price: "36"},
//                        {name: "Upper Leg + Hollywood", duration: "40", price: "54"},
//                        {name: "Upper Leg + Brazilian", duration: "40", price: "50"},
//                        {name: "Upper Leg + Bikini Line", duration: "40", price: "34"},
//                        {name: "Upper Leg + Bikini Extended", duration: "40", price: "34"},
//                        {name: "Leg + Hollywood", duration: "60", price: "60"},
//                        {name: "Leg + Brazilian", duration: "60", price: "55"},
//                        {name: "Leg + Bikini Line", duration: "50", price: "43"},
//                        {name: "Leg + Bikini Extended", duration: "50", price: "48"},
//                    ]
//                }
//            ]
//        },
//        {
//            name: "For Him",
//            subcategories:
//            [
//                {
//                    name: "Face",
//                    services: [
//                        {name: "Beard", duration: "30", price: "30"},
//                        {name: "Ears", duration: "10", price: "7"},
//                        {name: "Nose", duration: "10", price: "7"},
//                        {name: "Face Parts", duration: "10", price: "7"},
//                        {name: "Eye Brows", duration: "10", price: "13"},
//                    ]
//                },
//                {
//                    name: "Body",
//                    services: [
//                        {name: "Full Back", duration: "40", price: "40"},
//                        {name: "Full Front", duration: "40", price: "40"},
//                        {name: "Shoulders", duration: "20", price: "10"},
//                        {name: "Chest", duration: "20", price: "23"},
//                        {name: "Stomach", duration: "20", price: "20"},
//                        {name: "Neck", duration: "10", price: "10"},
//                        {name: "Back Neck", duration: "10", price: "10"},
//                        {name: "Arms", duration: "40", price: "45"},
//                        {name: "Under Arms", duration: "10", price: "12"},
//                        {name: "Hands", duration: "10", price: "8"},
//                        {name: "Legs", duration: "60", price: "70"},
//                    ]
//                }
//            ]
//        },
//    ],
//};

var available_appointments = [
    {time: "9:20am"},
    {time: "11:00am"},
    {time: "11:40pm"},
    {time: "5:40pm"},
    {time: "6:00pm"},
    {time: "7:00pm"}
]




# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-26 10:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0016_shop_min_slot_duration'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resourceregularavailability',
            name='resource',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='availability', to='revo_booker.Resource'),
        ),
    ]

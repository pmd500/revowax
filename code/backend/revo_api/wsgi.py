"""
WSGI config for revo_api project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os, sys
import json
import time
import traceback
import signal
import pip
import site
from django.core.wsgi import get_wsgi_application


# Gets secrets & passwords from secrets.json file
def getSecrets(BASE_DIR):
    try:
        path = BASE_DIR + "/revo_api/secrets.json"
        json_data=open(path).read()
        data = json.loads(json_data)
        return data
    except:
        print "Please create secrets.json file in revo_api directory"
        exit()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Get confidential information
SECRETS = getSecrets(BASE_DIR);

# Set setting module
os.environ.setdefault("DJANGO_SETTINGS_MODULE", SECRETS["DJANGO_SETTINGS_MODULE"])

#Activate Virtual Env
try:
    application = get_wsgi_application()
    print 'WSGI without exception'
except Exception:
    print 'handling WSGI exception'
    # Error loading applications
    if 'mod_wsgi' in sys.modules:
        traceback.print_exc()
        os.kill(os.getpid(), signal.SIGINT)
        time.sleep(2.5)


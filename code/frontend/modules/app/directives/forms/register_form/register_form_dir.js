revo_app.directive('registerForm', function(UserService, $state) {
    return {
        restrict: 'E',
        scope: {
            cancelState:"@",
            guestNextState:"@",
            regUserNextState:"@",
            // Hides password form
            guestMode:"="
        },
        templateUrl: 'modules/app/directives/forms/register_form/register_form_tmpl.html',
        link: function (scope, element, attrs, tabsCtrl) {

            // Error message variables
            scope.error_message = "";
            scope.confirmation_password = ""
            
            // register
            scope.show_register_from = false;
            scope.receive_promotions = true;

            // Register new user
            scope.register = function () {
                // Set non reg user password
                if(!scope.show_register_from && scope.guestMode)
                    scope.password = "none"

                UserService.register(
                    scope.first_name,
                    scope.last_name,
                    scope.email,
                    scope.mobile,
                    scope.password,
                    scope.receive_promotions,
                    // registered user
                    (scope.show_register_from || !scope.guestMode),
                    // success callback
                    function () {
                        // Registered user
                        if(scope.show_register_from || !scope.guestMode) {
                            $state.go(scope.regUserNextState)
                        }
                        // Anonymous guest user registration
                        else {
                            $state.go(scope.guestNextState)
                        }
                    },
                    // failure callback
                    function (response) {
                        console.log(response)
                        if (response.status == 500) {
                            scope.error_message = "Server unavailable at the moment"
                        }
                        else {
                            scope.error_message = response.data.message
                        }

                    }
                );
            }
        }
    }
});
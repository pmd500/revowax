from __future__ import unicode_literals
import enum
from django.db import models
from django.contrib.auth.models import User
from choices import *

class Profile(models.Model):
    user = models.OneToOneField(User, null=True, blank=True)
    profile_type = models.CharField(choices=ProfileTypes.choices, default=ProfileTypes.customer, max_length=1)
    registered = models.BooleanField(default=False)
    receive_promotions = models.BooleanField(default=True)
    first_name = models.CharField(max_length=60, null=True, blank=True)
    last_name = models.CharField(max_length=60, null=True, blank=True)
    date_started = models.DateField(auto_now_add=True)
    mobile = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=120, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=1, choices=Gender.choices, null=True, blank=True)

    # Set file type on save
    def save(self, force_insert=False, force_update=False, using=None):
        # On update
        if self.registered:
            print "ONLY PEOPLE WHO TO REGISTER"
            # Update User Details
            self.user.first_name = self.first_name
            self.user.last_name = self.last_name
            self.user.mobile = self.mobile
            self.user.email = self.email
            self.user.username = self.email
            self.user.save()
        super(Profile, self).save(force_insert, force_update, using=None)

    def __unicode__(self):
        return unicode(self.first_name) + " " + unicode(self.last_name)

class StaffType(models.Model):
    role = models.CharField(max_length=30)
    def __unicode__(self):
        return self.role

    class Meta:
        verbose_name_plural = "Staff types"

class Skill(models.Model):
    name = models.CharField(max_length=60, null=True, blank=True)

    def __unicode__(self):
        return self.name

class Staff(models.Model):
    profile = models.OneToOneField(Profile)
    business = models.ForeignKey("Business")
    type = models.ForeignKey("StaffType")
    skills = models.ManyToManyField(Skill,blank=True)
    hourly_rate = models.FloatField(blank=True,null=True)

    def __unicode__(self):
        return (self.business.name + " -> " + unicode(self.profile.first_name) + " " + unicode(self.profile.last_name))

    class Meta:
        verbose_name_plural = "Staff"

class StaffCustomAvailability(models.Model):
    staff = models.ForeignKey(Staff, related_name="custom_availability")
    type = models.IntegerField(choices=AvailabilityTypes.choices)
    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()

class StaffRegularAvailability(models.Model):
    staff = models.OneToOneField(Staff, related_name="regular_availability")
    monday_is_available= models.BooleanField(default=True)
    monday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    monday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    tuesday_is_available= models.BooleanField(default=True)
    tuesday_start = models.TimeField(default="12:00:00",null=True, blank=True)
    tuesday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    wednesday_is_available= models.BooleanField(default=True)
    wednesday_start = models.TimeField(default="12:00:00",null=True, blank=True)
    wednesday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    thursday_is_available= models.BooleanField(default=True)
    thursday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    thursday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    friday_is_available= models.BooleanField(default=True)
    friday_start = models.TimeField(default="12:00:00",null=True, blank=True)
    friday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    saturday_is_available= models.BooleanField(default=True)
    saturday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    saturday_end = models.TimeField(default="18:00:00", null=True, blank=True)
    sunday_is_available= models.BooleanField(default=False)
    sunday_start = models.TimeField(null=True, blank=True)
    sunday_end = models.TimeField(null=True, blank=True)

# class Customer(models.Model):
#     profile = models.OneToOneField(Profile)
#     comments = models.TextField(null=True, blank=True)
#     def __unicode__(self):
#         return (unicode(self.profile.first_name) + " " + unicode(self.profile.last_name))
#
#     class Meta:
#         verbose_name_plural = "Customers"

class BusinessType(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

class Business(models.Model):
    name = models.CharField(max_length=60)
    address = models.CharField(max_length=30, null=True, blank=True)
    address2 = models.CharField(max_length=30, null=True, blank=True)
    town_city = models.CharField(max_length=20, null=True, blank=True)
    postcode = models.CharField(max_length=8, null=True, blank=True)
    email = models.CharField(max_length=120, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    website = models.CharField(max_length=120, null=True, blank=True)
    customers = models.ManyToManyField(Profile, through="CustomerBusiness")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Businesses"

class CustomerBusiness(models.Model):
    customer = models.ForeignKey(Profile)
    business = models.ForeignKey(Business)
    date_joined = models.DateField(auto_now_add=True)

# Tree of categories and sub categories
class ServiceCategory (models.Model):
    name = models.CharField(max_length=30)
    parent = models.ForeignKey('ServiceCategory', blank=True, null=True, related_name="subcategories", )
    class Meta:
        verbose_name_plural = "Service categories"

    def __unicode__(self):
        str = self.name
        is_first = True
        node = self
        groups = ""
        business = ""
        while(True):
            if(node.parent == None):
                groups = ServiceGroup.objects.filter(root_catergory=node)
                if(len(groups) > 0):
                    business = groups[0].business
                else:
                    str = str = node.name
                break
            else:
                node = node.parent
            str = node.name + " -> " + str

        try:
            str = business.name + " -> " + groups[0].name + " -> " + str
        except:
            str = str
        return str

class Service(models.Model):
    Business = models.ForeignKey(Business)
    service_group = models.ForeignKey("ServiceGroup", related_name="services")
    name = models.CharField(max_length=64)
    price = models.FloatField()
    minutes = models.IntegerField()
    category = models.ForeignKey(ServiceCategory, related_name="services", null=True, blank=True)

    def save(self, force_insert=False, force_update=False):

        def get_service_group():
            service_group = None
            while (True):
                if (node.parent == None):
                    groups = ServiceGroup.objects.filter(root_catergory=node)
                    if (len(groups) > 0):
                        business = groups[0].business
                    break
                else:
                    node = node.parent
            return service_group

        # Set service group
        self.service_group = get_service_group()
        self.business = self.service_group.business

        super(Service, self).save(force_insert, force_update)

    def __unicode__(self):
        return self.name

class Shop(models.Model):
    name = models.CharField(max_length=30)
    treatwell_name = models.CharField(max_length=30, blank=True, null=True)
    business = models.ForeignKey(Business, related_name="shops")
    address = models.CharField(max_length=30, null=True, blank=True)
    address2 = models.CharField(max_length=30, null=True, blank=True)
    town_city = models.CharField(max_length=20, null=True, blank=True)
    postcode = models.CharField(max_length=8, null=True, blank=True)

    # Minimum appointment slots
    min_slot_duration = models.IntegerField(default=20)

    # Other Info
    cached_service_tree = models.TextField(null=True, blank=True)
    cached_at = models.DateTimeField(null=True, blank=True)
    customers = models.ManyToManyField(Profile, through="CustomerShop")
    staff = models.ManyToManyField(Staff, through="ShopStaff")
    service_groups = models.ManyToManyField("ServiceGroup", blank=True)

    def __unicode__(self):
        return self.business.name + " -> " + self.name

class ShopOpeningTimes(models.Model):
    shop = models.OneToOneField(Shop, related_name="opening_times")
    monday_is_open= models.BooleanField(default=True)
    monday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    monday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    tuesday_is_open= models.BooleanField(default=True)
    tuesday_start = models.TimeField(default="12:00:00",null=True, blank=True)
    tuesday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    wednesday_is_open= models.BooleanField(default=True)
    wednesday_start = models.TimeField(default="12:00:00",null=True, blank=True)
    wednesday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    thursday_is_open= models.BooleanField(default=True)
    thursday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    thursday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    friday_is_open= models.BooleanField(default=True)
    friday_start = models.TimeField(default="12:00:00",null=True, blank=True)
    friday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    saturday_is_open= models.BooleanField(default=True)
    saturday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    saturday_end = models.TimeField(default="18:00:00", null=True, blank=True)
    sunday_is_open= models.BooleanField(default=False)
    sunday_start = models.TimeField(null=True, blank=True)
    sunday_end = models.TimeField(null=True, blank=True)

    def __unicode__(self):
        return self.shop.business.name + " -> " + self.shop.name

    class Meta:
        verbose_name_plural = "Shop opening times"

class ShopStaff(models.Model):
    staff = models.ForeignKey(Staff)
    shop = models.ForeignKey(Shop)
    date_joined = models.DateField(auto_now_add=True)
    staff_type = models.ForeignKey(StaffType)

class CustomerShop(models.Model):
    customer = models.ForeignKey(Profile)
    shop = models.ForeignKey(Shop)
    date_joined = models.DateField(auto_now_add=True)

# Defines the type resource is
class ResourceType(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

# Each resource has one calendar
class Resource(models.Model):
    name = models.CharField(max_length=30, null=True, blank=True)
    shop = models.ForeignKey(Shop, related_name="resources")
    staff_allocation = models.ManyToManyField(Staff, through="StaffResourceAllocation")
    type = models.ForeignKey(ResourceType)

    def __unicode__(self):
        return self.shop.business.name + " -> " + self.shop.name + " -> " + self.name

    class Meta:
        verbose_name_plural = "Resources"

class ResourceRegularAvailability(models.Model):
    resource = models.OneToOneField(Resource, related_name="availability")
    match_store_opening_times = models.BooleanField(default=True)
    monday_is_available = models.BooleanField(default=True)
    monday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    monday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    tuesday_is_available = models.BooleanField(default=True)
    tuesday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    tuesday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    wednesday_is_available = models.BooleanField(default=True)
    wednesday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    wednesday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    thursday_is_available = models.BooleanField(default=True)
    thursday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    thursday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    friday_is_available = models.BooleanField(default=True)
    friday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    friday_end = models.TimeField(default="20:00:00", null=True, blank=True)
    saturday_is_available = models.BooleanField(default=True)
    saturday_start = models.TimeField(default="12:00:00", null=True, blank=True)
    saturday_end = models.TimeField(default="18:00:00", null=True, blank=True)
    sunday_is_available = models.BooleanField(default=False)
    sunday_start = models.TimeField(null=True, blank=True)
    sunday_end = models.TimeField(null=True, blank=True)

    def __unicode__(self):
        return self.resource.shop.business.name + " -> " + self.resource.shop.name + " -> " + self.resource.name

# Each resource has one calendar
class StaffResourceAllocation(models.Model):
    resource = models.ForeignKey(Resource)
    staff = models.ForeignKey(Staff)
    day_of_week = models.CharField(max_length=1, choices=DaysOfWeek.choices)
    start_time = models.TimeField(default="12:00:00")
    end_time = models.TimeField(default="20:00:00")

    def __unicode__(self):
        return self.resource.shop.business.name + " -> " + self.resource.shop.name + " -> " + self.resource.name + " (" + self.resource.type.name+ ") -> " + self.staff.first_name + " " + self.staff.last_name + " -> " + self.day_of_week

    class Meta:
        verbose_name_plural = "Staff Resource Allocations"

# Script runs daily
class StaffResourceAllocationLog(models.Model):
    business = models.ForeignKey(Business)
    shop = models.ForeignKey(Shop, related_name="staff_resource_allocation_log")
    resource = models.ForeignKey(Resource, related_name="staff_resource_allocation_log")
    staff = models.ForeignKey(Staff, related_name="staff_resource_allocation_log")
    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()
    hourly_rate = models.FloatField()
    total_pay = models.FloatField() 
    
    def __unicode__(self):
        return self.resource.shop.name + " " + self.resource.type.name + " "  + self.staff.name

    class Meta:
        verbose_name_plural = "Staff Resource Allocation Logs"

# Service settings i.e. resource dependencies and restrictions
class ServiceGroup(models.Model):
    name = models.CharField(max_length=30)
    business = models.ForeignKey(Business, null=True, blank=True, related_name="service_groups")
    root_catergory = models.ForeignKey(ServiceCategory, null=True, blank=True)
    resource_needed = models.ForeignKey(ResourceType, related_name="service_groups_used_for")

    def __unicode__(self):
        return self.business.name + " -> " + self.name

class Payment(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.customer.name


class CustomerBookingNotes(models.Model):
    notes = models.TimeField()


class CustomerBooking(models.Model):
    booking_source = models.CharField(max_length=2, choices=BookingSources.choices, default=BookingSources.revowax_website)
    booked_by = models.ForeignKey(Profile, blank=True, null=True)
    booked_at = models.DateTimeField()
    notes = models.OneToOneField(CustomerBookingNotes, null=True, blank=True)
    customer = models.ForeignKey(Profile, related_name="bookings")
    business = models.ForeignKey(Business, related_name="bookings")
    shop = models.ForeignKey(Shop, related_name="bookings")
    service_group = models.ForeignKey(ServiceGroup, related_name="bookings", blank=True, null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    duration = models.IntegerField()
    total_price = models.FloatField()
    booking_status = models.IntegerField(choices=BookingStatus.choices, default=BookingStatus.created)
    payment_status = models.IntegerField(choices=PaymentStatus.choices, default=PaymentStatus.pending)
    resource = models.ForeignKey(Resource, related_name="bookings")
    staff = models.ForeignKey(Staff, null=True, blank=True, related_name="bookings")
    services = models.ManyToManyField(Service, through="ServiceCustomerBooking")

    def __unicode__(self):
        return (unicode(self.customer.first_name) + " " + unicode(self.customer.last_name) + " at " + self.business.name + " " + self.shop.name + " on " + self.start_time.__str__())


class TreatwellBooking(models.Model):
    booking = models.OneToOneField(CustomerBooking, null=True, blank=True, related_name="treatwell_details")
    booking_ref = models.CharField(max_length=15)
    booking_link = models.CharField(max_length=100)
    confirmation_link = models.CharField(max_length=100)
    reschedule_link = models.CharField(max_length=100)
    payment_status = models.CharField(max_length=20)
    services = models.TextField()


class ServiceCustomerBooking(models.Model):
    service = models.ForeignKey(Service)
    customer_booking = models.ForeignKey(CustomerBooking)
    price = models.FloatField(blank = True, null=True)
    duration = models.IntegerField(blank = True, null=True)

    def __unicode__(self):
        return self.service.category.__unicode__() + " -> " + self.service.__unicode__() + " ( " + unicode(self.customer_booking.id)  + " )"
revo_app.factory("BookingService", ["$q","UtilityService","$http", "AppStateService", function ($q, UtilityService, $http, AppStateService) {
    // Bind to var
    var service = this;

    // Get available bookings for shop's service group
    service.get_available_booking_slots = function (
        business_id,
        shop_id,
        service_group_id,
        duration,
        start_date,
        end_date,
        //Callbacks
        success_external_callback,
        fail_external_callback
    ) 
    {
        // Start loading spinner
        UtilityService.spinner.start()

        // Call shop webservice
        $http({
            method: 'post',
            url: base_url + 'booking-availability/',
            data: {
                "business_id": business_id,
                "shop_id": shop_id,
                "service_group_id": service_group_id,
                "duration": duration,
                "start_date": start_date,
                "end_date": end_date
            }
        })
        .then(
            // SUCCESS
            function success_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Call success callback
                success_external_callback(response.data);
            },

            // FAIL
            function fail_callback(response) {

                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Call back
                fail_external_callback();

            }
        );
    }
    
    // Get available bookings for shop's service group
    service.book = function (
        booked_by_profile_id,
        customer_profile_id,
        business_id,
        shop_id,
        resource_id,
        service_group_id,
        staff_id,
        start_datetime,
        end_datetime,
        duration,
        booking_status_id,
        payment_status_id,
        total_price,
        service_list,
        //Callbacks
        success_external_callback,
        fail_external_callback
    )
    {
        // Start loading spinner
        UtilityService.spinner.start()


        var payload = {
            "business_id": business_id,
                "shop_id": shop_id,
                "booked_by_profile_id": booked_by_profile_id,
                "customer_profile_id": customer_profile_id,
                "resource_id": resource_id,
                "service_group_id": service_group_id,
                "staff_id": staff_id,
                "start_datetime": start_datetime,
                "end_datetime": end_datetime,
                "duration": duration,
                "booking_status_id": booking_status_id,
                "payment_status_id": payment_status_id,
                "total_price": total_price,
                "service_list":  service_list
        }
        p(payload)

        // Call shop webservice
        $http({
            method: 'post',
            url: base_url + 'new-booking/',
            // headers: {
            //     'Authorization': "Token " + AppStateService.user_state.token,
            // },
            data: payload
        })
        .then(
            // SUCCESS
            function success_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Call success callback
                success_external_callback(response.data);
            },

            // FAIL
            function fail_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Call back
                fail_external_callback();

            }
        );
    }

    // Get available bookings for shop's service group
    service.update_booking = function (
        booking_id,
        booked_at,
        booked_by_profile_id,
        customer_profile_id,
        business_id,
        shop_id,
        resource_id,
        service_group_id,
        staff_id,
        start_datetime,
        end_datetime,
        duration,
        booking_status_id,
        payment_status_id,
        total_price,
        service_list,
        //Callbacks
        success_external_callback,
        fail_external_callback
    )
    {
        // Start loading spinner
        UtilityService.spinner.start()

        // Set payload
        var payload = {
            "business": business_id,
            "shop": shop_id,
            "booked_by": booked_by_profile_id,
            "booked_at":booked_at,
            "customer": customer_profile_id,
            "resource": resource_id,
            "service_group": service_group_id,
            "staff": staff_id,
            "start_time": start_datetime,
            "end_time": end_datetime,
            "duration": duration,
            "booking_status": booking_status_id,
            "payment_status": payment_status_id,
            "total_price": total_price,
            "services":  service_list
        }

        // Call shop webservice
        $http({
            method: 'put',
            url: base_url + 'update-booking/'+booking_id + "/",
            headers: {
                'Authorization': "Token " + AppStateService.user_state.token,
            },
            data: payload
        })
        .then(
            // SUCCESS
            function success_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()
                // Call success callback
                success_external_callback(response.data);
            },

            // FAIL
            function fail_callback(response) {
                console.log("ERROR")
                console.log(response)
                // Default Stop Spinner
                UtilityService.spinner.stop()
                // Call back
                fail_external_callback();

            }
        );
    }


    // Get available bookings for shop's service group
    service.get_customer_bookings = function (
        customer_id,
        //Callbacks
        success_external_callback,
        fail_external_callback
    )
    {
        // Start loading spinner
        UtilityService.spinner.start()

        // Call shop webservice
        $http({
            method: 'get',
            url: base_url + 'customer/'+customer_id+'/bookings/',
            headers: {
                'Authorization': "Token " + AppStateService.user_state.token,
            }
        })
            .then(
                // SUCCESS
                function success_callback(response) {
                    // Default Stop Spinner
                    UtilityService.spinner.stop()

                    // Call success callback
                    success_external_callback(response.data);
                },

                // FAIL
                function fail_callback(response) {
                    // Default Stop Spinner
                    UtilityService.spinner.stop()

                    console.log(response)

                    // Call back
                    fail_external_callback();

                }
            );
    }

    // Cancel Customer booking
    service.cancel_customer_booking = function (
        booking_id,
        //Callbacks
        success_external_callback,
        fail_external_callback
    )
    {
        // Start loading spinner
        UtilityService.spinner.start()

        // Call shop webservice
        $http({
            method: 'patch',
            url: base_url + 'bookings/'+booking_id + "/",
            headers: {
                'Authorization': "Token " + AppStateService.user_state.token,
            },
            data:{
                "booking_status": BOOKING_STATUS.CANCELLED
            }
        })
        .then(
            // SUCCESS
            function success_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()

                // Call success callback
                success_external_callback(response.data);
            },

            // FAIL
            function fail_callback(response) {
                // Default Stop Spinner
                UtilityService.spinner.stop()

                console.log(response)

                // Call back
                fail_external_callback();

            }
        );
    }

    return service;
}]);

revo_app.controller("TestCtl", ["ShopService","RevowaxShopDetails" ,"BookingService", function(ShopService, RevowaxShopDetails, BookingService){
    var ctl = this;
    console.log("TEST MODE");
    console.log(RevowaxShopDetails);

    // Parameters
    var business_id = 1
    var shop_id = 3
    var service_group_id = 1
    var duration = 60
    var start_date = "2017-01-20"
    var end_date = "2017-01-27"

    // Results
    available_appointments = []
    // Get available bookings
    BookingService.get_available_booking_slots (
        business_id,
        shop_id,
        service_group_id,
        duration,
        start_date,
        end_date,
        // Success
        function(appointsments){
            available_appointments = appointsments
            book_appointment()
        },
        // Error
        function(){

        }
    )

    var book_appointment = function () {
        // Get first available appointment
        console.log(available_appointments[0])
        var a = available_appointments[0]

        var service_list = [
            {
                "service_id": 4,
                "price": 20,
                "duration":20
            },
            {
                "service_id": 3,
                "price": 20,
                "duration":20
            },
            {
                "service_id": 3,
                "price": 20,
                "duration":20
            }
        ]

        BookingService.book(
            business_id,
            shop_id,
            1,
            a.resource_id,
            service_group_id,
            "",
            a.start,
            a.end,
            duration,
            2,
            1,
            60,
            service_list,
            // Security
            "c89292f92f2fcb81c58fb8d47eb544c81cb77dd4",
            //Callbacks
            function(data){
                console.log(data)
            },
            function(){

            }
        )
    }



    return ctl
}]);
/* Stores all the details for making a booking */
revo_app.service("BookingDetailsModel", ["$rootScope", function($rootScope) {
    var data = {
        chosen_treatment_data : [],
        chosen_treatments : [],
        chosen_treatment_paths : [],
        total_price : 0,
        total_duration : 0,
        room_number: 0,
        desired_start_datetime : new Date(),
        available_appointments : [],
        selected_start_datetime : new Date(),
        selected_end_datetime : new Date(),
        booked_appointments : [],
        status : "Unconfirmed"
        };

    // Set loading status of GetAvailableAppointments Web Service
    var BroadCastChange = function () {
        $rootScope.$broadcast('BookingDetailsModel:updated', data);
    }

    // Change just the date of the datetime
    this.ChangeDesiredDate = function (datetime) {
        var oldDate = data.desired_start_datetime;
        var newDate = new Date(datetime.getFullYear(), datetime.getMonth(), datetime.getDate(), oldDate.getHours(), oldDate.getMinutes(), oldDate.getSeconds(), oldDate.getMilliseconds());
        data.desired_start_datetime = newDate;
        BroadCastChange();
    }

    // Change just the time of the datetime
    this.ChangeDesiredTime = function (datetime) {
        var oldDate = data.desired_start_datetime;
        var newDate = new Date(oldDate.getFullYear(), oldDate.getMonth(), oldDate.getDate(),  datetime.getHours(), datetime.getMinutes(), datetime.getSeconds(), datetime.getMilliseconds());
        data.desired_start_datetime = newDate;
        BroadCastChange();
    }

    // Calculate End Time of Appointment
    var CalcEndDateTime = function(start_datetime, durations_minutes) {
        return new Date(start_datetime.getTime() + durations_minutes*60000);
    }

    // Universal Getter
    this.GetBookingDetails = function () {
        return data;
    }

    // Setters
    this.SetChosenTreatmentData = function(chosen_treatment_data){
        data.chosen_treatment_data = chosen_treatment_data;
        BroadCastChange();
    }

    this.SetChosenTreatments = function(chosen_treatments){
        data.chosen_treatments = chosen_treatments;
        BroadCastChange();
    }

    this.SetChosenTreatmentPaths = function(chosen_treatment_paths){
        data.chosen_treatment_paths = chosen_treatment_paths;
        BroadCastChange();
    }

    this.SetTotalPrice = function(total_price){
        data.total_price = total_price;
        BroadCastChange();
    }

    this.SetTotalDuration = function(total_duration){
        data.total_duration = total_duration;
        BroadCastChange();
    }

    this.SetDesiredStartTime = function(desired_start_datetime){
        this.desired_start_datetime = desired_start_datetime;
        BroadCastChange();
    }

    this.SetAvailableAppointments = function(available_appointments){
        data.available_appointments = available_appointments;
        BroadCastChange();
    }

    this.SetSelectedStartDateTime = function(selected_start_datetime){
        data.selected_start_datetime = selected_start_datetime;
        BroadCastChange();
    }

    this.SetRoomNumber = function(room_number){
        data.room_number = room_number;
        BroadCastChange();
    }

    this.SetBookedAppointments = function(booked_appointments){
        data.booked_appointments = booked_appointments;
        BroadCastChange();
    }

    this.SetSelectedEndDateTime = function(selected_start_datetime, duration){
        data.selected_end_datetime = CalcEndDateTime(data.desired_start_datetime, data.total_duration);
        BroadCastChange();
    }

    this.SetStatus = function(status){
        data.status = status;
        BroadCastChange();
    }
}]);
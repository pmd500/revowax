import os
import json

# Gets secrets & passwords from secrets.json file
def getSecrets(BASE_DIR):
    try:
        path = os.path.join(BASE_DIR, "secrets.json")
        json_data=open(path).read()
        data = json.loads(json_data)
        return data
    except:
        print ("No /code/backend/revo_api/secrets.json present, please create a secrets.json file in the following format:")
        print ("""
            {
              "DATABASES" :
                {
                  "default": {
                    "NAME": "<db_name>",
                    "HOST": "<db_host>",
                    "PORT": "<db_port>",
                    "USER": "<db_user>",
                    "PASSWORD": "<db_password>"
                  }
                }
              ,
             "SECRET_KEY" : "<secret_key>",
             "DJANGO_SETTINGS_MODULE" : "revo_api.settings.<local/development/production>"
            }
        """)
        exit()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Get confidential information
SECRETS = getSecrets(BASE_DIR);

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = SECRETS["SECRET_KEY"]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'corsheaders',
    'django.contrib.sites',
    'allauth',
    'allauth.account',
    'rest_auth.registration',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.twitter',
    'django_ical',
    'revo_booker',
]

CORS_ORIGIN_ALLOW_ALL = True

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'revo_api.urls'
path = os.path.join(BASE_DIR, '../revo_templates')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [path, ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


DATABASES = {
    'default': {
        'NAME': SECRETS["DATABASES"]["default"]["NAME"],
        'HOST': SECRETS["DATABASES"]["default"]["HOST"],
        'PORT': SECRETS["DATABASES"]["default"]["PORT"],
        'ENGINE': SECRETS["DATABASES"]["default"]["ENGINE"],
        'USER': SECRETS["DATABASES"]["default"]["USER"],
        'PASSWORD': SECRETS["DATABASES"]["default"]["PASSWORD"],
    },
}

WSGI_APPLICATION = 'revo_api.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

SITE_ID = 1

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, "../revo_static/")
STATIC_URL = '/static/'
#
# REST_FRAMEWORK = {
#     'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
#     'PAGE_SIZE': 10
# }

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        # 'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    )
}

STANDARD_DATETIME_STRING_FORMAT = "%Y-%m-%d %H:%M"
STANDARD_DATE_STRING_FORMAT = "%Y-%m-%d"

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

INTERNAL_IPS = '127.0.0.1'

EMAIL_BACKEND = "sgbackend.SendGridBackend"
DEFAULT_FROM_EMAIL = "reset@revowax.com"
# EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

SENDGRID_API_KEY = SECRETS["SEND_GRID_API_KEY"]
LOGOUT_ON_PASSWORD_CHANGE = False
OLD_PASSWORD_FIELD_ENABLED = True

# Turn of timezone
USE_TZ = False
# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-23 15:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0026_auto_20161223_1018'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='servicecategory',
            name='has_services',
        ),
    ]

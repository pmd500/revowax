# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import sendgrid
import urllib2 as urllib

sg = sendgrid.SendGridAPIClient(apikey='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')
data = {
  "personalizations": [
    {
      "to": [
        {
          "email": "fatmansfridge@msn.com"
        }
      ],
      "substitutions": {
        "[name]": "Daniele",
        "[date]": "11am - 3rd January 2017",
        "[price]": "40.00",
        "[duration]": "50"
      },
      "subject": "Revowax - Booking Confirmation"
    },
  ],
  "from": {
    "email": "philip.day@live.co.uk"
  },
  "content": [
    {
      "type": "text/html",
      "value": "Revowax - Booking Confirmation"
    }
  ],
  "template_id": "89753c90-c1c1-4277-82ba-fa32acb52457"
}

try:
    response = sg.client.mail.send.post(request_body=data)
except urllib.HTTPError as e:
    print e.read()
    exit()
print(response.status_code)
print(response.body)
print(response.headers)
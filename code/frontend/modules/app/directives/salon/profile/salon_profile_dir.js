revo_app.directive('salonProfile', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/salon/profile/salon_profile_tmpl.html',
        scope: {
            // variables
            shop:"=",
        },
        link : function (scope) {
            // Init Scope
            scope.map = { center: { latitude: 51.5315740, longitude: -0.1223946 }, zoom: 15 };
            scope.marker = {
                id: 0,
                coords: {
                    latitude: 51.5315740,
                    longitude: -0.1223946
                },
                options: { draggable: true },
            };
        }
    };
});
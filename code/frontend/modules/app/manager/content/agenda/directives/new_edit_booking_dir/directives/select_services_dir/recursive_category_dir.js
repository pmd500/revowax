revo_app.directive('recursiveCategory', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_services_dir/recursive_category_tmpl.html',
        scope: {
            categories: "=",
            level : "=",
            selected_services: "=selectedServices"
        },
        link: function (scope) {
       
            
            scope.next_level = scope.level + 1
            var indent_string = "-";
            scope.repeated_indent_string = indent_string.repeat(scope.level)
        }
    }
});
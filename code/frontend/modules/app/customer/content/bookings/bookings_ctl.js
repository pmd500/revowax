revo_app.controller("CustomerBookingsCtl", ["BookingService", "AppStateService", function(BookingService, AppStateService ){
    var ctl = this;

    // Binded variables
    ctl.bookings = []

    // Get Booked Appointments
    var get_bookings = function(){
        console.log("AppStateService.user_state.customer_id")
        console.log(AppStateService.user_state.customer_id)

        BookingService.get_customer_bookings(
            // Customer ID
            AppStateService.user_state.customer_id,
            // On Success
            function (bookings) {
                ctl.bookings = bookings
            },
            // On Failure
            function () {
                console.log("ERROR")
            }

        )
    }

    // Cancel booking
    ctl.cancel_bookings= function(id){

        BookingService.cancel_customer_booking(
            // Appointment ID
            id,
            // On Success
            function () {
                // Update bookings
                get_bookings()
            },
            // On Failure
            function () {
                console.log("ERROR")
            }
        
        );
    
    }

    // Converts string date time to datetime
    ctl.convert_date_string = function(string){
        return new Date(string);
    }

    // Call webservice
    get_bookings()

}]);

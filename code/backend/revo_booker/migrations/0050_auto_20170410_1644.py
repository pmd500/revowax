# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-10 16:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0049_auto_20170406_1659'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='stafftype',
            name='role',
        ),
        migrations.AlterField(
            model_name='profile',
            name='mobile',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]

/**
 * calendarDemoApp - 0.9.0
 */
revo_app.controller('ManagerAgendaCtl', function ($compile, $timeout, uiCalendarConfig, $uibModal,BusinessService, ShopService, ShopBookings, ExistingCustomers, RevowaxShopDetails) {
    var ctl = this;
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    // Options
    var is_modal_open = false;

    // Get Today Date
    var today = new Date();
    var three_months_ahead = new Date();

    // Set the date for three months ahead
    three_months_ahead.setMonth(today.getMonth() + 3);

    // Init Calender to be empty
    ctl.events = [];
    ctl.existing_customers = ExistingCustomers;
    ctl.shop_details = RevowaxShopDetails;


    // Tooltip
    ctl.tooltip_name = ""
    ctl.tooltip_number = ""
    ctl.tooltip_date = ""
    ctl.tooltip_time= ""
    ctl.tooltip_price = ""
    ctl.tooltip_duration = ""
    ctl.tooltip_services = []
    ctl.tooltip_payment_status = "Prepaid by customer"
    ctl.tooltip_source = "Booked by RevoWax"

    // Webservice Successfully called, add bookings to data
    var add_bookings_to_calendar = function (bookings) {
        ctl.events.splice(0, ctl.events.length)

        // loop through all bookings
        bookings.forEach(function (booking) {
            // Generate Correct Colour
            var colour;
            if ((booking.payment_status == PAYMENT_STATUS.PAID) || (booking.payment_status == PAYMENT_STATUS.ONLINE)) {
                colour = "#5bb944";
            }
            else if (booking.booking_status == BOOKING_STATUS.NOSHOW) {
                colour = "#ff6600";
            }
            else {
                colour = "#0099cc";
            }

            // Find customer linked to booking
            console.log("booking")
            console.log(booking);
            var customer = _.find(ExistingCustomers, function (customer) {
                    return customer.id == booking.customer;
                });

            // get service group
            // console.log(booking)

            var service_group = _.find(RevowaxShopDetails.service_groups, function(service_group){
                return service_group.id = booking.service_group
            })


            var services = _.map(booking.services, function (booked_service_id) {
                return _.find(service_group.services, function (service) {
                    return service.id == booked_service_id;
                });
            })

            // Add full customer details to booking info
            booking.customer = customer
            booking.services = services

            // Add bookings to events
            ctl.events.push({
                    id: booking.id,
                    title: "Customer: " + customer.first_name + " " + (customer.last_name || "") + " (" + booking.duration + " mins)",
                    booking: booking,
                    customer: customer,
                    start: new Date(booking.start_time),
                    end: new Date(booking.end_time),
                    allDay: false,
                    stick: true,
                    color: colour,
                }
            )
        })
    };

    // Add bookings
    add_bookings_to_calendar(ShopBookings)


    var refresh_existing_customers = function () {
        BusinessService.get_business_customers(
            BUSINESS_ID,
            //success
            function (data) {
                ctl.existing_customers = data;
            },
            // fail
            function () {
                // do nothing
            }
        )
    }

    /* alert on eventClick */
    ctl.alert_on_event_click = function (event, jsEvent, view) {
        $(".revo-booking-tooltip").hide();
        open_edit_modal(event.start, event.end, event.id, event.booking, ctl.renderCalender)
    };

    /* alert on Drop */
    ctl.alert_on_drop = function (event, delta, revertFunc, jsEvent, ui, view) {
        ctl.alertMessage = ('Event Dropped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    ctl.alert_on_resize = function (event, delta, revertFunc, jsEvent, ui, view) {
        ctl.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };

    /* add and removes an event source of choice */
    ctl.add_remove_event_source = function (sources, source) {
        var canAdd = 0;
        angular.forEach(sources, function (value, key) {
            if (sources[key] === source) {
                sources.splice(key, 1);
                canAdd = 1;
            }
        });
        if (canAdd === 0) {
            sources.push(source);
        }
    };

    /* add custom event*/
    ctl.add_event = function () {
        ctl.events.push({
            title: 'Open Sesame',
            start: new Date(y, m, 28),
            end: new Date(y, m, 29),
            className: ['openSesame']
        });
    };

    /* remove event */
    ctl.remove = function (index) {
        ctl.events.splice(index, 1);
    };

    /* Change View */
    ctl.changeView = function (view, calendar) {
        console.log("CHANGED VIEW")
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        ctl.renderCalender(calendar);
    };

    /* Change View */
    ctl.renderCalender = function (calendar) {
        console.log("CAL RENDERED")
        $timeout(function () {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        });
    };

    /* Render Tooltip */
    ctl.eventRender = function (event, element, view) {
        // element.qtip({
        //     content: event.booking
        // });
    };

    /* config object */
    ctl.uiConfig = {
        calendar: {
            editable: true,
            ignoreTimezone: false,
            locale: 'en-gb',
            timezone:'local',
            height: window.innerHeight - 100,
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultView: "agendaWeek",
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                $(".revo-booking-tooltip").hide();
                open_new_modal(start, end, ctl.renderCalender);
            },
            eventClick: ctl.alert_on_event_click,
            eventDrop: ctl.alert_on_drop,
            eventResize: ctl.alert_on_resize,
            eventRender: ctl.eventRender,
            // businessHours:  // specify an array instead
            // {
            //     dow: [ 1, 2, 3,4,5,6], // Monday, Tuesday, Wednesday
            //     start: '12:00', // 8am
            //     end: '20:00' // 6pm
            // },
            minTime:"07:00",
            maxTime:"21:00",
            eventMouseover: function(calEvent, e) {
                // If actual event
                if(calEvent.title != null){
                    var $tooltip = $(".revo-booking-tooltip")
                    var div_height = $tooltip.height()
                    var div_width = $tooltip.width()
                    var window_height = $(window).height()
                    var window_width = $(window).width()

                    // Tooltip Data
                    ctl.tooltip_name = calEvent.customer.first_name + " " + calEvent.customer.last_name;
                    ctl.tooltip_number = "07506788205"
                    ctl.tooltip_date =  new Date(calEvent.booking.start_time);
                    ctl.tooltip_price = calEvent.booking.total_price;
                    ctl.tooltip_duration = calEvent.booking.duration;
                    ctl.tooltip_services = calEvent.booking.services;
                    ctl.tooltip_payment_status = "Prepaid by customer";
                    ctl.tooltip_source = "Booked by RevoWax";

                    var y_position = e.pageY;
                    var x_position = e.pageX + 50;

                    // Correct position
                    if (e.pageY > (window_height/2))
                        y_position = e.pageY - div_height;
                    // Correct position
                    if (e.pageX > (window_width/2))
                        x_position = e.pageX - div_width - 50;

                    $tooltip.css({
                        top: y_position + "px",
                        left: x_position + "px"
                    })


                    var show_timer;
                    clearTimeout(show_timer)
                    show_timer = setTimeout(function() {
                        $(".revo-booking-tooltip").show();
                    }, 500);
                }
                // $(this).css('z-index', 8);

                // $tooltip.fadeIn('500');
                // $tooltip.fadeTo('10', 1.9);
            
                // // Show Tooltip
                // $(this).mouseover(function(e) {
                //
                //     // var y_position = e.pageY;
                //     // var x_position = e.pageX;
                //     //
                //     // // Correct position
                //     // if (e.pageY > (window_height/2))
                //     //     y_position = e.pageY - div_height;
                //     // // Correct position
                //     // if (e.pageX > (window_width/2))
                //     //     x_position = e.pageX - div_width;
                //     //
                //     // console.log("top over",y_position )
                //     // console.log("left over",x_position )
                //
                //
                // }).mousemove(function(e) {
                //
                // });
            },
            eventMouseout: function(calEvent, jsEvent) {
                if(!is_modal_open){
                    // $(this).css('z-index', 8);
                    var show_timer;
                    clearTimeout(show_timer)
                    show_timer = setTimeout(function() {
                        $(".revo-booking-tooltip").hide();
                    }, 500);
                }
            }
        }
    };

    ctl.changeLang = function () {
        if (ctl.changeTo === 'Hungarian') {
            ctl.uiConfig.calendar.dayNames = ["Vas�rnap", "H�tf?", "Kedd", "Szerda", "Cs�t�rt�k", "P�ntek", "Szombat"];
            ctl.uiConfig.calendar.dayNamesShort = ["Vas", "H�t", "Kedd", "Sze", "Cs�t", "P�n", "Szo"];
            ctl.changeTo = 'English';
        } else {
            ctl.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            ctl.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            ctl.changeTo = 'Hungarian';
        }
    };

    ctl.event_sources = [ctl.events];

    // New Appointment Modal Control
    var open_new_modal = function (start, end, id) {
        is_modal_open = true;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: "modules/app/manager/content/agenda/modals/new_booking_modal_tmpl.html",
            controller: 'NewAppointmentModalCtl',
            windowClass: 'app-modal-window',
            resolve: {
                ExistingCustomers: function () {
                    return ExistingCustomers;
                },
                RevowaxShopDetails: function () {
                    return RevowaxShopDetails;
                },
                Start: function () {
                    return start
                },
                End: function () {
                    return end
                },
                Id: function () {
                    return id
                }
            }
        });

        // Call Back from Modal
        modalInstance.result.then(function () {
                is_modal_open = false;
                ShopService.get_shop_bookings(
                    SHOP_ID,
                    // Success
                    function (bookings) {
                        add_bookings_to_calendar(bookings)
                    },
                    // ERROR
                    function () {

                    }
                )
            },
            // If Cancelled
            function () {
                is_modal_open = false;
            });
    };

    // Edit Appointment Modal Control
    var open_edit_modal = function (start, end, id,booking, renderCalender) {
        is_modal_open = true;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: "modules/app/manager/content/agenda/modals/edit_booking_modal_tmpl.html",
            controller: 'EditAppointmentModalCtl',
            windowClass: 'app-modal-window',
            resolve: {
                ExistingCustomers: function () {
                    return ExistingCustomers;
                },
                RevowaxShopDetails: function () {
                    return RevowaxShopDetails;
                },
                BookingDetails:function(){
                    return booking
                },
                Start: function () {
                    return start
                },
                End: function () {
                    return end
                },
                Id: function () {
                    return id
                }
            }
        });

        // Call Back from Modal
        modalInstance.result.then(function () {
                is_modal_open = false;
                ShopService.get_shop_bookings(
                    SHOP_ID,
                    // Success
                    function (bookings) {
                        add_bookings_to_calendar(bookings)
                    },
                    // ERROR
                    function () {

                    }
                )
            },
            // If Cancelled
            function () {
                is_modal_open = false;
            });
    };
});




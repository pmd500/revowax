// revo_app.factory('BookingService', ['$http', '$rootScope', 'UserService', 'BookingDetailsModel', function($http, $rootScope, UserService, BookingDetailsModel) {
//
//     // init data
//     var booking_management = {
//         service_loading_status : {
//             AvailableAppointmentsLoading : false
//         }
//     };
//
//     // Get User Data
//     var userDetails = UserService.GetUserDetails();
//
//     // Set loading status of GetAvailableAppointments Web Service
//     var SetAvailableAppointmentsServiceStatus = function (flag) {
//         booking_management.service_loading_status.AvailableAppointmentsLoading = flag;
//         $rootScope.$broadcast('booking_management_service_status:updated', booking_management.service_loading_status);
//     }
//
//     // Get Available bookings
//     booking_management.GetAvailableAppointments = function (){
//
//         // Get up to date user data
//         userDetails = UserService.GetUserDetails();
//
//         // If service is not loading and duration > 0, call webservice
//         if((booking_management.service_loading_status.AvailableAppointmentsLoading == false)&& (BookingDetailsModel.GetBookingDetails().total_duration > 0)) {
//             // Set Loading Flag
//             SetAvailableAppointmentsServiceStatus(true);
//
//             // Get Available Appointments
//             $http({
//                 method: 'post',
//                 url: urlBase + ':9876/revoweb/booker/availableSlotsInDayQuery',
//                 headers: {
//                     "REVO-PUBLIC-TOKEN": "token"
//                 },
//                 data: {
//                     "clientName": "testSalon",
//                     "searchDay": FormatDate(BookingDetailsModel.GetBookingDetails().desired_start_datetime),
//                     "durationInMins": BookingDetailsModel.GetBookingDetails().total_duration
//                 }
//             // SUCCESS
//             }).then(function successCallback(response) {
//                     BookingDetailsModel.SetAvailableAppointments(response.data);
//                     SetAvailableAppointmentsServiceStatus(false);
//                 },
//
//                 // FAIL
//                 function errorCallback(response) {
//                     SetAvailableAppointmentsServiceStatus(false);
//                 });
//         };
//     };
//
//     // Book The Appointment
//     booking_management.BookAppointment = function (startTime, endTime, room, treatmentLocation, treatmentSpecialist, clientName, salonName,treatmentList, success_callback, failure_callback ){
//         // Get up to date user data
//         userDetails = UserService.GetUserDetails();
//         $http({
//             method: 'put',
//             url: urlBase + ':9876/revoweb/booker/addBooking',
//             headers: {
//                 'X-REVO-AUTH-TOKEN':  userDetails.hash,
//                 'X-REVO-AUTH-USER': userDetails.username
//             },
//             data: {
//                 "startTime" : startTime,
//                 "endTime" : endTime,
//                 "room" : room,
//                 "treatmentLocation" : treatmentLocation,
//                 "treatmentSpecialist" : treatmentSpecialist,
//                 "clientName" : clientName,
//                 "salonName" : salonName,
//                 "treatmentList": treatmentList
//             }
//             // SUCCESS
//             }).then(function successCallback(response) {
//                 success_callback();
//             },
//             // FAIL
//             function errorCallback(response) {
//                 failure_callback(response);
//             });
//     };
//
//
//     // Updates the booking status
//     booking_management.UpdateAppointmentStatus = function (appointmentId, status, success_callback, failure_callback ){
//         // Get up to date user data
//
//
//         userDetails = UserService.GetUserDetails();
//         $http({
//             method: 'put',
//             url: urlBase + ':9876/revoweb/booker/updateBookingStatus',
//             headers: {
//                 'X-REVO-AUTH-TOKEN':  userDetails.hash,
//                 'X-REVO-AUTH-USER': userDetails.username
//             },
//             data: {
//                 "appointmentId" : appointmentId,
//                 "status" : status,
//             }
//             // SUCCESS
//         }).then(function successCallback(response) {
//                 success_callback();
//             },
//             // FAIL
//             function errorCallback(response) {
//                 failure_callback();
//             });
//     };
//
//
//
//     // Get All Appointments for Salon
//     booking_management.GetAllAppointments = function (clientName, startDateTime, endDateTime, success_callback, failure_callback){
//         // Get up to date user data
//         userDetails = UserService.GetUserDetails();
//
//         $http({
//             method: 'put',
//             url: urlBase + ':9876/revoweb/booker/bookedAppointmentsQuery',
//             headers: {
//                 'X-REVO-AUTH-TOKEN': userDetails.hash,
//                 'X-REVO-AUTH-USER': userDetails.username
//             },
//
//             data: {
//                 "clientName" : clientName,
//                 "startDateTime" : startDateTime,
//                 "endDateTime" : endDateTime
//              }
//
//             // SUCCESS
//             }).then(function successCallback(response) {
//                 success_callback(response.data);
//             },
//
//             // FAIL
//             function errorCallback(response) {
//                 failure_callback();
//             });
//     };
//
//     // Get All Appointments for Salon
//     booking_management.GetAppointmentInfo = function (appointmentID, success_callback, failure_callback){
//         // Get up to date user data
//         userDetails = UserService.GetUserDetails();
//
//         $http({
//             method: 'put',
//             url: urlBase + ':9876/revoweb/booker/bookedAppointmentsQuery',
//             headers: {
//                 'X-REVO-AUTH-TOKEN': userDetails.hash,
//                 'X-REVO-AUTH-USER': userDetails.username
//             },
//
//             data: {
//                 "appointmentId" : appointmentID,
//             }
//
//         // SUCCESS
//         }).then(function successCallback(response) {
//                 success_callback(response);
//             },
//
//             // FAIL
//             function errorCallback(response) {
//                 failure_callback(response);
//             });
//     };
//
//
//     // Get All Appointments for Client
//     booking_management.GetCustomerAppointments = function (clientName, startDateTime, endDateTime, success_callback, failure_callback){
//         // Get up to date user data
//         userDetails = UserService.GetUserDetails();
//
//
//         $http({
//             method: 'put',
//             url: urlBase + ':9876/revoweb/booker/bookedAppointmentsForCustomerQuery',
//             headers: {
//                 // #FIXME
//                 'X-REVO-AUTH-TOKEN':  userDetails.hash,
//                 'X-REVO-AUTH-USER': userDetails.username
//             },
//
//             data: {
//                 "clientName" : clientName,
//                 "startDateTime" : startDateTime,
//                 "endDateTime" : endDateTime
//             }
//
//         }).then(
//
//         // SUCCESS
//         function successCallback(response) {
//             success_callback(response.data);
//         },
//
//         // FAIL
//         function errorCallback(response) {
//             failure_callback(response.data);
//         });
//     };
//
//     // format the numbers with 0 padding for single digit numbers
//     var pad2 = function (number) {
//         return (number < 10 ? '0' : '') + number
//     }
//
//     // Format Date time - example format 2016-01-05 10:00:00
//     /**
//      * @return {string}
//      */
//     booking_management.FormatDateTime = function (datetime) {
//         var formattedDateTimeString = (datetime.getFullYear() + "-"+pad2(datetime.getMonth() + 1)+"-"+pad2(datetime.getDate())+ " "+pad2(datetime.getHours()) +":"+pad2(datetime.getMinutes()) +":" +pad2(datetime.getSeconds()));
//         return formattedDateTimeString;
//     }
//
//     // Calculate End Time of Appointment
//     booking_management.CalcEndDateTime = function(start_datetime, durations_minutes) {
//         return new Date(start_datetime.getTime() + durations_minutes*60000);
//     }
//
//     var FormatDate = function (datetime) {
//         var formattedDateTimeString = (datetime.getFullYear() + "-"+pad2(datetime.getMonth() + 1) +"-"+pad2(datetime.getDate()));
//         return formattedDateTimeString;
//     }
//
//     return booking_management;
// }]);
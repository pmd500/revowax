from rest_framework.response import Response
from rest_framework.permissions import  IsAuthenticated
from rest_framework.views import APIView
from revo_booker.serializers.deep import *

# Bookings for Customers
class CustomerBookingsList(APIView):
    permission_classes = (IsAuthenticated,)

    # Get bookings for business
    def get(self, request, customer_id, format=None):
        bookings = CustomerBooking.objects.filter(customer=customer_id, booking_status=2)
        serializer = CustomerBookingSerializer(bookings, many=True)
        return Response(serializer.data)

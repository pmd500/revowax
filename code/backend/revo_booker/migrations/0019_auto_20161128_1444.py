# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-28 14:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0018_resourceregularavailability_match_store_opening_times'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='business',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='shops', to='revo_booker.Business'),
        ),
    ]

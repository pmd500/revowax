// // Standard Settings
// var DATE_STRING_FORMAT = "yyyy-mm-dd"
// var DAYS_AHEAD = 7; // number of days to look ahead
//
//
// // Booking Statuses
// const BOOKING_CONFIRMED = 2
// const BOOKING_CANCELLED = 3


// Configure App
revo_app.config(function($stateProvider, $urlRouterProvider, $httpProvider, usSpinnerConfigProvider) {
    // Enable html5 push state for SEO & web crawlers
    //$locationProvider.html5Mode(true);

    // Set up rest for https
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    // For any unmatched url, redirect to
    if(test_mode)
        $urlRouterProvider.otherwise("/test");
    else
        $urlRouterProvider.otherwise("/app/anonymous/landing");

    // Set up spinner options
    usSpinnerConfigProvider.setDefaults({color: '#5bb944'});

    // Abstract app
    $stateProvider
        .state('test', {
            url: "/test",
            template: "<h1>Testing</h1>",
            controller: "TestCtl",
            controllerAs: "test_ctl",
            resolve: {
                RevowaxShopDetails: function(ShopService){
                    return ShopService.get_defered_shop_details(SHOP_ID);
                }
            }
        })
    
    
    // Abstract app
    $stateProvider
        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "modules/app/app_tmpl.html",
            controller: "AppCtl",
            controllerAs: "app_ctl",
            resolve: {
                RevowaxShopDetails: function(ShopService){
                    return ShopService.get_defered_shop_details(SHOP_ID);
                },
            }
        })

    // Abstract anonymous
    $stateProvider
        .state('app.anonymous', {
            url: "/anonymous",
            abstract: true,
            templateUrl: "modules/app/anonymous/anonymous_tmpl.html",
            controller: "AnonymousCtl",
            controllerAs: "anony_ctl",
            data: {
                css: 'modules/app/anonymous/anonymous.css'
            }
        })


    // Anonymous Registration Page
    $stateProvider
        .state('app.anonymous.register', {
            url: "/register",
            views: {
                "anonymous_top_menu": {
                    templateUrl: "modules/app/anonymous/top_menu/top_menu_tmpl.html",
                    controller: "AnonymousTopMenuCtl",
                    controllerAs: "top_ctl"
                },
                "anonymous_content": {
                    templateUrl: "modules/app/anonymous/content/register/register_tmpl.html",
                    controller: "RegisterCtl",
                    controllerAs: "reg_ctl"
                }
            },
        })

    // Abstract Customer Page
    $stateProvider
        .state('app.customer', {
            url: "/customer",
            abstract: true,
            templateUrl: "modules/app/customer/customer_tmpl.html",
            controller: "CustomerCtl",
            controllerAs: "cust_ctl",
            resolve: {
                UserDetail: function(UserService){
                    return UserService.check_authenticated();
                },
            },
            data: {
                css: 'modules/app/customer/customer.css'
            },
        })

    // Abstract Customer Page
    $stateProvider
        .state('app.manager', {
            url: "/manager",
            abstract: true,
            templateUrl: "modules/app/manager/manager_tmpl.html",
            controller: "ManagerCtl",
            controllerAs: "man_ctl",
            resolve: {
                UserDetail: function(UserService){
                    return UserService.check_authenticated();
                },
            },
            data: {
                css: 'modules/app/customer/customer.css'
            },
        })

    // // Abstract Customer Page
    // $stateProvider
    //     .state('app.customer.booking', {
    //         url: "/booking",
    //         abstract: true,
    //         templateUrl: "modules/app/customer/customer_tmpl.html",
    //         controller: "BookingCtl",
    //         controllerAs: "book_ctl",
    //         resolve: {
    //             UserDetail: function(UserService){
    //                 return UserService.check_authenticated();
    //             },
    //         },
    //         data: {
    //             css: 'modules/app/customer/content/booking/booking.css'
    //         },
    //     })
    //
    // // Set up booking urls
    // // Customer Booking Page 1
    // $stateProvider
    //     .state('app.customer.booking.stage1', {
    //         url: "/stage1",
    //         templateUrl: "modules/app/customer/content/booking/stage7_problem/stage7_tmpl.html",
    //         controller: "BookingStage1Ctl",
    //         controllerAs: "booking_stage1_ctl"
    //     })
    //
    //
    // // Customer Booking Page 2
    // $stateProvider
    //     .state('app.customer.booking.stage2', {
    //         url: "/stage2",
    //         templateUrl: "modules/app/customer/content/booking/stage7_problem/stage7_tmpl.html",
    //         controller: "BookingStage2Ctl",
    //         controllerAs: "booking_stage2_ctl"
    //     })
    //
    // // Customer Booking Page 4
    // $stateProvider
    //     .state('app.customer.booking.stage4', {
    //         url: "/stage4",
    //         templateUrl: "modules/app/customer/content/booking/stage7_problem/stage7_tmpl.html",
    //         controller: "BookingStage4Ctl",
    //         controllerAs: "booking_stage4_ctl"
    //     })
    //
    // // Customer Booking Page 6
    // $stateProvider
    //     .state('app.customer.booking.stage6', {
    //         url: "/stage6",
    //         templateUrl: "modules/app/customer/content/booking/stage7_problem/stage7_tmpl.html",
    //         controller: "BookingStage6Ctl",
    //         controllerAs: "booking_stage6_ctl"
    //     })
    //
    // // Customer Booking Page 7
    // $stateProvider
    //     .state('app.customer.booking.stage7', {
    //         url: "/stage7",
    //         templateUrl: "modules/app/customer/content/booking/stage7_problem/stage7_tmpl.html",
    //         controller: "BookingStage7Ctl",
    //         controllerAs: "booking_stage7_ctl"
    //     })

    // Customer Landing Page
    
    // // Customer Landing Page
    // $stateProvider
    //     .state('app.customer.landing', {
    //         url: "/register",
    //         views: {
    //             "customer_top_menu": {
    //                 templateUrl: "modules/app/customer/top_menu/top_menu_tmpl.html",
    //                 controller: "CustomerTopMenuCtl",
    //                 controllerAs: "top_ctl"
    //             },
    //             "customer_content": {
    //                 templateUrl: "modules/app/customer/content/landing/landing_ctl.js",
    //                 controller: "CustomerLandingCtl",
    //                 controllerAs: "login_ctl"
    //             }
    //         },
    //     })


    // // Main APP
    // $stateProvider
    //     .state('app', {
    //         url: "/app",
    //         abstract: true,
    //         views: {
    //             "topmenu": {
    //                 templateUrl: "modules/topmenu/topmenu.tmpl.html",
    //                 controller: "topmenuController",
    //                 data: {
    //                     css: 'modules/topmenu/topmenu.css'
    //                 }
    //             },
    //             "app": {
    //                 templateUrl: "modules/app/app_tmpl.html",
    //                 controller: "AppController"
    //             }
    //         }
    //     })

    // // Now set up the states
    // $stateProvider
    //     .state('app.landing', {
    //         url: "/landing",
    //         templateUrl: "modules/app/shared/landing/landing_tmpl.html",
    //         controller: "landingController",
    //         data: {
    //             css: 'modules/landing/landing.css'
    //         }
    //     })
    //
    //     .state('app.booking', {
    //         url: "/booking",
    //         abstract: true,
    //         templateUrl: "modules/app/customer/booking/booking_tmpl.html",
    //         controller: "bookingController",
    //         data: {
    //             css: 'modules/booking/booking.css'
    //         }
    //     })
    //     .state('app.booking.stage1', {
    //         url: "/stage1/service-selection",
    //         templateUrl: "modules/app/customer/booking/stage1-services/stage1.tmpl.html",
    //         controller: "stage1Controller"
    //     })
    //
    //     .state('app.booking.stage2', {
    //         url: "/appointment-selection",
    //         abstract: true,
    //         templateUrl: "modules/app/customer/booking/stage2-timedate/stage2.tmpl.html",
    //         controller: "stage2Controller"
    //     })
    //
    //     .state('app.booking.stage2.appointment-selection', {
    //         url: "/",
    //         views: {
    //             "date_selection": {
    //                 templateUrl: "modules/app/customer/booking/stage2-timedate/date_selection/date_selection.tmpl.html",
    //                 controller: "dateSelectionController"
    //             },
    //             "time_selection": {
    //                 templateUrl: "modules/app/customer/booking/stage2-timedate/time_selection/time_selection_tmpl.html",
    //                 controller: "timeSelectionController"
    //             },
    //             "appointment_selection": {
    //                 templateUrl: "modules/app/customer/booking/stage2-timedate/appointment_selection/appointment_selection_tmpl.html",
    //                 controller: "appointmentSelectionController"
    //             }
    //         }
    //     })
    //     .state('app.booking.stage3', {
    //         url: "/user-details",
    //         templateUrl: "modules/app/customer/booking/stage3-userdetails/stage3_tmpl.html",
    //         controller: "stage3Controller"
    //     })
    //     .state('app.booking.stage4', {
    //         url: "/summary",
    //         templateUrl: "modules/app/customer/booking/stage4-overview/stage4_tmpl.html",
    //         controller: "stage4Controller"
    //     })
    //     .state('app.booking.stage5', {
    //         url: "/stage5/payment",
    //         templateUrl: "modules/app/customer/booking/stage5-payment/stage5_tmpl.html",
    //         controller: "stage5Controller"
    //     })
    //
    //     .state('app.booking.stage6', {
    //         url: "/confirmation",
    //         templateUrl: "modules/app/customer/booking/stage6-confirmation/stage6_tmpl.html",
    //         controller: "stage6Controller"
    //     })
    //     .state('app.booking.stage7', {
    //         url: "/error",
    //         templateUrl: "modules/app/customer/booking/stage7-problem/stage7_ctl_tmpl.html",
    //         controller: "stage7Controller"
    //     })
    //
    //     .state('app.agenda', {
    //         url: "/agenda",
    //         templateUrl: "modules/app/staff/agenda/agenda_tmpl.html",
    //         controller:  "calendarController",
    //         data: {
    //             css: 'modules/agenda/agenda.css'
    //         }
    //     })
    //
    //     .state('app.profile', {
    //         url: "/profile",
    //         templateUrl: "modules/app/shared/salon/profile/profile_ctl_tmpl.html",
    //         controller:  "ProfileController",
    //         data: {
    //             css: 'modules/salon/profile/profile.css'
    //         }
    //     })
    //
    //     .state('app.account', {
    //         url: "/account",
    //         abstract: true,
    //         templateUrl: "modules/account/account.tmpl.html",
    //         controller:  "AccountController",
    //         data: {
    //             css: 'modules/account/account.css'
    //         }
    //     })
    //
    //     .state('app.account.login', {
    //         url: "/login",
    //         templateUrl: "modules/account/login/login_tmpl.html",
    //         controller:  "loginController",
    //         data: {
    //             css: 'modules/account/login/login.css'
    //         }
    //     })
    //
    //     .state('app.account.register', {
    //         url: "/register",
    //         templateUrl: "modules/account/register/register_tmpl.html",
    //         controller:  "registerController",
    //         data: {
    //             css: 'modules/account/register/register.css'
    //         }
    //     })
    //
    //     .state('app.account.details', {
    //         url: "/user/details",
    //         templateUrl: "modules/app/customer/details/details_tmpl.html",
    //         controller:  "ReadDetailsController",
    //         data: {
    //             css: 'modules/account/details/details.css'
    //         }
    //     })
    //
    //     .state('app.account.bookings', {
    //         url: "/user/bookings",
    //         templateUrl: "modules/app/customer/bookings/appointments_tmpl.html",
    //         controller:  "AppointmentsController"
    //     })
});


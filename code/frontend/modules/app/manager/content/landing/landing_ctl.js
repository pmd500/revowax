revo_app.controller("ManagerLandingCtl", ["$scope", "$state", "RevowaxShopDetails", function($scope, $state, RevowaxShopDetails){
    var ctl = this;

    // bind variables
    ctl.shop = RevowaxShopDetails;
    
    return ctl;
}]);
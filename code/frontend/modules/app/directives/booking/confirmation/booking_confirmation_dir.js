revo_app.directive('bookingConfirmation', function(AppStateService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/confirmation/booking_confirmation_tmpl.html',
        scope: {
            // variables
            shop:"=",
            // redirect to state
            redirect:"@",
            // functions
            next:"@"
        },
        link : function (scope) {
            // Check there there is data otherwise redirect
            if(AppStateService.customer_state.selected_slot == null)
                $state.go(scope.redirect)
            
            // Bind appointment details
            scope.selected_slot = AppStateService.customer_state.selected_slot
            scope.selected_services = AppStateService.customer_state.selected_services
            scope.total_price = AppStateService.customer_state.total_price
            scope.total_duration = AppStateService.customer_state.total_duration
            scope.start_date =  new Date(scope.selected_slot.start)
            scope.new_email =  AppStateService.user_state.new_email
            scope.logged_in =  AppStateService.user_state.logged_in
            
            scope.continue = function(){
                $state.go(scope.next)
            }
        }
    };
});
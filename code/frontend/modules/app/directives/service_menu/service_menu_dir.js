revo_app.directive('serviceMenu', function() {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/service_menu/service_menu_tmpl.html',
        scope: {
            shop: "=",
            selectable: "=",
            selservices: "=",
            price:"=",
            duration:"="
        },
        link : function (scope) {
            scope.selservices = []

            var calculate_totol_price = function(){
                var price = 0;
                for (var i =0; i < scope.selservices.length; i++) {
                    price = price + parseInt(scope.selservices[i].price);
                }
                return price;
            }

            var calculate_total_duration= function(){
                var duration = 0;
                for (var i =0; i < scope.selservices.length; i++) {
                    duration = duration + parseInt(scope.selservices[i].minutes);
                }
                return duration;
            }
            
            // Removes service from selected services array
            var remove_service_frm_selected = function(service){
                for(var i = 0; i < scope.selservices.length; i++) {
                    if(scope.selservices[i].id == service.id) {
                        scope.selservices.splice(i, 1);
                        break;
                    }
                }
            }
            
            // Group together selected services
            scope.on_click_service = function(service){
                if(scope.selectable == true){

                    // if already selected
                    if(service.selected)
                        remove_service_frm_selected(service)
                    // add service to selected
                    else
                        scope.selservices.push(service)

                    // toggle field
                    service.selected = !service.selected;

                    // sum price and minutes
                    scope.price = calculate_totol_price()
                    scope.duration = calculate_total_duration()
                }
            }

            // unselect all categories
            scope.unselect_all = function(categories){
                categories.forEach(function (category) {
                    category.selected = false;
                    
                    category.subcategories.forEach(function(subcategory){
                        subcategory.selected = false;
                    })
                })
            }

            // Recursive function to tranverse all subcatogries and add selected field
            var tranverse_subcategories = function (category, top_selected){
                // loop the subcategories
                category.subcategories.forEach(function (subcategory,index) {

                    // add selected field to subcategory, first selected
                    subcategory.selected = (index == 0) && top_selected;

                    if(subcategory.services.length==0)
                        tranverse_subcategories(category, top_selected)
                    else
                        add_selected_to_services(category.services)
                })
            }

            // Add selected field to all services
            var add_selected_to_services = function (services){
                // loop through all services
                services.forEach(function (service) {

                    // add select field to service
                    service.selected = false;


                })
            }

            // Format the list of categories and services with IDs and selected flags
            var generate_selectable_services = function (shop) {
                var top_level_categories = shop.service_groups[0].root_catergory.subcategories;

                // Loop through top level categories
                top_level_categories.forEach(function (top_level_category, index) {
                    var is_first = (index == 0)
                    tranverse_subcategories(top_level_category, is_first)
                })

                return top_level_categories
            }

            // Add selected field every cetegory and service node
            scope.top_level_categories = generate_selectable_services(scope.shop);
        }
    };
});
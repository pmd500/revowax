revo_app.directive('anonymousTopMenu', function($state, UserService) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/anonymous/directives/anonymous_top_menu/anonymous_top_menu_tmpl.html',
        link: function(scope, element, attrs, tabsCtrl) {
            scope.is_collapsed = true;
            
            scope.toggle_nav = function () {
                scope.is_collapsed = !scope.is_collapsed
            }
            
        }
    };
});

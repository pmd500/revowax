revo_app.directive('passwordResetConfirmationForm', function(UserService, $state, $stateParams) {
    return {
        restrict: 'E',
        scope: {
        },
        templateUrl: 'modules/app/directives/forms/password_reset_confirmation_form/password_reset_confirmation_form_tmpl.html',
        link: function (scope, element, attrs, tabsCtrl) {

            // State params
            scope.token = $stateParams.token
            scope.uid = $stateParams.uid
            
            // Controller variables
            scope.password1 = ""
            scope.password2 = ""
            scope.message = "";
            scope.message_style = {"color": "green"};

            // Set up functions
            scope.reset_password_confirmation = function (){
                // Call webservice
                UserService.reset_password_confirmation(
                    scope.password1,
                    scope.password2,
                    scope.uid,
                    scope.token,
                    
                    // On Success
                    function(){
                        scope.message = "Password successfully updated, redirecting to login screen."
                        scope.message_style = {"color":"green"}

                        // After 2 second delay, go to login screen
                        setTimeout(function() {
                            $state.go("app.anonymous.login")
                        }, 2000);

                    },
                    // On Failure
                    function(){
                        scope.message_style = {"color": "red"};
                        scope.message = "Stale reset confirmation code, please try resetting the password again";
                    }
                )
            }
        }
    }
});
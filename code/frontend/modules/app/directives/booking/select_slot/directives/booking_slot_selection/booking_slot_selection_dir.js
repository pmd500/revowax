revo_app.directive('bookingSlotSelection', function(BookingService) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/select_slot/directives/booking_slot_selection/booking_slot_selection_tmpl.html',
        scope: {
            seldatetime : "=",
            bookingslots : "=",
            duration: "=",
            selslot : "="
        },
        link : function (scope) {
            
            var unselect_all_slots = function(){
                scope.bookingslots.forEach(function(slot){
                    slot.selected=false;
                })
            }

            // handle service selection
            scope.select_booking_slot = function (slot) {
                unselect_all_slots();
                slot.selected=true;
                scope.selslot = slot
                console.log("selected slot")
                console.log(slot)
            }

            // Returns date
            scope.date_string_to_date = function (date_string) {
                return new Date(date_string)
            }
            
            // Returns correctly formatted 12 hour clock
            scope.formatAMPM = function (date_string) {
                date = new Date(date_string)
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return strTime;
            }
        }
    };
});

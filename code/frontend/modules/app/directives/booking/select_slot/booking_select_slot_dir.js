revo_app.directive('bookingSelectSlot', function(BookingService, UtilityService, AppStateService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/select_slot/booking_select_slot_tmpl.html',
        scope: {
            // shop details
            shop:"=",
            // redirect if problem
            redirect:"@",
            // next state
            next:"@"
        },
        link : function (scope) {
            // Chosen Available appointment
            scope.chosen_date = "";
            scope.chosen_room = "";

            scope.bookingslots = null;
            scope.seldatetime = new Date();
            scope.selslot = null;
            scope.duration = AppStateService.customer_state.total_duration

            // Internal variables
            var downloaded_slots = [];

            // Check there there is data
            if(scope.duration == null)
                $state.go(scope.redirect)

            scope.get_available_booking_slots = function (search_date) {

                //Set scope
                scope.seldatetime = search_date

                var look_ahead_datetime = new Date(search_date)
                look_ahead_datetime.setDate(search_date.getDate() + DAYS_AHEAD);

                // Booking slots for day already downloaded
                var selected_days_slots = sort_booking_slots(downloaded_slots, scope.seldatetime )
                var date_already_downloaded = (selected_days_slots.length > 0)

                // If appointment data already available
                if (date_already_downloaded){
                    scope.bookingslots = selected_days_slots;
                }
                // Call webservice to get available bookings
                else {
                    console.log("Searching for available bookings between " + search_date.format(DATE_STRING_FORMAT)
                    + " and " + look_ahead_datetime.format(DATE_STRING_FORMAT)
                    )
                    BookingService.get_available_booking_slots(
                        BUSINESS_ID,
                        SHOP_ID,
                        GROUP_ID,
                        scope.duration,
                        // Start date
                        search_date.format(DATE_STRING_FORMAT),
                        // End date - number of days ahead
                        look_ahead_datetime.format(DATE_STRING_FORMAT),
                        // Success
                        function (booking_slots) {
                            // push new slots onto array
                            downloaded_slots = booking_slots;
                            // updated booking slots
                            scope.bookingslots = sort_booking_slots(booking_slots, scope.seldatetime )
                        },
                        // Error
                        function () {

                        }
                    )
                }
            }

            // Filters booking slots on selected day and desired time
            var sort_booking_slots = function (booking_slots, search_date){
                var next_day = new Date(search_date)

                // Set hours to 0:00 am
                next_day.setHours(0)
                next_day.setMinutes(0)

                // Calculate 24 hours from start of today
                next_day.setDate(search_date.getDate() + 1)

                console.log("filtering slots between " + search_date.format(DATE_STRING_FORMAT)
                    + " and " + next_day.format(DATE_STRING_FORMAT))
                return _.filter(booking_slots, function(slot){

                    return (new Date(slot.start) >= search_date) && (new Date(slot.start) < next_day)
                })

            };

            // Save Booking Details
            scope.continue = function(){
                AppStateService.customer_state.selected_slot = scope.selslot
                if(scope.selslot != null){
                    $state.go(scope.next)
                }
            }
            
            // Get initial bookings
            scope.get_available_booking_slots(scope.seldatetime)
        }
    };
});
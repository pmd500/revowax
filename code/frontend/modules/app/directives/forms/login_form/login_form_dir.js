revo_app.directive('loginForm', function(UserService, $state, AppStateService) {
    return {
        restrict: 'E',
        scope: {
            nextState:"@",
            showRegister:"="
        },
        templateUrl: 'modules/app/directives/forms/login_form/login_form_tmpl.html',
        link: function (scope, element, attrs, tabsCtrl) {
            // Controller variables
            scope.username = ""
            scope.password = ""
            scope.error_message = "";

            // Set up functions
            scope.login = function (username, password){
                // Call webservice
                UserService.login(
                    username,
                    password,
                    // On Success
                    function(){
                        if(scope.showRegister){
                            AppStateService.navigate_to_profile_home()
                        }
                        else {
                            $state.go(scope.nextState)
                        }
                    },
                    // On Failure
                    function(){
                        scope.error_message = "Invalid username/password, please try again.";
                    }
                )
            }
        }
    }
});
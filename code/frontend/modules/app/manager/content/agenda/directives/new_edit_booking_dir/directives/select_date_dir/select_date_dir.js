revo_app.directive('agendaDateSelect', function(AppStateService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_date_dir/select_date_tmpl.html',
        scope: {
            start : "=",
        },
        link: function (scope) {

            // DATE PICKER
            scope.today = function() {
                scope.dt = new Date(scope.start.toDate())
            };
            scope.today();

            scope.clear = function () {
                scope.dt = null;
            };

            // Disable weekend selection
            scope.disabled = function(date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 ));
            };

            scope.toggleMin = function() {
                scope.minDate = scope.minDate ? null : new Date();
            };
            scope.toggleMin();
            scope.maxDate = new Date(2020, 5, 22);

            scope.open = function($event) {
                scope.status.opened = true;
            };

            scope.setDate = function(year, month, day) {
                scope.dt = new Date(year, month, day);          

            };

            scope.options = {
                formatYear: 'yy',
                formatMonth:'mm',
                startingDay: 1
            };

            scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yy', 'shortDate'];
            scope.format = scope.formats[2];

            scope.status = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            scope.events =
                [
                    {
                        date: tomorrow,
                        status: 'full'
                    },
                    {
                        date: afterTomorrow,
                        status: 'partially'
                    }
                ];

            scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);

                    for (var i=0;i<scope.events.length;i++){
                        var currentDay = new Date(scope.events[i].date).setHours(0,0,0,0);

                        if (dayToCheck === currentDay) {
                            return scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            // Watch DT model
            scope.$watch('dt', function() {
                scope.start.year(scope.dt.getFullYear()).month(scope.dt.getMonth()).date(scope.dt.getDate())
            });
        }
    }
});
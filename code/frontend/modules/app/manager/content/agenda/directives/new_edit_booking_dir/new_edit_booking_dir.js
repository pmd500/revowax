revo_app.directive('agendaNewEditBooking', function(AppStateService, BookingService, UtilityService,$filter) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/new_edit_booking_tmpl.html',
        scope: {
            existing_customers: "=existingCustomers",
            shop_details: "=shopDetails",
            start : "=",
            end : "=",
            id : "=",
            modal_instance:"=modalInstance",
            booking_details:"=bookingDetails",
            editable: "="
        },
        link: function (scope) {

            // Init selected services
            scope.total_cost = 0;
            scope.total_duration = 0
            scope.selected_customer = {}
            scope.selected_group = {}
            scope.selected_services = []

            // Sums the cost of all services
            var calculate_duration = function () {
                return _.reduce(scope.selected_services , function(memo, service){ return memo + service["minutes"]; }, 0)
            }

            // Sums the cost of all services
            var calculate_total = function () {
                return _.reduce(scope.selected_services , function(memo, service){ return memo + service["price"]; }, 0)
            }

            // Data changes
            scope.$watchCollection('selected_services', function(newValue, oldValue) {
                scope.total_cost = calculate_total()
                scope.total_duration = calculate_duration()
            });
            
            // Check all fields have been field in
            var validate_form = function () {
                scope.is_form_valid = true;
                scope.message = ""
                
                if(scope.selected_customer.id == null){
                    scope.is_form_valid = false;
                    scope.error_message = "please select or create a new customer." 
                }

                else if(scope.selected_services == []){
                    scope.is_form_valid = false;
                    scope.error_message = "please select at least one service."
                }
                return scope.is_form_valid;
            }

            // Continue
            scope.update_booking = function () {
                // Create booking
                if(validate_form()){
                    // Book
                    BookingService.update_booking(
                        // booking id
                        scope.booking_details.id,
                        // Booked at
                        scope.booking_details.booked_at,
                        // Booked By Profile ID
                        AppStateService.user_state.id,
                        // Booked For Customer ID
                        scope.selected_customer.id,
                        // Business_id
                        BUSINESS_ID,
                        // shop_id
                        SHOP_ID,
                        // resource_id
                        7,
                        // service_group_id
                        scope.booking_details.service_group.id,
                        // staff_id
                        "",
                        // start_datetime
                        $filter('date')(scope.start.toDate(), "yyyy-MM-dd HH:mm"),
                        // end_datetime
                        $filter('date')(UtilityService.date_methods.calc_end_date_time(scope.start.toDate(), scope.total_duration), "yyyy-MM-dd HH:mm"),
                        // duration
                        scope.total_duration,
                        // booking_status_id
                        BOOKING_STATUS.CONFIRMED,
                        // payment_status_id
                        PAYMENT_STATUS.PENDING,
                        // total_price
                        scope.total_cost,
                        // service_list
                        scope.selected_services,
                        // _.pluck(scope.selected_services, "id"),
                        // Success
                        function (data) {
                            scope.modal_instance.close();
                        },
                        // Error
                        function () {
                        }
                    )
                }
            }
            
            
            // Continue
            scope.book = function () {
                // Create booking
                if(validate_form()){
                    // Book
                    BookingService.book(
                        // Booked By Profile ID
                        AppStateService.user_state.id,
                        // Booked For Customer ID
                        scope.selected_customer.id,
                        // Business_id
                        BUSINESS_ID,
                        // shop_id
                        SHOP_ID,
                        // resource_id
                        7,
                        // service_group_id
                        scope.selected_group.selected.id,
                        // staff_id
                        "",
                        // start_datetime
                        $filter('date')(scope.start.toDate(), "yyyy-MM-dd HH:mm"),
                        // end_datetime
                        $filter('date')(UtilityService.date_methods.calc_end_date_time(scope.start.toDate(), scope.total_duration), "yyyy-MM-dd HH:mm"),
                        // duration
                        scope.total_duration,
                        // booking_status_id
                        BOOKING_STATUS.CONFIRMED,
                        // payment_status_id
                        PAYMENT_STATUS.PENDING,
                        // total_price
                        scope.total_cost,
                        // service_list
                        scope.selected_services,
                        // Success
                        function (data) {
                            scope.modal_instance.close();
                        },
                        // Error
                        function () {
                        }
                    )
                }
            }
        }
    }
});
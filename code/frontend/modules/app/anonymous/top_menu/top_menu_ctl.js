revo_app.controller("AnonymousTopMenuCtl", ["$scope", "$state", function($scope, $state){
    var ctl = this
    ctl.state = $state;

    // Add environment variables
    ctl.activate_treatwell_bookings = ACTIVATE_TREATWELL;
    
    return ctl
}]);
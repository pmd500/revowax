# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 15:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0045_auto_20170301_1522'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerbooking',
            name='booking_status',
            field=models.IntegerField(choices=[(1, b'Created'), (2, b'Confirmed'), (3, b'Cancelled'), (4, b'Complete'), (5, b'No Show')], default=1),
        ),
        migrations.AlterField(
            model_name='customerbooking',
            name='payment_status',
            field=models.IntegerField(choices=[(1, b'Refunded'), (2, b'Paid'), (3, b'Instore'), (4, b'Online'), (5, b'Pending')], default=5),
        ),
        migrations.DeleteModel(
            name='BookingStatus',
        ),
        migrations.DeleteModel(
            name='PaymentStatus',
        ),
    ]

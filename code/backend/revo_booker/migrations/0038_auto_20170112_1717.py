# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-12 17:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('revo_booker', '0037_remove_profile_is_staff'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='user_type',
            new_name='profile_type',
        ),
    ]

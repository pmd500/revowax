revo_app.factory("BusinessService", ["UtilityService", function (UtilityService) {
    // Bind to var
    var service = this;

    // Get Business Customers
    var get_business_customers = function (business_id, success_call, error_callback) {
        UtilityService.api_utils.get(
            // URL
            base_url + 'businesses/'+ business_id +'/customers/',
            // Success
            success_call,
            // Error
            error_callback

        )
    }

    // Get Business Customers Deferred
    var get_deferred_business_customers = function (business_id) {
        return UtilityService.api_utils.get_deferred(base_url + 'businesses/'+ business_id +'/customers/')
    }

    // Bind functions to service
    service.api = {
        // Get Customers belonging to Business (with deffered)
        get_business_customers: get_business_customers,
        get_deferred_business_customers: get_deferred_business_customers

    } ;

    return service;
}]);

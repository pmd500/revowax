revo_app.directive('agendaStaffSelect', function(AppStateService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/manager/content/agenda/directives/new_edit_booking_dir/directives/select_staff_dir/select_staff_tmpl.html',
        scope: {

        },
        link: function (scope) {
            scope.all_staff = ["Dani", "Phil", "Sarah"];
            scope.staff = {};
        }
    }
});
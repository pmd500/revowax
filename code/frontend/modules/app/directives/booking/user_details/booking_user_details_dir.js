revo_app.directive('bookingUserDetails', function(AppStateService, UserService, $state) {
    return {
        restrict: 'E',
        templateUrl: 'modules/app/directives/booking/user_details/booking_user_details_tmpl.html',
        scope: {
            // variables
            shop:"=",
            // functions
            next:"="
        },
        link : function (scope) {

            // Shows register form
            scope.show_register_from = false;

            // Register new user
            scope.register = function () {
                UserService.register(
                    ctl.first_name,
                    ctl.last_name,
                    ctl.email,
                    ctl.mobile,
                    ctl.password,
                    // registered user
                    true,
                    // success callback
                    function () {
                        alert("You have successfully registered")
                        $state.go("app.anonymous.login")
                    },
                    // failure callback
                    function (response) {
                        if (response.status == 500) {
                            ctl.error_message = "Server unavailable at the moment"
                        }
                        else {
                            ctl.error_message = response.data.message
                        }

                    }
                );
            }

            // Continue Button
            scope.continue = function(){


            }

        }
    };
});
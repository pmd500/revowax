import mysql.connector
import json

cnx = mysql.connector.connect(user='root', password='newyork1980',
                              host='127.0.0.1',
                              database='revosoft')


cursor = cnx.cursor()


# Add Treatment
add_treatment_node = ("INSERT INTO salontreatments "
              "(salonName, treatmentName, treatmentParent, treatmentPath, priceCurrency, price, durationMins) "
              "VALUES (%(salonName)s, %(treatmentName)s, %(treatmentParent)s, %(treatmentPath)s, %(priceCurrency)s, %(price)s, %(durationMins)s)")


with open("data.json") as json_file:
    json_data = json.load(json_file)
    print(json_data)


salonName = "Kingscross"

# Add to database
def AddTreatmentNode(treatmentName, treatmentParent,  treatmentPath, price, durationMins ):
    treatment_node = {
      'salonName': "Kingscross",
      'treatmentName': treatmentName,
      'treatmentParent': treatmentParent,
      'treatmentPath': treatmentPath,
      'priceCurrency': "GBP",
      'price': price,
      'durationMins': durationMins,
    }
    cursor.execute(add_treatment_node, treatment_node)
    
# Add top two nodes
AddTreatmentNode("AllTreatments", "", "AllTreatments","", "")
AddTreatmentNode("Waxing", "AllTreatments", "AllTreatments-Waxing", "","")
for catergory in json_data["categories"]:
    treatmentParent = "AllTreatments"
    treatmentName = catergory["name"]
    treatmentPath = "AllTreatments-Waxing-" + catergory["name"]
    print treatmentPath
    AddTreatmentNode(treatmentName, treatmentParent, treatmentPath, "", "")
    for subcategory in catergory["subcategories"]:
        treatmentParent = catergory["name"]
        treatmentName = subcategory["name"]
        treatmentPath = "AllTreatments-Waxing-" + catergory["name"] + "-" + subcategory["name"]
        print treatmentPath
        AddTreatmentNode(treatmentName, treatmentParent, treatmentPath,"","")
        for service in subcategory["services"]:
            treatmentParent = subcategory["name"]
            treatmentName = service["name"]
            treatmentPath = "AllTreatments-Waxing-" + catergory["name"] + "-" + subcategory["name"] + "-" + treatmentName
            treatmentPrice = service["price"]
            treatmentDuration = service["duration"]
            print treatmentPath, treatmentPrice, treatmentDuration
            AddTreatmentNode(treatmentName, treatmentParent, treatmentPath, treatmentPrice, treatmentDuration)

# Make sure data is committed to the database
cnx.commit()

cursor.close()
cnx.close()



